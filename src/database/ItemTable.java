package database;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;

import translator.Translator.AtomData;
import database.obj.Atom;
import database.obj.Const;
import database.obj.Term;
import database.obj.Var;
import database.DataBase;

/**
 * アイテムテーブルを司るクラスです．
 *
 * @author sin8
 */
public class ItemTable {

	//=========================================================================
	// ◆ 変数
	//=========================================================================

	/** リテラル集合 */
	private Atom[]		items;
	/** リテラル集合の大きさ */
	private int		itemSize;
	private ConcurrentHashMap<Integer, ConcurrentHashMap<String,Integer>> NameToID;//nameIdから<引数,ID>のハッシュを呼ぶ
	
	private DatatypeDef dataType = null;
	//=========================================================================
	// ◆ コンストラクタ
	//=========================================================================
	/**
	 * 空のアイテムテーブルを生成します
	 */
	public ItemTable() {  }
	/**
	 * データベースからアイテムテーブルを生成します
	 *
	 * @param database データベース
	 * @param varNum 変数の数
	 * @param noConst 定数の有無 trueならなし
	 */
	public ItemTable(DataBase database,DatatypeDef dataType,int varNum,boolean noConst) {
		this();
		System.out.println("itemTable make");
		makeCands(database,dataType,varNum,noConst);
	}
	//=========================================================================
	// ◆ オーバーライドするメソッド
	//=========================================================================

	//=========================================================================
	// ◆ GetterおよびSetterのメソッド
	//=========================================================================
	/**
	 * リテラル集合の大きさを取得します
	 */
	public int getItemSize() { return itemSize; }
	/**
	 * キーアトムを取得します
	 *
	 * @return
	 */
	public Atom getKey() { return items[0]; }
	/**
	 * リテラル集合からアトムを取得します
	 *
	 * @param index
	 * @return
	 */
	public Atom getAtom(int index) { return items[index]; }
	public int getAtomIDfromHM(Atom p) {
		int id = p.getNameID();
		ConcurrentHashMap<String, Integer> map = NameToID.get(id);
		return map.get(p.toStringArg());
	}
	public int getAtomIDfromHM(int nameID, String args) {
		ConcurrentHashMap<String, Integer> map = NameToID.get(nameID);
		if(map.get(args) != null){return map.get(args);}
		return -1;
	}
	/**
	 * リテラル集合そのものを取得します
	 *
	 * @return
	 */
	public Atom[] getAtoms() { return items; }

	/**
	 * アトムのIDを返します
	 * @param atom
	 * @return ID　無ければ-1
	 */
	public int getAtomID(Atom atom){
		
		for(int i = 0;i<this.itemSize;i++){
		
			if(this.items[i].compareTo(atom)==0){
				return i;
			}
		}
		
		return -1;
	}
	
	//=========================================================================
	// ◆ アイテムテーブルの生成
	//=========================================================================
	/**
	 * アイテムテーブルを生成します．
	 *
	 * @param	database	データベース
	 * @param	varNum		変数の数
	 * @param	noConst		定数の出現を許すか
	 */
	public void makeCands(DataBase database,DatatypeDef dataType, int varNum,boolean noConst) {
		//System.out.println("ItemTableを生成中...");
		this.dataType = dataType;
		//アイテムのリスト
		ArrayList<Atom> cands	= new ArrayList<Atom>();

		// キーの名前を取得
		String keyName = database.getKeyName();

		// 変数のリストを生成
		Var[] vars = new Var[varNum];
		for(int i=0;i<varNum;i++) { vars[i] = new Var(i); }

		// キーアトムを最初に
		cands.add(new Atom(keyName, 0, new Term[]{vars[0]}, dataType));

		// 残りのアトムについて
		keyName += "/1";

		String[] names = database.getPreds();//述語名取得

		for(int i=0;i<names.length;i++) {

			// キーは追加しているのでスキップ
			if(names[i].equals(keyName)) { continue; }

			String[] name	= names[i].split("/");
			int argSize		= Integer.valueOf(name[1]);//引数の数

			// 引数の大きさが 1 の場合
			if(argSize == 1) {
				for(int j=0;j<varNum;j++){
					cands.add(new Atom(name[0], i+1, new Term[]{vars[j]}, dataType));
					}
			}

			// 引数の大きさが 2 以上の場合
			else {
				if(noConst) {//定数なし

					genCandsNoConst(0,name[0], i+1, new Term[argSize],vars,cands);
				}

				else {//定数あり

					Term[][] atoms = database.getDatas(names[i]);//データ取得

					for(int j=0;j<atoms.length;j++) {
						genCandsWithConst(0, name[0], i+1, atoms[j], vars, cands);
					}
				}
			}
		}

		// キーアトム以降をバイアスを考慮した順序にソート
		items = cands.toArray(new Atom[0]);
		Arrays.sort(items, 1, items.length);
		
		itemSize = items.length;
		
		//IDを登録
		for(int i = 0;i<this.itemSize;i++){
			this.items[i].setID(i);
		}

		// 生成結果を標準出力
//		System.out.println("完了");

//		for(int i = 0;i < cands.size();i++){
//			System.out.println(i+":"+cands.get(i));
//		}
//		System.out.println("アイテム数: "+itemSize);
		
		//アトム->IDのハッシュマップを作成(05/15)
		NameToID = new ConcurrentHashMap<Integer, ConcurrentHashMap<String,Integer>>();
		for(int i=0, n=items.length;i<n;++i){
			Atom p = items[i];
			int nameID = p.getNameID();
			ConcurrentHashMap<String, Integer> map = NameToID.get(nameID);
			if(map == null){//内部ハッシュが作られていない
				ConcurrentHashMap<String, Integer> tempMap = new ConcurrentHashMap<String, Integer>();
				tempMap.put(p.toStringArg(), p.getID());
				NameToID.put(nameID, tempMap);
			}
			else{
				map.put(p.toStringArg(), p.getID());
			}
		}
		/*
		for(int i=0, n=items.length;i<n;++i){
			String name = items[i].getName();
			int ID = getAtomIDfromHM(items[i]);
			System.out.println(i + ", " + name + ", " + ID);
		}*/
	}

	/**
	 * 定数を含まない候補リテラルの生成を行います．
	 *
	 * @param index		注目するインデックス
	 * @param name		アトムの述語名
	 * @param args		アトムの引数
	 * @param vars		変数リスト
	 * @param cands		今までに生成した候補リテラルのリスト
	 */
	private void genCandsNoConst(int index,String name, int id, Term[] args,Var[] vars,ArrayList<Atom> cands) {

		if(index == args.length) {
			cands.add(new Atom(name,id,args,dataType)); //リテラル追加
			}

		else {
			Term[] newArgs = new Term[args.length];

			for(int i=0;i<args.length;i++) {
				if(index != i) {
					newArgs[i] = args[i];
					}
				}

			for(int i=0;i<vars.length;i++) {
				newArgs[index] = vars[i];

				//再帰的に全ての組み合わせの作る
				genCandsNoConst(index+1,name,id,newArgs,vars,cands);
			}

		}
	}

	/**
	 * 定数を含んだ候補リテラルの生成を行います．
	 *
	 * @param index		注目するインデックス
	 * @param name		アトムの述語名
	 * @param args		アトムの引数
	 * @param vars		変数リスト
	 * @param cands		今までに生成した候補リテラルのリスト
	 */
	private void genCandsWithConst(int index, String name, int id, Term[] args, Var[] vars, ArrayList<Atom> cands) {

		if(index >= args.length) { return; }


		// index 番目の引数を変数に置き換えない
		boolean containVar = false;
		for(int i=0;i<args.length;i++) {
			if(args[i] instanceof Var) {
				containVar = true;
				break;
			}
		}

		if(containVar) {
			Atom cand = new Atom(name,id,args,dataType);
			if(! cands.contains(cand)) {
				cands.add(cand); //リテラル追加
				}
		}
		genCandsWithConst(index+1,name,id,args,vars,cands);//再帰１



		// index 番目の引数を変数に置き換え
		Term[] newArgs = new Term[args.length];
		for(int i=0;i<args.length;i++) {
			if(index != i) {
				newArgs[i] = args[i];
				}
			}

		for(int i=0;i<vars.length;i++) {
			newArgs[index] = vars[i];
			Atom cand = new Atom(name,id,newArgs,dataType);
			if(! cands.contains(cand)) {
				cands.add(cand); //リテラル追加
				}
			genCandsWithConst(index+1,name,id,newArgs,vars,cands);//再帰２
		}
	}

	//=========================================================================
	// ◆ アイテムテーブルの出力
	//=========================================================================
	/**
	 * アイテムテーブルを出力します
	 *
	 * @param filename	ファイル名(なしの場合は標準出力)
	 * @throws Exception
	 */
	public void dumpItemTable(String filename) throws Exception {
		File file = new File(filename);
		if(! file.exists()) {
			file.createNewFile();
		}
		if(! file.canWrite()) {
			//System.err.println("ファイル\""+filename+"\"に書き込めません");
			return;
		}

		//System.out.println("ファイル出力中...");
		BufferedWriter bw = new BufferedWriter(
				new OutputStreamWriter(
						new FileOutputStream(file)));
		for(int i=0; i<items.length;i++) {
			String o = "itemtable("+i+","+items[i].toSwiString()+").\n";
	//		System.out.print(o);
			bw.write(o);
		}
		bw.close();
		//System.out.println("ファイル出力終了");
	}
	//=========================================================================
		// ◆ rule mining用 アイテムテーブルの生成
		//=========================================================================
		/**
		 * アイテムテーブルを生成します．
		 *
		 * @param	database	データベース
		 * @param	varNum		変数の数
		 * @param	noConst		定数の出現を許すか
		 */
		public void transAtoms(DataBase database,DatatypeDef dataType, int varNum,boolean noConst) {
			//System.out.println("rule mining用 ItemTableを生成中...");
			this.dataType = dataType;
			//アイテムのリスト
			ArrayList<Atom> atomsList	= new ArrayList<Atom>();
	
			// キーの名前を取得
			String keyName = database.getKeyName();
			// 変数のリストを生成
			Var[] vars = new Var[varNum];
			for(int i=0;i<varNum;i++) { vars[i] = new Var(i); }
	
			// キーアトムを最初に(ruleの先頭がキーアトム)
			atomsList.add(new Atom(keyName, 0, new Term[]{vars[0]}, dataType));	
			// 残りのアトムについて
			keyName += "/1";
	
			ArrayList<AtomData> atomsInConfig = database.getTranslator().patternData.atomList; // configファイルで与えられるatomのリスト， 各アトム： [atom, 0,1,2] atom(A,B,C)
			//String[] names = database.getPreds();//述語名取得	
			for(int i=1;i<atomsInConfig.size();i++) {	// 	i=1: キーアトムはすでに追加済み
				AtomData atomData = atomsInConfig.get(i);// ex. atom[0,1,2]
				//String[] atomTermList	= atomsInConfig.get(i); // atomのtermList表現： [atom, 0,1,2]
				String[] atomTermList = atomData.toString().split(",");
				int[] atomArgs = atomData.getArg();
				//int argSize		= atomTermList.length-1; // 引数の数
				int argSize = atomData.getLength();
				Term[] args = new Term[argSize];

				for(int arg_i=0;arg_i<argSize;arg_i++) {
					//Integer v_i = 
					//String v_id = atomTermList[arg_i+1]; // 0
					//args[arg_i]= vars[Integer.parseInt(v_id)]; //args[0] = A
					args[arg_i]= vars[atomArgs[arg_i]];
				}				
				atomsList.add(new Atom(atomTermList[0], i+1, args, dataType));	
			}	
			// (キーアトム以降をバイアスを考慮した順序にソート) 今回はしない
			items = atomsList.toArray(new Atom[0]);
			//Arrays.sort(items, 1, items.length);			
			itemSize = items.length;			
			//IDを登録
			for(int i = 0;i<this.itemSize;i++){
				this.items[i].setID(i);
			}	
			// 生成結果を標準出力
	//		System.out.println("完了");	
	//		for(int i = 0;i < cands.size();i++){
	//			System.out.println(i+":"+cands.get(i));
	//		}
	//		System.out.println("アイテム数: "+itemSize);
			
			//アトム->IDのハッシュマップを作成(05/15)
			NameToID = new ConcurrentHashMap<Integer, ConcurrentHashMap<String,Integer>>();
			for(int i=0, n=items.length;i<n;++i){
				Atom p = items[i];
				int nameID = p.getNameID();
				ConcurrentHashMap<String, Integer> map = NameToID.get(nameID);
				if(map == null){//内部ハッシュが作られていない
					ConcurrentHashMap<String, Integer> tempMap = new ConcurrentHashMap<String, Integer>();
					tempMap.put(p.toStringArg(), p.getID());
					NameToID.put(nameID, tempMap);
				}
				else{
					map.put(p.toStringArg(), p.getID());
				}
			}
		}
}
