package database;

import java.util.ArrayList;

/**
 * 型クラス
 * DatatypeDefクラスで用いられる.
 * @author sekilab
 *
 */
public class Type{

	//=========================================================================
	// ◆ 変数
	//=========================================================================

	/**
	 * データの型名
	 */
	private int type;

	/** 親ノード */
	private Type   parent;
	/** 子ノード */
	private ArrayList<Type>	children;

	/** 子ノードの数*/
	private int childNum = 0;

	//=========================================================================
	// ◆ コンストラクタ
	//=========================================================================
	Type(int type, Type parent){
		this.type = type;
		this.parent= parent;

		children = new ArrayList<Type>();

		// 親ノードがいる場合は登録
		if(parent != null) { parent.addChild(this); }
		// いない場合，自らがルートノードとして振る舞う
		// else { ROOT = this; }

	}

	//=========================================================================
	// ◆ GetterおよびSetterのメソッド
	//=========================================================================
	/**
	 * 型を返します
	 */
	public int getTypeName(){ return type; }

	/**
	 * 親ノードを取得します
	 *
	 * @return 親ノード
	 */
	public Type getParent() { return parent; }

	/**
	 * 子ノードのリストを取得します
	 *
	 * @return 子ノードのリスト
	 */
	public ArrayList<Type> getChildren() { return children; }

	/**
	 * 子ノードの数を取得します
	 *
	 * @return 子ノードの数
	 */
	public int getChildNum() { return childNum; }

	/**
	 * 子ノードを追加します
	 *
	 * @param node	追加するノード
	 */
	public void addChild(Type type) { children.add(type); childNum++; }

	//=========================================================================
	// ◆ isおよびhasのメソッド
	//=========================================================================
	/**
	 * このオブジェクトが子供typeを持つかどうか
	 * @param type
	 * @return
	 */
	public boolean hasChild(int type){
		for(int i=0; i < childNum; i++){
			if(children.get(i).getTypeName() == (type)) return true;
		}
			return false;
	}

}
