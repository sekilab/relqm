package database.obj;

import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;

import main.Mining;
import database.DatatypeDef;

public class Conjunction {

	//=========================================================================
	// ◆ 変数
	//=========================================================================
	/** 連言を構成するアトム */
	private Atom[]		atoms;
	/** 連言の長さ */
	private int		length;

	/** 含まれる項 */
	private Term[]		range;

	/** 含まれる変数 */
	private Var[]		vars;

	/** 出現集合 */
	private ArrayList<String[]>	occur;

	/** 連言に含まれる変数の型情報 */
	//private ConcurrentHashMap<Integer, Integer> typeinfo = new ConcurrentHashMap<Integer, Integer>();
	//private int[] typeinfo = new int[Mining.instance().getVarSize()];//
	private int[] typeinfo;
	public int getTypeSize(){return typeinfo.length;}
	private String data;
	private int maxVarInd	= 0;//変数の最大インデックス

	//=========================================================================
	// ◆ コンストラクタ
	//=========================================================================
	/**
	 * キーのみからなる連言を生成します
	 *
	 * @param key	キーアトム
	 * @param occur	キーのみの連言の出現集合
	 */
	public Conjunction(Atom key, ArrayList<String[]> occur) {
		this(new Atom[]{key}, occur);
		this.maxVarInd = key.getMaxVarInd();
		typeinfo = new int[this.maxVarInd+1];
		Term[] keyargs =key.getArgs();	//keyの変数
		for(int i = 0,n=keyargs.length; i < n; i++){
			// キーアトムの変数の型情報を連言cの型情報のハッシュテーブルに挿入する
			this.setTypeInfo(key.getArgsPattern()[i], key.getArgsType()[i]);
		}
		
	}
	public Conjunction(int max, Atom[] atoms, ArrayList<String[]>occur) {//closureRRppcから呼ばれる
		this.atoms		= atoms;
		this.occur		= occur;
		typeinfo = new int[max+1];
		setRangeVars();
	}
	/**
	 * アトムの配列から連言を生成します
	 * @param atoms	アトムの配列
	 * @param occur	出現集合
	 */
	public Conjunction(Atom[] atoms, ArrayList<String[]>occur) {
		this.atoms		= atoms;
		this.occur		= occur;

		setRangeVars();
	}

	/**
	 * 連言にアトムを1つ追加した連言を生成します
	 *
	 * @param c		元となる連言
	 * @param p		追加するアトム
	 * @param occur	出現集合
	 */
	public Conjunction(Conjunction c,Atom p, ArrayList<String[]> occur) {

		this.atoms = Arrays.copyOf(c.atoms, c.atoms.length+1);//cをコピー（配列の数は+1）

		this.atoms[c.atoms.length] = p;//ｐを追加

		this.occur = occur;

		int cMax = c.getMaxVarInd();
		int pMax = p.getMaxVarInd();
		int max = cMax;
		if(cMax < pMax){max = pMax;}
		this.typeinfo = Arrays.copyOf(c.getTypeInfo(), max+1);
		this.maxVarInd = max;
		//this.setTypeInfo(c.getTypeInfo().clone());//型情報

		// データベースの各述語の型を定義した配列
		int[] atomType = p.getArgsType();
		int[] atomPattern = p.getArgsPattern();
		for(int i = 0,n=atomPattern.length; i < n; i++){
			//連言cで型anyを持っているときは,pの第i引数の型情報をtypeinfoに追加する
			//if(c.getTypeInfo(atomPattern[i]) == 0){
			if(cMax < atomPattern[i]){//cが持っていない引数
				typeinfo[atomPattern[i]] = atomType[i];
			}
			//if(this.getTypeInfo(atomPattern[i]) == 0){//typeinfoの拡張された部分は0が入っていると想定
			//	typeinfo[atomPattern[i]] = atomType[i];
			//}
			// pの第i引数の型がanyであったら
			if(atomType[i] == 999){//999=any
				continue;
			}
			// cとpの変数の型が一致しているときだけこのコンストラクタを用いる
		}
		setRangeVars();
	}
	//=========================================================================
	// ◆ オーバーライドするメソッド
	//=========================================================================
	/**
	 * このオブジェクトの文字列表現を取得します
	 *
	 * @return 連言の文字列表現
	 */
	public String toString() {
		
		StringBuilder sb = new StringBuilder("[");

		sb.append(atoms[0]);
		for(int i=1;i<atoms.length;i++) {
			sb.append(", ");
			sb.append(atoms[i]);
		}
		sb.append("]");

		return sb.toString();
	}
	//=========================================================================
	// ◆ GetterおよびSetterのメソッド
	//=========================================================================
	public int getMaxVarInd() { return maxVarInd; }
	/**
	 * 連言が指定したアトムを含むかを返します
	 *
	 * @param atom	調べるアトム
	 * @return		atomが連言に含まれていれば true
	 */
	public boolean hasAtom(Atom atom) {

		for(int i=0;i<atoms.length;i++) {
			if(atoms[i]==atom) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 連言の長さを取得します
	 *
	 * @return	連言の長さ
	 */
	public int length() { return length; }

	/**
	 * 連言を構成するアトムの配列を取得します
	 *
	 * @return	連言を構成するアトム
	 */
	public Atom[] getAtoms() { return atoms; }

	/**
	 * 連言に含まれる項の配列を取得します
	 *
	 * @return	連言に含まれる項の配列
	 */
	public Term[] getRange() { return range; }

	/**
	 * 連言に含まれる変数の配列を取得します
	 *
	 * @return	連言に含まれる変数の配列
	 */
	public Var[] getVars() { return vars; }



	/**
	 * typeinfoに変数のインデクス(index)をキーとした型情報を入力します.
	 * @param index
	 * @param type
	 */
	public void setTypeInfo(int index, int type){
		typeinfo[index] = type;
	}

	/**
	 * 指定した変数のインデクス(index)をキーとした型情報を取得します.
	 * @param index
	 * @return
	 */
	public int getTypeInfo(int index){
		return typeinfo[index];
	}

	/**
	 * 連言の持つ変数の型情報をハッシュテーブルで取得します.
	 * @return
	 */
	public int[] getTypeInfo(){
		return typeinfo;
	}

	/**
	 * 連言の持つ変数の型情報をハッシュテーブルでセットします.
	 * @param typeinfo
	 */
	public void setTypeInfo(int[] typeinfo){
		this.typeinfo = typeinfo;
	}
	public void setMaxVar(int idx){
		this.maxVarInd = idx;
	}
	/**
	 * 連言の出現集合を取得します
	 *
	 * @return	連言の出現集合
	 */
	public ArrayList<String[]> getOccur() { return occur; }
	public ArrayList<String[]> getOccurString() { return occur; }
	/**
	 * 連言の出現集合を文字列型で返す
	 * @return 文字列型の出現集合
	 */
	public String getOccurStringArray(){
		String str = "";
		for(int i=0,n=occur.size();i<n;++i){
			String[] array = occur.get(i);
			str += (array[0]);
			for(int j=1,m=array.length;j<m;++j){
				str += ("," + array[j]);
			}
			str += ("\n");
		}
		return str;
	}

	public boolean hasAtom(int atom) {

		for(int i=0;i<atoms.length;i++) {
			if(atoms[i].getID()==atom) {
				return true;
			}
		}
		return false;
	}
	//=========================================================================
	// ◆ 項と変数の更新
	//=========================================================================
	/**
	 * range(項) と vars(変数) を更新します
	 *
	 * このメソッドはインスタンスが生成される時に1度だけ呼び出されます．
	 */
	private void setRangeVars() {

		ArrayList<Term> rangeList = new ArrayList<Term>();
		ArrayList<Var> varList = new ArrayList<Var>();

		// アトムを順に調べる
		for(int i=0;i<atoms.length;i++) {

			// アトムの項を順に調べる
			Term[] args = atoms[i].getArgs();

			for(Term arg : args) {

				if(! rangeList.contains(arg)) {//項に含まれていない
					rangeList.add(arg);
					}

				if(arg.getClass() == Var.class && ! varList.contains(arg)) {//変数に含まれていない
					varList.add((Var)arg);
				}
			}
		}

		//更新
		this.range	= rangeList.toArray(new Term[0]);
		this.vars	= varList.toArray(new Var[0]);
	}
	public void renewData( String data){
		this.data = data;
	}
	
	public String getData(){
		return this.data;
	}

}

