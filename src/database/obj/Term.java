package database.obj;



/**
 * 項の抽象クラスです
 *
 * @author sin8
 */
public abstract class Term implements Comparable<Term> {

	//=========================================================================
	// ◆ オーバーライドするメソッド
	//=========================================================================
	/**
	 * 項同士の順序比較を行います
	 */
	public int compareTo(Term term) {

		// this == 定数項
		if(this.getClass() == Const.class) {

			// 両方とも定数項
			if(term.getClass() == Const.class) {
				Const c0 = (Const)this;
				Const c1 = (Const)term;
				return c0.getName().compareTo(c1.getName());
			}

			// 相手は変数項
			else if(term.getClass() == Var.class) {
				return -1;
			}
		}

		// this == 変数項
		else if(this.getClass() == Var.class) {

			// 両方とも変数項
			if(term.getClass() == Var.class) {
				Var v0 = (Var)this;
				Var v1 = (Var)term;
				return v0.getIndex()-v1.getIndex();

			}
			// 相手は定数項
			else if(term.getClass() == Const.class) {
				return 1;
			}
		}
		return 0;
	}
	//=========================================================================
	// ◆ その他のメソッド
	//=========================================================================
	/**
	 * 項のProlog版文字列表現を取得します
	 *
	 * 実際の処理はこのクラスを継承するTerm, Varで実装します．
	 *
	 * @return	Prolog版文字列表現
	 */
	public abstract String toSwiString();
}
