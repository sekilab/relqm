package database.obj;

import java.util.ArrayList;
import java.util.Arrays;

import database.DatatypeDef;
import database.Type;

/**
 * アトムクラスです
 *
 * @author sin8
 */
public class Atom implements Comparable<Atom> {
	//=========================================================================
	// ◆ 変数
	//=========================================================================
	/** 述語名 */
	private String name;
	private int nameID;
	private int arity;
	private String key;
	
	/**	 ID */
	private int id = -1;
	
	/** 引数 */
	private Term[]	args;
	private int[]	argsPattern;//変数のインデックス
	private int[]	argsType;//0426追加:引数の型
	private Type[]	argsTypeArray;//0426追加:引数の型
	/** 引数に含まれる変数 */
	private Var[]	vars; // 変数はsortされている
	private Var[]  vars1;
	private Var[] nosortvars;
	/** 変数の最小インデックス(変数がない場合を-1としておく) */
	private int minVarInd	= -1;
	private int maxVarInd	= 0;

	//=========================================================================
	// ◆ コンストラクタ
	//=========================================================================
	/**
	 * アトムのインスタンスを生成します
	 *
	 * @param name	述語名
	 * @param args	引数の項配列
	 */
	public Atom(String name, int nameID, Term[] args, DatatypeDef dataType) {
		this.name = name;		
		this.nameID = nameID;
		this.arity= args.length;
		this.key=name+'/'+arity;
		this.args = Arrays.copyOf(args, arity);
		this.argsPattern = new int[this.arity];
		this.argsType = new int[this.arity];//0426追加
		this.argsTypeArray = new Type[this.arity];
		// 引数に含まれる変数を抽出. 同時にargsPatternも生成．各引数の変数indexを格納．引数が変数でないときは-999
		ArrayList<Var> varlist = new ArrayList<Var>();
		int index=0;
		for(int i=0;i<args.length;i++) {
			if(args[i].getClass() == Var.class) {
				if(! varlist.contains(args[i])) { 
					varlist.add((Var)args[i]);
					index=((Var)args[i]).getIndex();
					this.argsPattern[i] = index;
					if(index>this.maxVarInd){ 
						this.maxVarInd=index; // maxVarIndを更新
						} 
					}
			}
			else if(args[i].getClass() == Const.class){
				this.argsPattern[i]= -999; // 引数が定数の場合，負の数を入れる
			}
		}
		//型情報
		this.argsType = dataType.getDataDefName(this);
		this.argsTypeArray = dataType.getDataDef(this);
		
		
		vars = varlist.toArray(new Var[0]);
		nosortvars = varlist.toArray(new Var[0]);
		Arrays.sort(vars);
		// 引数に含まれる変数を抽出(重複あり)
		ArrayList<Var> list1 = new ArrayList<Var>();
		for(Term term : args) {
			if(term.getClass() == Var.class) {
				 list1.add((Var)term); 
			}
		}
		
		vars1 = list1.toArray(new Var[0]);
		Arrays.sort(vars1);
		
		// 変数の最小インデックスを取得
		if(vars.length != 0) {
			this.minVarInd = vars[0].getIndex();
		}

	}
	
	
	//=========================================================================
	// ◆ オーバーライドするメソッド
	//=========================================================================

	/**
	 * このオブジェクトと等価なオブジェクトかを判定します
	 *
	 * @param obj	判定対象のオブジェクト
	 * @return 	等価なオブジェクト(アトム)ならば true
	 */
	public boolean equals(Object obj) {
		if(obj instanceof Atom) {
			return this.name.equals(((Atom)obj).name) && Arrays.equals(this.args, ((Atom)obj).args);
		}
		else {
			return super.equals(obj);
		}
	}


	/**
	 * このオブジェクトの文字列表現を取得します
	 *
	 * @return アトムの文字列表現
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder(name);
		sb.append("(");
		sb.append(args[0]);
		for(int i=1;i<args.length;i++) { sb.append(","); sb.append(args[i]); }
		sb.append(")");

		return sb.toString();
	}
	public String toStringArg() {
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		sb.append(args[0]);
		for(int i=1;i<args.length;i++) { 
			sb.append(","); 
			sb.append(new Var(argsPattern[i]));
		}
		sb.append(")");

		return sb.toString();
	}
	public boolean hasVar(int x){
		for(int i=0,n=argsPattern.length;i<n;i++) {
			if(argsPattern[i] == x){return true;}
		}
		return false;
	}
	/**
	 * アトム同士で順序を比較します
	 *
	 * @param atom	比較対象のアトム
	 * @return		this < atom : -1 / this == atom : 0 / this > atom : 1
	 */
	public int compareTo(Atom atom) {
		/*
		 // 最小の変数インデックス
		{
			   if(this.minVarInd != atom.minVarInd) { return (this.minVarInd - atom.minVarInd); }
		}
		{              
		        //　引数の数の比較
		        int minVarSize1 = this.vars1.length;
		        int minVarSize2 = atom.vars1.length;
		        int minVarSize;
		        if(minVarSize1 > minVarSize2){ minVarSize = minVarSize2;}
		        else{ minVarSize = minVarSize1;}
		            
		        // 引数の変数の辞書順で比較
		 	    int result = 0;
			    for(int i=0;i<minVarSize;i++) {  
				if(this.vars1[i] == atom.vars1[i]){ result = 0; }
				else{ result = this.vars1[i].compareTo(atom.vars1[i]); }
				
				    if(result != 0){ break; }
					
				}
			    if(result == 0 && this.vars1.length > atom.vars1.length) { result = 1; }
			    if(result == 0 && this.vars1.length < atom.vars1.length) { result = -1; }
				//述語記号の辞書順
				if(result == 0 && this.vars1.length == atom.vars1.length && this.name != atom.name) 
				{ result = this.name.compareTo(atom.name); }
				//引数の辞書順
				if(result == 0 && this.vars1.length == atom.vars1.length && this.name == atom.name)
				{
				for(int j=0;j<this.args.length;j++){
					result = this.args[j].compareTo(atom.args[j]);	
					if(result != 0) { break; }
				}
					
				}
					
					return result;
			}

		
		*/
		
		
		//旧順序
		
		// 最小の変数インデックス
		{
			if(this.minVarInd != atom.minVarInd) { return (this.minVarInd - atom.minVarInd); }
		}

		// 辞書順
		{
			// 述語名で比較
			if(! this.name.equals(atom.name)) {
				return this.name.compareTo(atom.name);
			}

			else {
				// 引数の辞書順で比較
				int result = 0;
				for(int i=0;i<this.args.length;i++) {
					if(i == atom.args.length) { result = 1; }
					else {
						result = this.args[i].compareTo(atom.args[i]);
					}

					if(result != 0) { break; }
				}
				if(result == 0 && this.args.length < atom.args.length) { result = -1; }
				return result;
			}

		}
			}
	//=========================================================================
	// ◆ GetterおよびSetterのメソッド
	//=========================================================================
	/**
	 * 述語名を取得します
	 *
	 * @return 述語名
	 */
	public String getName() { return name; }
	public int getArity() { return arity; }
	public String getKey() { return key; }
	public int[] getArgsPattern() { return argsPattern; }
	public int[] getArgsType() { return argsType; }
	public Type[] getArgsTypeArray() { return argsTypeArray; }
	public int getMinVarInd() { return minVarInd; }
	public int getMaxVarInd() { return maxVarInd; }
	/**
	 * 引数を取得します
	 *
	 * @return アトムの引数の配列
	 */
	public Term[] getArgs() { return args; }
	/**
	 * アトムに含まれる変数を取得します
	 *
	 * @return アトムに含まれる変数の配列
	 */
	public Var[] getVars() { return vars; }
	public Var[] getnosortVars() { return nosortvars; }

	/**
	 * 先頭の引数項を取得します
	 *
	 * @return	第一引数
	 */
	public Term getHeadTerm() { return args[0]; }

	public int getID(){return this.id;}
	public int getNameID(){return this.nameID;}
	public void setID(int id){this.id = id;}
	
	//=========================================================================
	// ◆ その他のメソッド
	//=========================================================================
	/**
	 * アトムのProlog版文字列表現を取得します
	 *
	 * @return	Prolog版文字列表現
	 */
	public String toSwiString() {
		StringBuilder sb = new StringBuilder(name);
		sb.append("(");
		sb.append(args[0].toSwiString());
		for(int i=1;i<args.length;i++) { sb.append(","); sb.append(args[i].toSwiString()); }
		sb.append(")");

		return sb.toString();
	}
}



