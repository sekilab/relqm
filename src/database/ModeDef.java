package database;

import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

import database.obj.Atom;

public class ModeDef {

	//=========================================================================
	// ◆ 変数
	//=========================================================================
	/** 述語のモード情報を持つハッシュテーブル */
	private ConcurrentHashMap<String, String[]> modedef;


	//=========================================================================
	// ◆ コンストラクタ
	//=========================================================================
	public ModeDef(){
		this.modedef = new ConcurrentHashMap<String, String[]>();
	}

	//=========================================================================
	// ◆ GetterおよびSetterのメソッド
	//=========================================================================

	/**
	 * アトムのモード情報を挿入する
	 *
	 * @param key アトムの述語名
	 * @param mode モード情報
	 */
	public void setmoddef(String key, String[] mode){ modedef.put(key, mode); }

	/**
	 * アトムpのモード情報を取得する
	 *
	 * @param p モード情報を取得するアトム
	 * @return アトムpのモード情報
	 */
	public String[] getmoddef(Atom p){
		// ハッシュテーブルにキーを持つ場合, ハッシュテーブルからモード情報を取得
		if(modedef.contains(p.getName())){
			return modedef.get(p.getName());
		}
		// ハッシュテーブルにキーを持たない場合, デフォルトのモード情報を返す
		else{

			String[] strarray = new String[p.getArgs().length];

			// デフォルトとして第一引数のモードだけ"+", それ以外は"-"とした
			strarray[0] = "+";
			for(int i = 1; i < p.getArgs().length; i++){
				strarray[i] = "-";
			}
			/*for(int i = 0; i < p.getArgs().length; i++){
				strarray[i] = "-";
			}*/

			return strarray;

		}
	}

}
