package database;

import io.SwiFileLexer;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import translator.Translator;
import translator.Translator.Predicate;
import main.Mining;
import database.obj.Atom;
import database.obj.Const;
import database.obj.Term;
import database.obj.Var;

public class DataBase{

	//=========================================================================
	// ◆ 変数
	//=========================================================================
	/** 読み込んだデータベースのファイル名 */
	private String filename;

	/** configファイルの設定読み込みクラス */
	private Translator translator;
	
	/** データベースのConcurrentHashMap(述語名の1段階ハッシング) */
	private ConcurrentHashMap<String,Const[][]>					datas;
	/** データベースのConcurrentHashMap(述語名と第1引数の2段階ハッシング) */
	private ConcurrentHashMap<String,ConcurrentHashMap<Const,Const[][]>>	datas2;

	/** datasをConst[][]→String[][]に変換したもの */
	private ConcurrentHashMap<String,String[][]>					datasStr;
	/** データベースのConcurrentHashMap(述語名と第1引数の2段階ハッシング) */
	private ConcurrentHashMap<String,ConcurrentHashMap<String,String[][]>>	datas2Str;

	/** キーアトム名 */
	private String keyname;

	/** 述語名から各引数の種類(q,id,n)を得る 2016/11/13*/
	public ConcurrentHashMap<String,Integer[]>	name2type = new ConcurrentHashMap<String,Integer[]>();
	//=========================================================================
	// ◆ コンストラクタ
	//=========================================================================
	/**
	 * データを読み込んでいない空のインスタンスを生成します．
	 */
	public DataBase() {

		this.datas	= new ConcurrentHashMap<String,Const[][]>();
		this.datas2	= new ConcurrentHashMap<String,ConcurrentHashMap<Const,Const[][]>>();

		this.datasStr = new ConcurrentHashMap<String,String[][]>();
		this.datas2Str = new ConcurrentHashMap<String,ConcurrentHashMap<String, String[][]>>();

	}

	/**
	 * 指定されたファイルを読み込んでデータベースのインスタンスを生成します．
	 *
	 * @param filename		読み込むファイル名
	 * @throws Exception	ファイルの読み込みでエラーが生じた場合
	 */
	public DataBase(String filename) throws Exception {
		this();
		translator = new Translator(filename);
		translator.process();
		loadDataFile_csv(translator);//generated datasStr.
		//loadDataFile(filename);
		//datasTodatasStr();
		generate_datas2Str();
		datas2Todatas2Str();
		/*
		for(String predName : datas2Str.keySet()){
			System.out.println(predName);
			ConcurrentHashMap<String, String[][]> hash = datas2Str.get(predName);
			for(String firstValue : hash.keySet()){
				System.out.println("  " + firstValue);
				String[][] value = hash.get(firstValue);
				
				for(String[] tpl : value){
					String str = "";
					for(String atom : tpl){
						str += atom;
						str += " ";
					}
					//System.out.println("    " + str);
				}
			}
		}
		*/
	}


	//=========================================================================
	// ◆ GetterおよびSetterのメソッド
	//=========================================================================
	/**
	 * 生成したTranslatorを取得します．
	 *
	 * @return	translatorの実体
	 */
	public Translator getTranslator() { return translator; }
	
	/**
	 * データファイルのファイル名を取得します．
	 *
	 * @return	データファイルのファイル名
	 */
	public String getFileName() { return filename; }

	/**
	 * キーアトム名を取得します．
	 *
	 * @return	キーアトム名
	 */
	public String getKeyName() { return keyname; }


	/**
	 * データベース中の述語名の文字列配列を取得します．
	 *
	 * @return	データベース中の述語名の文字列配列
	 */
	public String[] getPreds() {
		return datasStr.keySet().toArray(new String[0]);
		//return datas.keySet().toArray(new String[0]);
	}


	/**
	 * データベース中のデータを取得します．
	 *
	 * @param key	データを取得する述語名
	 * @return		key に対応するデータ
	 */
	public Const[][] getDatas(String key) { return datas.get(key); }

	/**
	 * タプルargsがデータベース(ConcurrentHashMap)にあるかをチェック
	 * @param args
	 * @param ht
	 * @return
	 */
	private boolean isInDB(Term[] args, int[] pattern, String[] occtuple, ConcurrentHashMap<String,String[][]> ht){
		// 出現を確認
		String[][] data = null;

		data = ht.get(occtuple[pattern[0]]);

		// 定数(もしくは代入)なのにnullならデータベース中に存在しない
		if(data == null) { return false; } // タプルargsはデータベースにない

		for(String[] dtuple : data){// dtupleはデータベースの1タプル

			boolean flag = true; // タプルargsがデータベースのあるタプルとマッチした

			//for(int i=0, n=pattern.length; i < n; i++){ // patternの各要素についてループ
			for(int i=0 ; i < pattern.length; i++){ // patternの各要素についてループ

				if(pattern[i]>=0){ // 引数args[i]が旧変数の場合
					// 一致しなかった場合はbreak
					if(!(occtuple[pattern[i]].equals(dtuple[i]))) { flag = false; break; } // dtupleは一致しなかった
					}
				else if(pattern[i] == -999){ // 引数args[i]が定数の場合
					if(!(args[i].toString().equals(dtuple[i]))) { flag = false; break; }
				}
			}

			if(flag){return true;}
		}
		return false; // タプルargsにマッチするタプルはデータベースになかった
	}


	//=========================================================================
	// ◆ データ読み込みと変換
	//=========================================================================
	public void loadDataFile_csv(Translator trans) throws Exception {
		//System.out.print("ファイルの読み込みを開始...");

		keyname = trans.getKeyName();
		for(Predicate pred : trans.getPredList()){
			datasStr.put(pred.getName() + "/" + pred.getLength(), pred.getArray());
			name2type.put(pred.getName(), pred.getTypes());
		}

	}
	public void generate_datas2Str(){
		//datasStrのキー集合を取得
		for(String predName : datasStr.keySet()){
			ConcurrentHashMap<String,String[][]> hash_2nd = new ConcurrentHashMap<String,String[][]>();
			String[][] value = datasStr.get(predName);
			for(int i=0,len = value.length;i<len;++i){
				String[] tuple = value[i];
				String first = tuple[0];
				if(!hash_2nd.containsKey(first)){
					ArrayList<String[]> tempValue = new ArrayList<String[]>();
					while(true){
						tempValue.add(tuple);
						if(i+1 == len){break;}
						tuple = value[i+1];//次のタプルを取得
						if(!tuple[0].equals(first)){break;}//異なるキーを発見->break
						++i;
					}
					//ArrayList<String[]> -> String[][]
					int length = tuple.length;
					String[][] array = new String[tempValue.size()][length];
					for(int k=0;k<tempValue.size();++k){
						String[] tpl = tempValue.get(k);
						for(int j=0;j<length;++j){
							array[k][j] = tpl[j];
						}
					}
					hash_2nd.put(first, array);
				}
			}
			datas2Str.put(predName, hash_2nd);
		}
	}
	/**
	 * データファイルを読み込みます
	 *
	 * @param filename		データファイルのファイル名
	 * @throws	Exception	読み込みの際にエラーがあった場合返されます
	 */
	public void loadDataFile(String filename) throws Exception {

		//System.out.print("ファイルの読み込みを開始...");

		//ファイルの指定
		this.filename	= filename;
		File file = new File(filename);

		// ファイルの存在および読み込み可能かをチェック
		if(! file.exists() || ! file.canRead()) {
			System.err.println("ファイル\""+filename+"\"が読み込めません");
			return;
		}

		//inputStream -> data解析
		InputStreamReader in = new InputStreamReader(new FileInputStream(file));
		SwiFileLexer lex = new SwiFileLexer(in); //解析されたデータ

		ConcurrentHashMap<String,ConcurrentHashMap<Const,ArrayList<Const[]>>> hash =
			new ConcurrentHashMap<String,ConcurrentHashMap<Const,ArrayList<Const[]>>>();

		// アトムを次々と読み込む
		while(lex.advance()) {

			String key = lex.name();

			// キーアトムの指定

			if(key.equals("target/1")) {
				keyname = lex.args()[0].getName();
			}

			// アトムの追加
			else {
				Const[]	args	= lex.args();
				Const	head	= args[0];

				ConcurrentHashMap<Const,ArrayList<Const[]>> hash2 = null;
				if(! hash.containsKey(key)) {
					hash2 = new ConcurrentHashMap<Const,ArrayList<Const[]>>();
					hash.put(key, hash2);
				}
				else { hash2 = hash.get(key); }

				if(! hash2.containsKey(head)) {
					hash2.put(head,new ArrayList<Const[]>());
				}
				hash2.get(head).add(args);
			}
		}

		// 可変長配列のハッシュ → 固定長配列のハッシュ
		for(Iterator<String> i=hash.keySet().iterator();i.hasNext();) {
			String key = i.next();
			ConcurrentHashMap<Const,ArrayList<Const[]>> hash2 = hash.get(key);

			// 1次のみハッシュに追加するリスト
			ArrayList<Const[]> list = new ArrayList<Const[]>();

			// 保持データのハッシュを取得(なければ生成)
			ConcurrentHashMap<Const,Const[][]> data2 = null;
			if(! datas2.containsKey(key)) {
				data2 = new ConcurrentHashMap<Const,Const[][]>();
				datas2.put(key, data2);
			}
			else { data2 = datas2.get(key); }


			for(Iterator<Const> j=hash2.keySet().iterator();j.hasNext();) {
				Const term = j.next();
				ArrayList<Const[]> list2 = hash2.get(term);
				Collections.sort(list2, new Comparator<Const[]>(){
					public int compare(Const[] o1, Const[] o2) {
						for(int i=0;i<o1.length;i++) {
							// o2の終端に到達
							if(o2.length <= i) { return 1; }
							//
							if(o1[i] != o2[i]) { return o1[i].compareTo(o2[i]); }
						}
						// o1の終端に到達
						if(o1.length < o2.length) { return -1; }

						return 0;
					}
				});
				data2.put(term, list2.toArray(new Const[0][]));
				list.addAll(list2);
			}

			Collections.sort(list, new Comparator<Const[]>(){
				public int compare(Const[] o1, Const[] o2) {
					for(int i=0;i<o1.length;i++) {
						// o2の終端に到達
						if(o2.length <= i) { return 1; }
						//
						if(o1[i] != o2[i]) { return o1[i].compareTo(o2[i]); }
					}
					// o1の終端に到達
					if(o1.length < o2.length) { return -1; }

					return 0;
				}
			});
			datas.put(key,list.toArray(new Const[0][]));
		}

		// 解放処理を行う

		// 生成結果を標準出力
		//System.out.println("完了");
		//System.out.println("キー: "+keyname);
	}

	/**
	 * constからstrに変換
	 *
	 * @param ctuples 変換するconst
	 * @return 変換結果
	 */
	private String[][] const2str(Const[][] ctuples){

		String[][] stuples = new String[ctuples.length][ctuples[0].length];

		for(int i = 0; i < ctuples.length; i++){
			for(int j = 0; j < ctuples[i].length; j++){
				stuples[i][j] = ctuples[i][j].toString();
			}
		}
		return stuples;
	}

	/**
	 *  dataからdataStrに変換
	 */
	private void datasTodatasStr(){
		Enumeration<String> keys=datas.keys();

		while(keys.hasMoreElements()){
			String key = keys.nextElement();
			datasStr.put(key, const2str(datas.get(key)));
		}
	}

	/**
	 * data2からdata2Strに変換
	 */
	private void datas2Todatas2Str(){
		Enumeration<String> keys=datas2.keys();

		while(keys.hasMoreElements()){
			String key = keys.nextElement();

			ConcurrentHashMap<Const, Const[][]> ctuples = datas2.get(key);

			Enumeration<Const> ckeys= ctuples.keys();

			ConcurrentHashMap<String, String[][]> stuples = new ConcurrentHashMap<String, String[][]>();

			while(ckeys.hasMoreElements()){
				Const ckey = ckeys.nextElement();
				stuples.put(ckey.toString(), const2str(ctuples.get(ckey)));
			}

			datas2Str.put(key, stuples);
		}
	}

	//=========================================================================
	// ◆出現集合のカウント
	//=========================================================================

	/**
	 * キーアトムの出現集合を計算します．
	 *
	 * 現在の実装ではキーアトムの引数が1つのみと想定して実装しています．
	 *
	 * @return	キーアトムの出現集合
	 */
	public ArrayList<String[]> coversKey() {

		ArrayList<String[]> list = new ArrayList<String[]>();//出現集合のリスト

		String[][] data = datasStr.get(keyname+"/1");//keyリテラルのデータベースの取得
		//System.out.println("data:" + datasStr.toString());
		for(String[] args: data) {
			list.add(args);
			}//カウント

		//String[][] array = list.toArray(new String[0][]);//変換

		return list;
	}

	/**
	 * アトムの出現集合を計算します．
	 *
	 * ただし，連言に追加する際の出現集合のチェックを想定しているため，
	 * 第二引数の occur にマッチする出現集合のみが得られます．
	 * 
	 * @param atom		出現集合を計算する追加アトム
	 * @param occur		現在の出現集合
	 * @return			アトムの出現集合
	 */
	/*public String[][] coversAtom(Atom atom,String[][] occur) {
		if(occur.length == 0){
			return new String[0][];
		}
		// 連言にアトムatomを追加した時の出現集合のリスト
		ArrayList<String[]> list = new ArrayList<String[]>();

		String	key	= atom.getKey();//リテラルのkey (名前/arity)
		Term[]	args	= atom.getArgs();//リテラルの引数
		int argsL = args.length;
		int occArgsL = occur[0].length;
		int maxVar = 0;// 追加リテラル内の変数のインデックスで最大値(注： キーアトム内の変数index=0)		
		// 出現のパターン用の配列
		int[] pattern = new int[argsL];

		for(int i = 0; i < args.length; i++){ //出現パターンとmaxVarを設定
			if(args[i].getClass() == Var.class) {//引数は変数
				int index = ((Var)args[i]).getIndex(); //i番目の引数のインデックス
				if(index < occArgsL) { // 追加アトム引数args[i]が出現集合occurで既に代入されている場合
					pattern[i] = index;
				}
				else{ // 追加アトム引数args[i]が出現集合occurに現れない新変数の場合
					pattern[i] = -index; // すでに現れている変数と区別するために負の整数にする
					}
				if(index > maxVar){
					maxVar = index;}
			}
			else{ // 引数が定数の場合
				pattern[i] = 999; // 新しい変数の場合とぶつからないような数にしておくため, とり得ない整数を代入
			}
		}

		if( occArgsL> maxVar){ // 追加atom内に新変数がなかった
			long StartTime = System.currentTimeMillis();		
			
			//coversStringForBoundArgs(name, args, pattern, occur, list);
			//(String key,Term[] args, int[] pattern, ArrayList<String[]> occur, ArrayList<String[]> list)
			coversStringForBoundArgsUnf(key, args, pattern, occur, list);
			//Mining.instance().addTime(10, System.currentTimeMillis() - StartTime);

		}
		else{ //追加atom内に新変数がある
			long StartTime = System.currentTimeMillis();
			// 出現集合の各タプルに対しループ
			for(int i=0;i<occur.length;i++) {
				// 出現を確認 (新変数への代入を得るため String[][])				
				coversStringWithFreeArgs(key, args, pattern, occur[i], list, maxVar+1);				
			}
			//Mining.instance().addTime(11, System.currentTimeMillis() - StartTime);
			//Mining.instance().addCallCount(1, 1);
		}
		//String[][] array = list.toArray(new String[0][]);
		return list.toArray(new String[0][]);//array;
	}*/
	
	//0415追加 arrayList版coversAtom
	public ArrayList<String[]> coversAtomAL(Atom atom,ArrayList<String[]> occur) {
		if(occur.size() == 0){
			return new ArrayList<String[]>(0);
		}
		// 連言にアトムatomを追加した時の出現集合のリスト
		ArrayList<String[]> list = new ArrayList<String[]>();

		//String	name	= atom.getName();//リテラルの名前
		Term[]	args	= atom.getArgs();//リテラルの引数
		String key= atom.getKey();
		//int argsL = args.length;
		int occArgsL = occur.get(0).length;
		int maxVar=atom.getMaxVarInd();// 追加リテラル内の変数のインデックスで最大値(注： キーアトム内の変数index=0)		
		// 出現のパターン用の配列
		int[] pattern = atom.getArgsPattern();

		/*for(int i = 0; i < args.length; i++){ //出現パターンとmaxVarを設定
			if(args[i].getClass() == Var.class) {//引数は変数
				int index = ((Var)args[i]).getIndex(); //i番目の引数のインデックス
				if(index < occArgsL) { // 追加アトム引数args[i]が出現集合occurで既に代入されている場合
					pattern[i] = index;
				}
				else{ // 追加アトム引数args[i]が出現集合occurに現れない新変数の場合
					pattern[i] = -index; // すでに現れている変数と区別するために負の整数にする
					}
				if(index > maxVar){ // 現在のmaxVarより大きいindexなのでmaxVarを更新
					maxVar = index;}
			}
			else{ // 引数が定数の場合
				pattern[i] = 999; // 新しい変数の場合とぶつからないような数にしておくため, とり得ない整数を代入
			}
		}*/

		if( occArgsL> maxVar){ // 追加atom内に新変数がなかった
			long StartTime = System.currentTimeMillis();		
			
			//coversStringForBoundArgs(name, args, pattern, occur, list);
			coversStringForBoundArgsUnf(key, args, pattern, occur, list);
		}
		else{ //追加atom内に新変数がある
			long StartTime = System.currentTimeMillis();
			// 出現集合の各タプルに対しループ
			for(int i=0;i<occur.size();i++) {
				// 出現を確認 (新変数への代入を得るため String[][])				
				//coversStringWithFreeArgs(name, args, pattern, occur[i], list, maxVar+1);
				coversStringWithFreeArgsUnf(key, args, pattern, occur.get(i), list, occArgsL,maxVar+1);				
			}
			//Mining.instance().addTime(11, System.currentTimeMillis() - StartTime);
			//Mining.instance().addCallCount(1, 1);
		}
		//String[][] array = list.toArray(new String[0																												][]);
		//return list.toArray(new String[0][]);//array;
		return list;//array;
	}
	public void coversStringWithFreeArgsUnf(String key,Term[] args, int[] pattern, String[] occtuple, ArrayList<String[]> list,int occArgsL,int varSize) {

		String[][] data = null;// ハッシュテーブル用		
		if(pattern[0]>=0){//追加アトムの第一引数が確定している(定数or代入された変数)場合
			data = datas2Str.get(key).get(occtuple[pattern[0]]);
			if(data == null) {// 追加アトムの第一引数が確定しているのにnullならデータベース中に存在しない
				return;
			}
		}
		else { // そうでない場合は一段階ハッシュ(述語名だけ)
			data = datasStr.get(key);
		}
		
		int plength = pattern.length;

		for(int i=0, n=data.length; i<n;i++) {// data[i]は追加アトムのデータベースの1タプル			
			/*String[] newOcc	= new String[varSize];			
			for(int j = 0;j<varSize;j++){//occtuple-> newOccへ値のコピー			
				//newOcc[i] = null;
				if(j<occtuple.length){
					newOcc[j] = occtuple[j];
				}
			}*/
			/*if(!boundArgsChk(args, pattern, occtuple, data[i])){ //pattern内の束縛変数の代入をチェック，一致していないときは次のdtupleへ
				continue;
				}*/
			//
			boolean not_match = false; // 追加アトムのocctupleとdtupleがマッチしない場合にtrueとなるブール変数
			// patternの各引数に対しループ 
			String[] dtuple = data[i];
			for(int j=0;j<plength;j++) {
				if(occArgsL <= pattern[j]){// 新変数(自由変数)の時, 次の引数をチェック
					continue; }
				// 束縛引数の場合
				if(occArgsL> pattern[j]){ // 引数args[i]が旧変数pattern[i]の場合
					// 旧変数の出現タプルの値occtuple[pattern[j]] とdtuple[j]が一致しなかった場合はbreak (for-loopを抜ける)
					if(!(dtuple[j].equals(occtuple[pattern[j]]))) { not_match = true; break; }
				}
				else { // 引数args[i]が定数で，それがdtuple[i]と不一致の場合． 要修正
					if(!(dtuple[j].equals(args[j].toString()))) { not_match = true; break; }
				}
			}
			// 追加アトムの全ての束縛引数で，出現タプルの値とデータベースのタプルdtupleとが一致した場合
			if(not_match) { //Mining.instance().addCallCount(3, 1);   
				continue;} // 次のdtuple(data[i++])へ
			
			boolean newVar_flag	= true; // 追加アトム内の新変数の扱い用ブール変数
			// 追加アトムの各引数argsに対しループ
			String[] newOcc = Arrays.copyOf(occtuple, varSize);
			for(int j=0;j<plength;j++) {
				//if(pattern[j] < 0){ // 引数args[i]が新変数(自由)の場合
					int newVarIndex = pattern[j];					
					if(newOcc[newVarIndex]==null){// 新変数でatom内の最初の出現のとき
						boolean diff_flag = true;// 異なる変数には違う値を入れるという仮定
						for(int k = 0;k < varSize;k++){
							if(newOcc[k] != null && dtuple[j].equals(newOcc[k])){
								diff_flag = false;
								break;
							}
						}						
						if(diff_flag){ //異なる変数には違う値を入れる仮定が満たされているとき
							newOcc[newVarIndex] = dtuple[j];
						}
						else{
							newVar_flag = false;break; // for-loopを抜けて次のdtupleへ
						}
					}					
					// 新変数で2回目以降の出現の時，代入済みの値とタプルの値が一致しなかった場合はbreak
					else if(!(dtuple[j].equals(newOcc[newVarIndex]))) { newVar_flag = false; break; }
					//}
				/*else { // 引数args[i]が定数，あるいは束縛変数の場合(この場合はチェック済み) -> 次の引数へ
					//if(args[i].toString()!= dtuple[i]) { bool = false; break; }
					continue;
				}*/
			}			
			if(newVar_flag) {// 追加アトムの全ての引数がデータベースのタプルdtupleと一致した場合
				list.add(newOcc);
			}
		}
	}
	
	/*
	public void coversStringForBoundArgsUnf(String key,Term[] args, int[] pattern, ArrayList<String[]> occur, ArrayList<String[]> list) {	
		String[][] data = null;// ハッシュテーブル用
		String[] occtuple = null;
		//String key = name+"/"+pattern.length;
		int firstArg = pattern[0];
		//Hashtable<String,String[][]> data1Str = datas2Str.get(key);
		//final HashMap<String,String[][]> data1Str = datas2Str.get(key);
		final ConcurrentHashMap<String,String[][]> data1Str = datas2Str.get(key);
		// 出現集合の各タプルに対しループ

		for(int i=0, n=occur.size(); i<n;i++) {  					
			occtuple = occur.get(i); 
			// 追加アトムの第一引数は確定している(定数(999)or代入された変数), pattern[0]に入っている
			// 今回は代入された変数に限定, pattern[0]=変数のindex
			data = data1Str.get(occtuple[firstArg]);
			// 定数(もしくは代入)なのにnullならデータベース中に存在しない
			if(data == null) {continue;}	
			for(int k=0, dl=data.length; k<dl; k++) {// dtupleは追加アトムのデータベースの1タプル		
				String[] dtuple = data[k];
				boolean bool = true; // 追加アトムとdtupleがマッチする場合にtrueとなるブール変数
				for(int j=1, l=pattern.length;j<l;j++) { // patternの各引数に対しループ 
					int p_j = pattern[j];
					if(!(dtuple[j].equals(occtuple[p_j]))) { bool = false; break; }
				}
				if(bool) {	
					list.add(occtuple);	   		
					break; // 次のoccur[i]へ
				}
				else {continue;} // 次のdtupleへ
			}
		}					
	}
	*/
	/**
	 * 連言(その出現集合occur)にアトムname(args)を追加した時の出現集合を返す
	 *
	 * @param name　追加するリテラルの名前
	 * @param args 述語nameの引数. 引数がocctupleに現れているときは定数になっている(coversAtomで)
	 * @param pattern 引数の種類
	 * @param occur　代入済み
	 * @param list 出現集合のリスト
	 * @return
	 */
	public void coversStringForBoundArgs(String name,Term[] args, int[] pattern, String[][] occur, ArrayList<String[]> list) {

		/*String[][] data = null;// ハッシュテーブル用
		String[] occtuple = null;
		String key = name+"/"+pattern.length;*/
		// 出現集合の各タプルに対しループ
		for(int i=0, n=occur.length; i<n;i++) {  
			coversStringForBoundArgsBody(i, name,args, pattern,occur, list); 	
		}
	}

	public void coversStringForBoundArgsUnf(String key,Term[] args, int[] pattern, ArrayList<String[]> occur, ArrayList<String[]> list) {	
		String[][] data = null;// ハッシュテーブル用
		String[] occtuple = null;
		//String key = name+"/"+pattern.length;
		int firstArg = pattern[0];
		//Hashtable<String,String[][]> data1Str = datas2Str.get(key);
		//final HashMap<String,String[][]> data1Str = datas2Str.get(key);
		final ConcurrentHashMap<String,String[][]> data1Str = datas2Str.get(key);
		
		String pre_firstArg=occur.get(0)[firstArg];
		String[][] pre_data =  data1Str.get(pre_firstArg);; 
		// 出現集合の各タプルに対しループ
		for(int i=0, n=occur.size(); i<n;i++) {  					
			occtuple = occur.get(i); 
			// 追加アトムの第一引数は確定している(定数(999)or代入された変数), pattern[0]に入っている
			// 今回は代入された変数に限定, pattern[0]=変数のindex
			if(occtuple[firstArg].equals(pre_firstArg)){ 
				data=pre_data;
				}
			else{
				data = data1Str.get(occtuple[firstArg]);	
				pre_firstArg=occtuple[firstArg];
				pre_data = data;
			}
			
			// 定数(もしくは代入)なのにnullならデータベース中に存在しない
			if(data == null) {continue;}	
			for(int k=0, dl=data.length; k<dl; k++) {// dtupleは追加アトムのデータベースの1タプル		
				String[] dtuple = data[k];
				boolean bool = true; // 追加アトムとdtupleがマッチする場合にtrueとなるブール変数
				for(int j=1, l=pattern.length;j<l;j++) { // patternの各引数に対しループ 
					int p_j = pattern[j];
					if(!(dtuple[j].equals(occtuple[p_j]))) { bool = false; break; }
				}
				if(bool) {	
					list.add(occtuple);	   		
					break; // 次のoccur[i]へ
				}
				else {continue;} // 次のdtupleへ
			}
		}					
	}

/*	
	public void coversStringForBoundArgsUnf(String name,Term[] args, int[] pattern, String[][] occur, ArrayList<String[]> list) {
		String[][] data = null;// ハッシュテーブル用
		String[] occtuple = null;
		String key = name+"/"+pattern.length;
		int firstArg = pattern[0];
		ConcurrentHashMap<String,String[][]> data1Str = datas2Str.get(key);
		// 出現集合の各タプルに対しループ
		for(int i=0, n=occur.length; i<n;i++) {  	
			occtuple = occur[i]; 
			data = data1Str.get(occtuple[firstArg]);
			if(data == null) {
				continue;
			}
			for(int k=0, dl=data.length; k<dl; k++) {	
				String[] dtuple = data[k];
				boolean bool = true; 		
				for(int j=0, l=pattern.length;j<l;j++) {
					int p_j = pattern[j];
						if(!(dtuple[j].equals(occtuple[p_j]))) { bool = false; break; }
				}
				if(bool) {
					list.add(occtuple); 		
					break;// 次のoccur[i]へ
				} 
				else {continue;} // 次のdtupleへ
			}
		}							
	}
*/
	
	private void coversStringForBoundArgsBody(int i, String name,Term[] args, int[] pattern, String[][] occur, ArrayList<String[]> list) {				
		//long StartTime14 = System.currentTimeMillis();
		String[][] data = null;// ハッシュテーブル用
		String[] occtuple = null;
		String key = name+"/"+pattern.length;
		
		//long StartTime14 = System.currentTimeMillis();		
		//long StartTime = System.currentTimeMillis();							
		occtuple = occur[i]; 
		// 追加アトムの第一引数は確定している(定数(999)or代入された変数), pattern[0]に入っている
		// 今回は代入された変数に限定, pattern[0]=変数のindex
		data = datas2Str.get(key).get(occtuple[pattern[0]]);
		// 定数(もしくは代入)なのにnullならデータベース中に存在しない
		if(data == null) {
			return;}
		//Mining.instance().addTime(13, System.currentTimeMillis() - StartTime);
		//
		for(String[] dtuple : data) {// dtupleは追加アトムのデータベースの1タプル			
			boolean bool	= true; // 追加アトムとdtupleがマッチする場合にtrueとなるブール変数
			//
			//StartTime = System.currentTimeMillis();				
			bool = boundArgsChk(args, pattern, occtuple, dtuple);			
			//Mining.instance().addTime(12, System.currentTimeMillis() - StartTime);

			// 追加アトムの全ての引数で，出現タプルの値とデータベースのタプルdtupleとが一致した場合			
			if(bool) { list.add(occtuple);			
			break; // 次のoccur[i]へ
			}						
		}
		//Mining.instance().addTime(14, System.currentTimeMillis() - StartTime14);
		//Mining.instance().addCallCount(3, 1);
	}
	
	
	/**
	 * 連言(その出現集合の1タプルocctuple)にアトムname(args)を追加した時の出現集合を返す．追加アトムは2引数以上でも可
	 * @param name　追加するリテラルの名前
	 * @param args 述語nameの引数. 引数がocctupleに現れているときは定数になっている(coversAtomで)
	 * @param pattern 引数の種類
	 * @param occtuple　代入済み出現集合の一タプル
	 * @param list 出現集合のリスト
	 * @param varSize 追加アトムに現れる変数の数
	 */
	public void coversStringWithFreeArgs(String name,Term[] args, int[] pattern, String[] occtuple, ArrayList<String[]> list,int varSize) {

		String[][] data = null;// ハッシュテーブル用

		//追加アトムの第一引数が確定している(定数or代入された変数)場合
		if(pattern[0]>=0){
			data = datas2Str.get(name+"/"+pattern.length).get(occtuple[pattern[0]]);
			// 定数(もしくは代入)なのにnullならデータベース中に存在しない
			if(data == null) {
				return;
			}
		}
		else { // そうでない場合は一段階ハッシュ
			data = datasStr.get(name+"/"+pattern.length);
		}
		
		for(String[] dtuple : data) {// dtupleは追加アトムのデータベースの1タプル
			
			if(!boundArgsChk(args, pattern, occtuple, dtuple)){ //pattern内の束縛変数の代入をチェック，一致していないときは次のdtupleへ
				continue;
				}
	
			// 追加アトムの束縛引数はdtupleと一致していることは確認済み
			String[] newOcc	= new String[varSize];
			for(int i = 0;i<varSize;i++){//occtuple-> newOccへ値のコピー			
				//newOcc[i] = null;
				if(i<occtuple.length){
					newOcc[i] = occtuple[i];
				}
			}
			
			boolean bool	= true; // 追加アトムとdtupleがマッチする場合にtrueとなるブール変数

			// 追加アトムの各引数argsに対しループ
			for(int i=0;i<pattern.length;i++) {

				if(pattern[i] < 0){ // 引数args[i]が新変数(自由)の場合
					int index = -pattern[i];
					
					// 新変数でatom内の最初の出現のとき
					if(newOcc[index]==null){

						boolean diff_flag = true;// 異なる変数には違う値を入れるという仮定
						for(int j = 0;j < newOcc.length;j++){
							if(newOcc[j] != null && dtuple[i].equals(newOcc[j])){
								diff_flag = false;
								break;
							}
						}
						
						if(diff_flag){ //異なる変数には違う値を入れる仮定が満たされているとき
							newOcc[index] = dtuple[i];
						}
						else{
							bool = false;break; // for-loopを抜けて次のdtupleへ
						}
					}
					
					// 新変数で2回目以降の出現の時，代入済みの値とタプルの値が一致しなかった場合はbreak
					else if(!(dtuple[i].equals(newOcc[index]))) { bool = false; break; }
					}

				else { // 引数args[i]が定数，あるいは束縛変数の場合(この場合はチェック済み) -> 次の引数へ
					//if(args[i].toString()!= dtuple[i]) { bool = false; break; }
					continue;
				}
			}

			// 追加アトムの全ての引数がデータベースのタプルdtupleと一致した場合
			if(bool) {
				list.add(newOcc);
			}
		}
	}

	/**
	 * 連言の出現occtupleと追加アトムのタプルdtupleがそのアトムの束縛変数についてマッチするかをチェックする
	 * @param args 追加アトムの引数
	 * @param pattern 引数の種類，定数(999が入っている)，束縛変数のindex: 正整数または0， 自由変数：負整数
	 * @param occtuple　連言の出現
	 * @param dtuple 追加アトムのデータベースの一タプルdtuple
	 * @return
	 */
	private boolean boundArgsChk(Term[] args, int[] pattern, String[] occtuple, String[] dtuple) {

		boolean bool	= true; // 追加アトムとdtupleがマッチする場合にtrueとなるブール変数
		// patternの各引数に対しループ 
		for(int i=0;i<pattern.length;i++) {
			if(0 > pattern[i]){// 新変数(自由変数)の時, 次の引数をチェック
				continue; }
			// 束縛引数の場合
			if(999> pattern[i]){ // 引数args[i]が旧変数pattern[i]の場合
				// 旧変数の出現タプルの値occtuple[pattern[j]] とdtuple[j]が一致しなかった場合はbreak (for-loopを抜ける)
				if(!(dtuple[i].equals(occtuple[pattern[i]]))) { bool = false; break; }
			}
			else { // 引数args[i]が定数で，それがdtuple[i]と不一致の場合
				if(!(dtuple[i].equals(args[i].toString()))) { bool = false; break; }
			}
		}
		// 追加アトムの全ての束縛引数で，出現タプルの値とデータベースのタプルdtupleとが一致した場合
		if(bool) { //Mining.instance().addCallCount(3, 1);   
			return true;}
		else {return false;		
		}
	}

	/**
	 * 連言(その出現集合の1タプルocctuple)にアトムname(args)を追加した時の出現集合を返す
	 *
	 * @param name　追加するリテラルの名前
	 * @param args 述語nameの引数. 引数がocctupleに現れているときは定数になっている(coversAtomで)
	 * @param pattern 引数の種類
	 * @param occtuple　代入済み
	 * @param list 出現集合のリスト
	 * @return
	 */
/*	public void coversString2var(String name,Term[] args, int[] pattern, String[] occtuple, ArrayList<String[]> list) {

		String[][] data = null;// ハッシュテーブル用

		// 追加アトムの第一引数が確定している(定数or代入された変数)場合
		if(pattern[0]>=0){

			data = datas2Str.get(name+"/"+pattern.length).get(occtuple[pattern[0]]);

			// 定数(もしくは代入)なのにnullならデータベース中に存在しない
			if(data == null) {
				return;
				}
		}
		else { // そうでない場合は一段階ハッシュ
			data = datasStr.get(name+"/"+pattern.length);
		}

		for(String[] dtuple : data) {// dtupleは追加アトムのデータベースの1タプル

			String[] newOcc	= occtuple; // アトムを追加後の連言の出現集合の1タプル

			boolean bool	= true; // 追加アトムとdtupleがマッチする場合にtrueとなるブール変数

			// 追加アトムの引数argsに対しループ
			for(int i=0;i<pattern.length;i++) {

				// 引数args[i]が代入のない新しい変数の場合(coversAtomでチェック済み)
				if(pattern[i] < 0 && pattern[i] > -999) {

					int ind = -pattern[i];

					newOcc = Arrays.copyOf(newOcc, Math.max(newOcc.length, ind+1));//更新

					newOcc[ind] = dtuple[i]; // データベースのタプル第i引数の値をnewOccのindに代入
				}

				else if(pattern[i]>=0){ // 引数args[i]が旧変数の場合
					// 一致しなかった場合はbreak
					if(occtuple[pattern[i]] != dtuple[i]) { bool = false; break; }
				}
				else { // 引数args[i]が定数の場合
					if(args[i].toString()!= dtuple[i]) { bool = false; break; }
				}
			}
			// 追加アトムの全ての引数がデータベースのタプルdtupleと一致した場合
			if(bool) {
				list.add(newOcc);
			}
		}
	}
*/
	/**
	 * 連言(その出現集合の1タプルocctuple)にアトムname(args)を追加した時の出現集合を返す
	 * ２変数以上
	 *
	 * @param name　追加するリテラルの名前
	 * @param args 述語nameの引数. 引数がocctupleに現れているときは定数になっている(coversAtomで)
	 *
	 * @param pattern 引数の種類
	 *
	 * @param occtuple　代入済み
	 * @param list 出現集合のリスト
	 * 
	 * @param varSize 現れる変数の数
	 * 
	 * @return
	 */
/*	public void coversString(String name,Term[] args, int[] pattern, String[] occtuple, ArrayList<String[]> list,int varSize) {

		String[][] data = null;// ハッシュテーブル用

		// 追加アトムの第一引数が確定している(定数or代入された変数)場合
		//if(pattern[0]>=0){

			//data = datas2Str.get(name+"/"+pattern.length).get(occtuple[pattern[0]]);

			// 定数(もしくは代入)なのにnullならデータベース中に存在しない
//			if(data == null) {
//				return;
			//	}
//		}
	//	else { // そうでない場合は一段階ハッシュ
			data = datasStr.get(name+"/"+pattern.length);
		//}
		
		for(String[] dtuple : data) {// dtupleは追加アトムのデータベースの1タプル

			String[] newOcc	= new String[varSize];

			for(int i = 0;i<varSize;i++){//データのコピー
			
				newOcc[i] = null;
				if(i<occtuple.length){
					newOcc[i] = occtuple[i];
				}
			}
			
			boolean bool	= true; // 追加アトムとdtupleがマッチする場合にtrueとなるブール変数

			// 追加アトムの引数argsに対しループ
			for(int i=0;i<pattern.length;i++) {

				if(pattern[i]>=0){ // 引数args[i]が変数の場合
					
					//代入前
					if(newOcc[pattern[i]]==null){

						boolean def_frag = true;//違う変数には違う値を入れる
						
						for(int j = 0;j < newOcc.length;j++){
						
							if(dtuple[i] == newOcc[j]){
								def_frag = false;
								break;
							}
						}
						
						if(def_frag){
							newOcc[pattern[i]] = dtuple[i];
						}
						else{
							bool = false;break;
						}
					}
					
					// 代入済みの値とタプルの値が一致しなかった場合はbreak
					else if(newOcc[pattern[i]] != dtuple[i]) { bool = false; break; }
					}

				else { // 引数args[i]が定数の場合
					if(args[i].toString()!= dtuple[i]) { bool = false; break; }
				}
			}

			// 追加アトムの全ての引数がデータベースのタプルdtupleと一致した場合
			if(bool) {
				list.add(newOcc);
			}
		}
	}*/

	//=========================================================================
	// ◆ 飽和
	//=========================================================================
	/**
	 * 追加アトムが連言の出現集合を変えないかをチェックする
	 *
	 * @param atom	出現集合を見るアトム
	 * @param occur	アトムが追加される連言の出現集合
	 * @return
	 */
	public boolean isInClo(Atom atom, ArrayList<String[]> occur){

		String	key	= atom.getKey();
		Term[]	args	= atom.getArgs();

		// 追加アトムの二段階ハッシュテーブルを取得(データベースアクセス)
		//Hashtable<String,String[][]> ht = datas2Str.get(key);
		final ConcurrentHashMap<String,String[][]> ht = datas2Str.get(key);
		
		// 出現のパターン用の配列
		//int[] pattern = new int[args.length];
		int[] pattern = atom.getArgsPattern();

		/*for(int i = 0; i < args.length; i++){

			if(args[i].getClass() == Var.class) {

				int index = ((Var)args[i]).getIndex();

				if(index < occur[0].length) { // 追加アトム引数args[i]が出現集合occurで既に代入されている場合
					pattern[i] = index; 	  // 閉包計算なので新変数無し
				}
			}

			// 引数が定数の場合
			else{
				pattern[i] = -999; // 新しい変数の場合とぶつからないような数にしておくため
			}
		}*/

		// 出現集合の各タプルに対しループ
		for(int i=0,n=occur.size();i<n;i++) {
			// 出現を確認
			boolean flag =isInDB(args, pattern, occur.get(i), ht);
			if(flag){ // タプルoccur[i]がデータベースhtに出現する

				continue;
			}
			else{ return false;} // 出現集合が異なるのでアトムatomは閉包に入らない
		}
		return true; // 出現集合の全てのタプルが追加アトムatomのデータベースに出現するのでatomは閉包に入る
	}

	//=========================================================================
	// ◆ キー飽和
	//=========================================================================
	/**
	 * 追加アトムが連言のキーアトムの出現集合の変えないかをチェックする
	 *
	 * @param atom	出現集合を見るアトム
	 * @param occur	アトムが追加される連言の出現集合
	 * @return
	 */
	public boolean isInKeyClo(Atom atom, ArrayList<String[]> occur){

		String	name	= atom.getName();
		Term[]	args	= atom.getArgs();

		// 出現のパターン用の配列
		int[] pattern = new int[args.length];

		for(int i = 0; i < args.length; i++){

			if(args[i].getClass() == Var.class) {

				int index = ((Var)args[i]).getIndex();

				if(index < occur.get(0).length) { // 追加アトム引数args[i]が出現集合occurで既に代入されている場合
					pattern[i] = index; 	  // 閉包計算なので新変数無し
				}
			}

			// 引数が定数の場合
			else{
				pattern[i] = -999; // 新しい変数の場合とぶつからないような数にしておくため
			}
		}

		// 追加アトムの二段階ハッシュテーブルを取得(データベースアクセス)
		//Hashtable<String,String[][]> ht = datas2Str.get(name+"/"+args.length);
		ConcurrentHashMap<String,String[][]> ht = datas2Str.get(name+"/"+args.length);

		String prekeyval = "";   // 直前にチェックしたキーアトムの値
		boolean inkeyClo = true; // チェックしたキーアトムの値はDBにマッチしていることが分かっている

		// 出現集合の各タプルに対しループ
		for(int i=0;i<occur.size();i++) {

			if(occur.get(i)[0] != prekeyval && !inkeyClo){ //新しいキーアトム値になり, 直前のキーアトム値がDBにマッチしなかった
				return false;
			}

			else if(!(occur.get(i)[0].equals(prekeyval))  && inkeyClo){ // 新しいキーアトム値になり, 直前のキーアトム値がDBにマッチした
				if(isInDB(args, pattern, occur.get(i), ht)){ // タプルがDBに出現する
					prekeyval = occur.get(i)[0];
					inkeyClo  = true;
					continue; // 次のタプルへ
				}

				else{ // タプルがDBに出現しない
					prekeyval = occur.get(i)[0];
					inkeyClo  = false;
					continue; // 次のタプルへ
				}
			}

			else if(occur.get(i)[0].equals(prekeyval)){ // キーアトム値が直前のタプルと同じ場合
				if(inkeyClo){continue;} // キーアトム値がすでにDBにマッチしている場合
				else{ inkeyClo = isInDB(args, pattern, occur.get(i), ht); } // キーアトム値がDBにマッチしていない場合, DBに問い合わせる
			}
		}

		// 全てのタプルに対して判定を行った結果
		return inkeyClo;
	}

}