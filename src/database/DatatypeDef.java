package database;


import database.Type;
import database.obj.Atom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

import translator.Translator;
import translator.Translator.Predicate;

/**
 * 　要書換え
 *
 * データベースの各述語の引数の型を定義するクラスです。
 * 例えば, p(_, _)という2引数の述語があるとき, 第一引数と第二引数の型を
 * 定義します.
 * これによってpに対する型検査が可能になります.
 *
 * @author sekilab
 *
 **/
public class DatatypeDef {
	//=========================================================================
	// ◆ 変数
	//=========================================================================
	/* 型定義のハッシュテーブル */
	//private Hashtable<String, Type[]> typetable; // キーに述語名, 値に述語の型の配列を持つハッシュテーブル
	private ConcurrentHashMap<String, Type[]> typetable; // キーに述語名, 値に述語の型の配列を持つハッシュテーブル
	

	//=========================================================================
	// ◆ コンストラクタ
	//=========================================================================
	/**
	 * データを読み込んでいない空のハッシュテーブルを生成し、述語の型定義を
	 * ハッシュテーブルに入れていく．
	 *
	 * ※要修正
	 *   現段階ではコンストラクタで述語の型を与えているが, データベースから読み込んだ際に各述語の型情報を得られることから
	 *   コンストラクタで型を与えるのではなく, データベースが読み込まれたときに型情報を得ることが望ましい.
	 */
	public DatatypeDef(String key){
		typetable = new ConcurrentHashMap<String, Type[]>();



		// muta_term2.txtのDBの型情報
		/*Type type1 = new Type("matter_id",null);
		Type type2 = new Type("atom_id", null);
		Type type3 = new Type("elem_symbol", null);
		Type type4 = new Type("float",null);
		Type type5 = new Type("int",null);
		setDataDef("active", new Type[]{type1});
		setDataDef("atom", new Type[]{type1, type2});
		setDataDef("bond", new Type[]{type2, type2});
		setDataDef("elem", new Type[]{type2, type3});
		setDataDef("charge", new Type[]{type2, type4});
		setDataDef("no_of_benzenes", new Type[]{type1, type5});
		setDataDef("no_of_carbon_5_aromatic_rings", new Type[]{type1, type5});
		setDataDef("no_of_carbon_5_rings", new Type[]{type1, type5});
		setDataDef("no_of_carbon_6_rings", new Type[]{type1, type5});
		setDataDef("no_of_hetero_aromatic_5_rings", new Type[]{type1, type5});
		setDataDef("no_of_hetero_aromatic_6_rings", new Type[]{type1, type5});
		setDataDef("no_of_nitros", new Type[]{type1, type5});
		setDataDef("no_of_methyls", new Type[]{type1, type5});
		*/
		
		if(key.equals("active")){setDataDef_muta();}
		else if(key.equals("english")){setDataDef_eng();}
		//setDataDef("lumo",new Type[]{type1, type3}); //lumo(d1,-1.246).
		//setDataDef("logp",new Type[]{type1, type3}); //logp(d1,4.23).
	}

	/** configファイル対応版コンストラクタ 2017/01/27 */
	public DatatypeDef(Translator trans){
		typetable = new ConcurrentHashMap<String, Type[]>();
		setDataDefForConfig(trans);
	}
	private void setDataDefForConfig(Translator trans){
		//<a,type1>,<b,type2>,...
		ConcurrentHashMap<String, Type> typeHash = new ConcurrentHashMap<String, Type>();
		ArrayList<Predicate> predList = trans.getPredList();//述語リスト取得
		for(Predicate pred : predList){
			String typeDefStr = pred.getTypeDef();
			if(typeDefStr == null){continue;}
			String name = pred.getName();
			String[] typeDef = typeDefStr.split(":");//a:b:b -> {a,b,b}
			Type[] typeArray = new Type[typeDef.length];
			for(int i = 0;i < typeDef.length;++i){
				String var = typeDef[i];
				Type type = typeHash.get(var);
				if(type == null){
					int num = typeHash.size();
					type = new Type(num,null);
					typeHash.put(var, type);
				}
				typeArray[i] = type;
			}
			//System.out.println(name + " : " + Arrays.toString(typeArray));
			setDataDef(name,typeArray);
		}
	}
	final private void setDataDef_muta(){
		//System.out.println(" dataType muta");
		// muta_original.txtのDBの型情報
		/*
		Type type1 = new Type("matter_id",null);
		Type type2 = new Type("atom_id", null);
		Type type3 = new Type("elem_symbol", null);
		Type type4 = new Type("float",null);
		Type type5 = new Type("int",null);
		Type type6 = new Type("negfloat",null);
		*/
		Type type_m1 = new Type(1,null);//キーに1を入れるよう変更(05/13)
		Type type_m2 = new Type(2, null);
		Type type_m3 = new Type(3, null);
		Type type_m4 = new Type(4,null);
		Type type_m5 = new Type(5,null);
		Type type_m6 = new Type(6,null);
		setDataDef("active", new Type[]{type_m1});  //active(d1).
//		setDataDef("atm", new Type[]{type1, type2, type3, type5, type4});  //atm(f6,f6_23,h,3,0.106).
		setDataDef("bond", new Type[]{type_m1, type_m2, type_m2, type_m5}); //bond(f6,f6_17,f6_23,1).
		setDataDef("act",new Type[]{type_m1, type_m4});  //act(d1,2.11).
		setDataDef("lumo",new Type[]{type_m1, type_m6}); //lumo(d8, -1.437).
		setDataDef("ind1",new Type[]{type_m1, type_m4}); //ind1(d1,1.0).
		setDataDef("inda",new Type[]{type_m1, type_m4}); //inda(d189,0.0).
		setDataDef("logp",new Type[]{type_m1, type_m4}); //logp(d8, 3.46).
		
		setDataDef("atm_c", new Type[]{type_m1, type_m2});  //atm_c(d1,d1_1).
		setDataDef("atm_cl", new Type[]{type_m1, type_m2});  //atm_c(d1,d1_1).
		setDataDef("atm_f", new Type[]{type_m1, type_m2});  //atm_c(d1,d1_1).
		setDataDef("atm_h", new Type[]{type_m1, type_m2});  //atm_h(d1,d1_7).
		setDataDef("atm_o", new Type[]{type_m1, type_m2});  //atm_o(d1,d1_25).
		setDataDef("atm_br", new Type[]{type_m1, type_m2});  //atm_o(d1,d1_25).
		setDataDef("atm_n", new Type[]{type_m1, type_m2});  //atm_o(d1,d1_25).
		setDataDef("atm_s", new Type[]{type_m1, type_m2});  //atm_o(d1,d1_25).
		
		setDataDef("bond_single", new Type[]{type_m1, type_m2, type_m2}); //bond_single(d1,d1_1,d1_7).
		setDataDef("bond_double", new Type[]{type_m1, type_m2, type_m2}); //bond_double(d1,d1_24,d1_25).
		setDataDef("bond_benzene", new Type[]{type_m1, type_m2, type_m2}); //bond_benzene(d1,d1_1,d1_2).

		setDataDef("ind1_0",new Type[]{type_m1}); //ind1_0(d2).
		setDataDef("ind1_1",new Type[]{type_m1}); //ind1_1(d1).

		setDataDef("inda_0",new Type[]{type_m1}); //inda_0(d1).
		setDataDef("inda_1",new Type[]{type_m1}); //inda_1(d23).
	}
	final private void setDataDef_eng(){
		//System.out.println(" dataType eng");
		// english3369.txtのDBの型情報
		/**
		Type type1 = new Type("tag_no",null);
		Type type2 = new Type("sent_id", type1);
		Type type3 = new Type("non_sid_tag_no", type1);
		Type type4 = new Type("wd",null);
		setDataDef("english", new Type[]{type2});
		setDataDef("word", new Type[]{type1, type4});
		setDataDef("x", new Type[]{type1, type3});
		setDataDef("np", new Type[]{type1, type3});
		setDataDef("pp", new Type[]{type1, type3});
		setDataDef("s", new Type[]{type1, type3});
		setDataDef("vp", new Type[]{type1, type3});
		*/
		Type type_e0 = new Type(1,null);
		Type type_e1 = new Type(2, type_e0);
		Type type_e2 = new Type(3,null);
		setDataDef("english", new Type[]{type_e0});
		setDataDef("word", new Type[]{type_e0, type_e2});
		setDataDef("x", new Type[]{type_e0, type_e1});
		setDataDef("np", new Type[]{type_e0, type_e1});
		setDataDef("pp", new Type[]{type_e0, type_e1});
		setDataDef("s", new Type[]{type_e0, type_e1});
		setDataDef("vp", new Type[]{type_e0, type_e1});
	}
	
	//=========================================================================
		// ◆ GetterおよびSetterのメソッド
		//=========================================================================
	/** ハッシュテーブルにpredicate(述語名)をキーとしてtermdef(引数の型定義)を挿入する
	 * */
	public void setDataDef(String predicate, Type[] termdef){
		typetable.put(predicate, termdef);
	}

	/** アトムpの各引数の型を配列で返す.ハッシュテーブルに型がない場合は"any"を返す.
	 * */
	
	public int[] getDataDefName(Atom p){
		int[] stringarray = new int[p.getArgs().length];

		/* ハッシュテーブルにキーを持つ場合 */
		if(typetable.containsKey(p.getName())){
			/* 引数の数が一致する場合 */
			if(typetable.get(p.getName()).length == p.getArgs().length){
				for(int i= 0,n=p.getArgs().length; i < n; i++){
					stringarray[i] = typetable.get(p.getName())[i].getTypeName();
				}
			}
			else{
				for(int i = 0; i < p.getArgs().length; i++)
					stringarray[i]= 999;//anyの代わりにとりあえず999
			}

			return stringarray;
		}

		/* ハッシュテーブルにキーを持たない場合 */
		else{
			for(int i = 0; i < p.getArgs().length; i++)
				stringarray[i]= 999;
			return stringarray;
		}
	}

	public Type[] getDataDef(Atom p){
		/* ハッシュテーブルにキーを持つ場合 */
		if(typetable.containsKey(p.getName())){
		 return typetable.get(p.getName());
		}

		/* ハッシュテーブルにキーを持たない場合 */
		else{
			Type[] array = new Type[p.getArgs().length];
			for(int i = 0; i < p.getArgs().length; i++)
				array[i]= new Type(999,null);
			return array;
		}
	}

}