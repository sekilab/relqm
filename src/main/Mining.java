package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

import result.ResultData;
import database.DataBase;
import database.DatatypeDef;
import database.ItemTable;
import database.ModeDef;
import database.obj.Conjunction;
import database.obj.Atom;
import ffLCM.FfLCM;


/**
 * プログラム全体を統括するクラス
 *
 * @author sin8
 */
public class Mining {

	//=========================================================================
	// ◆ 変数
	//=========================================================================
	/** シングルトンインスタンス */
	private static Mining ins = new Mining();

	// マイニングパラメータ

	/** データベース */
	private DataBase	database;

	/** 最小頻度閾値 */
	private int		minFreq	= 1;
	private int		minFreqPercent	= 0;


	/** 出現する変数の上限数 */
	private int		varSize			= 2;
	/** 定数の出現フラグ */

	private boolean	noConst			= false;

	/** リテラル集合(アイテムテーブル) */
	private ItemTable	itemtable;

	/** アトムのモード情報 */
	private ModeDef modedef;

	/** 述語の引数の型情報 */
	private DatatypeDef datatypedef;

	/** マイニングクラス */
	private FfLCM		ffLCM;

	/** マイニングスレッド */
	private Thread		Thread;

	/** モード 0:Se-Extention  1:ffLCM */
	private int mood = 1;
	
	private int parallelMode = 3;
	public void setParallelMode(int m){parallelMode = m;}
	private int parallelThread = 2;
	public void setParallelThread(int t){parallelThread = t;}
	private int parallelDepth = 2;
	public void setParallelDepth(int d){parallelDepth = d;}
	
	//動的分割数
	private int divisionNumber = 0;
	/** 飽和連言のリスト */
	private ArrayList<Conjunction> outPuts;
	private ArrayList<ResultData> closureList = new ArrayList<ResultData>();//最終的なResultDataを格納
	
	static long system_start_time;

	//=========================================================================
	// ◆ コンストラクタ
	//=========================================================================
	/**
	 * 空のデータベースとアイテムテーブルを持つメインクラスを生成します
	 */
	private Mining() {

		this.database	= new DataBase();
		this.itemtable	= null;
		this.modedef	= null;
		this.datatypedef = null;
		
		
	}
	
	public void Start(String fileName,int varsize,int minsup,boolean noconst) throws InterruptedException{
		long StartTime;
		StartTime = System.currentTimeMillis();
		try {
			this.database	= new DataBase(fileName);//ファイル読み込み
		} catch (Exception e) {

			e.printStackTrace();
		}
		//System.out.println("\n make database time: "+ (System.currentTimeMillis() - StartTime));
		
		this.varSize = varsize;
		this.minFreqPercent = minsup;
		this.noConst = noconst;
		if(fileName.contains("config")){
			//configファイルのtypedefを適用させる
			this.datatypedef = new DatatypeDef(this.database.getTranslator());
		}
		else{this.datatypedef = new DatatypeDef(this.database.getKeyName());}
		
		this.itemtable	= new ItemTable();
		this.modedef	= new ModeDef();
		
		
		// アイテムテーブル生成
		//StartTime = System.currentTimeMillis();
		if(this.database.getTranslator().patternData.atomList.size() == 0){
			//pattern情報なし 通常のitemtable作成
			this.itemtable.makeCands(this.database, this.datatypedef, this.varSize, this.noConst);
		}
		else{
			//pattern情報あり 専用のitemtable作成
			System.out.println("\n Making pattern-specific itemtable ...");
			this.varSize = this.database.getTranslator().patternData.getMaxIndex();
			this.itemtable.transAtoms(this.database, this.datatypedef, this.varSize, this.noConst);
		}
		
		System.out.println("itemtable size = "+ this.itemtable.getItemSize());
		for(int it=0;it<this.itemtable.getItemSize();it++){
			System.out.println("itemtable["+it+"] = "+ this.itemtable.getAtom(it).toString());
		}
		//System.out.println("\n make itemtable time: "+ (System.currentTimeMillis() - StartTime));

		//this.miningStart();
		this.setMinFreq();
		ffLCM = new FfLCM();
		ffLCM.setMode(parallelMode);
		ffLCM.setThread(parallelThread);
		ffLCM.setDepth(parallelDepth);
		ffLCM.set(itemtable, modedef, datatypedef,minFreq,this.getVarSize());
		//ffLCMを起動
		// マイニングスタート
		ffLCM.run();

		this.outPuts = this.ffLCM.getOutputs();
		
		//StartTime = System.currentTimeMillis();
		for(int i = 0;i<this.outPuts.size();i++){//this.outPuts.size();i++){
			this.output(this.outPuts.get(i));
			
		}

		//try{
			File file = new File("result.txt");

			//BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			outputClos(file);
			/*BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			
			for(int i=0;i<this.closureList.size();++i){
				bw.write("conID=");
				bw.write(this.closureList.get(i).getConjunctionID());
				bw.write(" ,keyOcc=");
				bw.write(String.valueOf(this.closureList.get(i).getKeyOccSize()));
				bw.write(" ,Occ=");
				bw.write(String.valueOf(this.closureList.get(i).getOccSize()));
				bw.newLine();
				
				String write ="";

        		for(int atom:writeData.getAtomsID()){
        			if(write.equals("")){
        				write += itemtable.getAtom(atom);
        			}
        			else{
        				write += ", "+itemtable.getAtom(atom);
        			}
        		}

        		this.writer.write(write+"\n");
			}
			bw.flush();
			bw.close();*/
		//}catch (IOException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}


	}
	
	//=========================================================================
	// ◆ GetterおよびSetterのメソッド
	//=========================================================================
	/**
	 * インスタンスを取得します
	 */
	public static Mining instance() { return ins; }

	/**
	 * マイニングしたパターンを取得します
	 *
	 * @return
	 */
	public ArrayList<Conjunction> getOutput() { return outPuts; }
	/**
	 * データベースを取得します
	 *
	 * @return
	 */
	public DataBase getDataBase() { return database; }
	/**
	 * 最小頻度閾値を取得します
	 *
	 * @return
	 */
	public int getMinFreqPercent() { return minFreqPercent; }
	public int getMinFreq() { return minFreq; }
	/**
	 * 出現する変数の上限数を取得します
	 *
	 * @return
	 */
	public int getVarSize() { return varSize; }
	/**
	 * 定数の出現フラグを取得します
	 *
	 * @return
	 */
	public boolean isNoConst() { return noConst; }
	/**
	 * アイテムテーブル(リテラル集合)を取得します
	 *
	 * @return
	 */
	public ItemTable getItemTable() { return itemtable; }

	/** アトムのモード情報 */
	public ModeDef getModeDef(){return modedef;}

	/** 述語の引数の型情報 */
	public DatatypeDef getDatatypeDef(){return datatypedef;}
	
	/**
	 * システムの開始時刻を表示します.
	 *
	 */
	public static long getStartTime(){ return system_start_time; }


	/**
	 * 新しいデータベースを設定します
	 *
	 * @param newDataBase
	 */
	public void setDataBase(DataBase newDataBase) { this.database = newDataBase; }
	/**
	 * 最小頻度閾値を設定します
	 *
	 * @param newValue
	 */
	public void setMinFreqPercent(int newValue) { 

		this.minFreqPercent = newValue; 	
	
	}

	public void setMinFreq() {

		int newValue = (this.minFreqPercent * this.database.coversKey().size())/100;


		if(newValue > 0){
			this.minFreq = newValue;
		}
		else{
			this.minFreq = 1;
		}

	}

	/**
	 * 出現する変数の上限数を設定します
	 *
	 * @param newValue
	 */
	public void setVarSize(int newValue) { 
		
		this.varSize = newValue; 
		
	}
	/**
	 * 定数の出現フラグを設定します
	 *
	 * @param b
	 */
	public void setNoConst(boolean b) { 
		this.noConst = b; 
		}
	/**
	 * 新しいアイテムテーブルを設定します
	 *
	 * @param newItemTable
	 */
	public void setItemtable(ItemTable newItemTable) { this.itemtable = newItemTable; }

	/**
	 * 現在マイニング中であるかを取得します
	 *
	 * @return
	 */
	public boolean isMining() {
		if(Thread == null) { return false; }
		return Thread.isAlive();
	}

	public void setMood(int mood){
		this.mood = mood;
	}
	public int getMood(){
		return this.mood;
	}

	public ArrayList<ResultData> getClosureList(){
		return this.closureList;
	}
	
	//=========================================================================
	// ◆ その他のメソッド
	//=========================================================================

	/**
	 * マイニングスレッドを起動し，マイニングを開始します
	 */
	public void miningStart() {

		//System.out.println("ffLCM");
		this.setMinFreq();
		ffLCM.set(itemtable, modedef, datatypedef,minFreq,this.getVarSize());

		//ffLCM.run();
		//ffLCM.Start();
		/*
		Thread = new Thread(ffLCM);
		Thread.start();

		try {
			
			Thread.join();
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	}
	
	public void output(Conjunction c) {
		
		this.closureList.add(new ResultData(c));

	}
	class ConjunctionComparator implements java.util.Comparator {
		public int compare(Object s, Object t) {
			int sSize = ((Conjunction) s).getOccur().size();
			int tSize = ((Conjunction) t).getOccur().size();
			return sSize - tSize;
		}
	}
	public void outputClos(File file){
		try{	
			
			Collections.sort(this.outPuts, new ConjunctionComparator());
			/*
			int cccCount = 0;
			for(int i=0,n=this.outPuts.size();i<n;++i){//文字並び替えカウント
				Conjunction Ci = this.outPuts.get(i);
				for(int j=i+1,m=this.outPuts.size();j<m;++j){//i+1番目からサーチ
					Conjunction Cj = this.outPuts.get(j);
					if(Cj.testCount != 0){continue;}
					if(Ci.getOccur().size() != Cj.getOccur().size()){continue;}
					if(Ci.getAtoms().length != Cj.getAtoms().length){continue;}
					Atom[] array = Ci.getAtoms();
					int flag = 0;//ダブってないアトムの数
					int id = 0;//唯一C_i側のダブっていないアトムインデックス
					for(int h=0;h<array.length;++h){//C_iの全アトムで走査
						if(!Cj.hasAtom(array[h])){//アトムを持っていない
							flag++;
							id = h;
						}
					}
					if(flag == 1){//ダブってないアトムが一つだけ
						int id_j = 0;
						Atom[] array_j = Cj.getAtoms();
						for(int h=0;h<array_j.length;++h){//C_jの全アトムで走査
							if(!Ci.hasAtom(array_j[h])){//アトムを持っていない
								id_j = h;
							}
						}
						String A = array[id].toString();
						String B = array_j[id_j].toString();
						//System.out.println("Matching :" + Ci + " & " + Cj);
						//System.out.println("         :" + A + " & " + B);
						String A2 = A.substring(0, A.indexOf("("));
						String B2 = B.substring(0, B.indexOf("("));
						if(A2.equals(B2)){++cccCount;}
						else{
							//System.out.println("Matching :" + Ci + " & " + Cj);
						}
						Cj.testCount = 1;
					}
				}
			}
			System.out.println("cccCount = " + cccCount);
			*/
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			for(int i=0,n=this.outPuts.size();i<n;++i){
				//bw.write("conID=");
				//bw.write(this.closureList.get(i).getConjunctionID());
				/*bw.write(" ,keyOcc=");
				bw.write(String.valueOf(this.closureList.get(i).getKeyOccSize()));
				bw.write(" ,Occ=");
				bw.write(String.valueOf(this.closureList.get(i).getOccSize()));*/
				//bw.newLine();
				//bw.write("conID=");
				//bw.write(this.outPuts.get(i).getAtoms().toString());
				
				bw.write("Occ=");
				bw.write(this.outPuts.get(i).getOccurString().size()+", ");
				String conjPattern="";
				Conjunction conj=this.outPuts.get(i);
				for(Atom atom:conj.getAtoms()){
					if(conjPattern.equals("")){
						//conjPattern += itemtable.getAtom(atom.getID()) + "(" + atom.getNameID() + "," + atom.getID() + ")";
						conjPattern += itemtable.getAtom(atom.getID());
					}
					else{
						//conjPattern += ", "+itemtable.getAtom(atom.getID()) + "(" + atom.getNameID() + "," + atom.getID() + ")";
						conjPattern += ", "+itemtable.getAtom(atom.getID());
					}
				}
				bw.write(conjPattern+"\n");
				//bw.write(" " + conj.getOccurStringArray()+"\n\n");
			}
			bw.flush();
			bw.close();
		}catch (IOException e) {
			System.out.println(e);
		}


}

}
