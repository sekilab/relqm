package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ConcurrentHashMap;

import database.obj.Conjunction;
import miningRule.MiningIP;

//-Xrunhprof:cpu=samples,file=cpu_times.txt,depth=6
//-Xrunhprof:heap=sites,file=sites.txt
//-Xloggc:filename
//-XX:+UseConcMarkSweepGC 並列GC
//-Xms4g -Xmx4g
class Main {
    public static void main(String[] args) throws IOException {
    	//パターンに含まれる変数の数
    	int var_size = 5;
    	//最小サポート(0~100(%))
    	int min_sup = 0;
    	//並列の方式(0:逐次, 1:node, 2:krajca(fair), 3:para-for)
    	int para_mode = 0;
    	//並列処理のスレッド数
    	int threads = 4;
    	//krajca方式の探索の深さに対するしきい値
    	int dl = 2;
    	
    	//ルールマイニングでのしきい値(args未対応)
    	float ruleMinSup = 0.1f;
    	float ruleMinConf = 0.6f;
    	// ルール計算の方法を選択   CIP:0,GA:1,CAIM:2 
    	int ruleMode = 1;
    	int compSup_mode = 2;  // GAの時のサポート計算方法  0: compSuppModで計算, 1: QM流でdatabaseを1回scanでsupp計算. 2: 両方
    	int generationLimit = 100;  // GA世代数. default=100  
    	
    	String file_name = "";
    	//file_name = "src/data/config_muta.txt";
    	//file_name = "src/data/config_muta2.txt";
    	file_name = "src/data/config_fina.txt";
    	//file_name = "src/data/config_mond.txt";
    	
    	Mining instance = Mining.instance();
    	//実行時引数の処理
    	read_inputArgs(args,file_name,var_size,min_sup,para_mode,threads,dl);
    	/*if(args.length != 0){
    		file_name = args[0];
    		if(file_name.equals("help") || file_name.equals("-h") || file_name.equals("-help")){
    			//ヘルプ表示
    			System.out.println("arguments: 1file_name 2var_size 3minsup 4para_mode 5#Threads 6dl(krajca)");
    			System.out.println(" var_size: 1~ : default=5");
    			System.out.println(" minsup: 0~100(%) : default=0");
    			System.out.println(" para_mode: 0=seq, 1=node, 2=subtree, 3=paraFor : default=0");
    			System.out.println(" #Threads: 1~ : default=4");
    			System.out.println(" dl(krajca): 1~ : default=2");
    			return;
    		}
    		else{
    			if(args.length >= 2){
    				var_size = Integer.parseInt(args[1]);
    				if(args.length >= 3){
    					min_sup = Integer.parseInt(args[2]);
    					if(args.length >= 4){
    						para_mode = Integer.parseInt(args[3]);    						
    						if(args.length >= 5){
        						threads = Integer.parseInt(args[4]);        						
        						if(args.length >= 6){
            						dl = Integer.parseInt(args[5]);            						
            					}
        					}
    					}
    				}
    			}
    		}
    	}*/
    	instance.setParallelMode(para_mode);
    	instance.setParallelThread(threads);
    	instance.setParallelDepth(dl);
    	System.out.println(file_name);
    	try {
    		//ffLCM実行
    		instance.Start(file_name, var_size, min_sup, true);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	MiningIP MIP = new MiningIP(instance.getDataBase());
    	int num = 0;
    	Conjunction targetConj = null;
    	if(instance.getDataBase().getTranslator().patternData.atomList.size() == 0){
    		//configで使うパターンが指定されていない場合
    		int index = 0;
        	for(Conjunction conj : instance.getOutput()){
    			System.out.println(index + "\tconj : " + conj + "\t" + conj.getOccur().size());
    			++index;
    		}
        	InputStreamReader is = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(is);
            System.out.println("Select pattern");
            String number = br.readLine(); 
            if(number.equals("")){System.out.println("end.");return;}
            num = Integer.parseInt(number);    		
            targetConj=instance.getOutput().get(num);
    	}
    	else{
    		//configでパターン指定済み
    		//num = instance.getOutput().size() - 1; // 修正 2017.0329
    		num=instance.getDataBase().getTranslator().patternData.atomList.size();
    		for(Conjunction conj:instance.getOutput()){
    			// System.out.println("conj :"+conj+"conj.getAtoms().length="+conj.getAtoms().length+", num="+num);
    			if(conj.getAtoms().length >= num){
    				targetConj= conj;
    			}
    		}
    	}
    	MIP.setMinSup(ruleMinSup);
    	MIP.setMinConf(ruleMinConf);
    	MIP.setMode(ruleMode);
    	MIP.setCompSupMode(compSup_mode);
    	MIP.setGenerationLimit(generationLimit);
    	
    	//あるパターンに対し, 量的属性の区間を計算しルールの表示まで行う
    	//MIP.process(instance.getOutput().get(num));
    	MIP.process(targetConj);
    	System.out.println("end.");
	}

	private static void read_inputArgs(String[] args, String file_name,int var_size, int min_sup,
			int para_mode, int threads, int dl) {
		if(args.length != 0){
			file_name = args[0];
			if(file_name.equals("help") || file_name.equals("-h") || file_name.equals("-help")){
				//ヘルプ表示
				System.out.println("arguments: 1file_name 2var_size 3minsup 4para_mode 5#Threads 6dl(krajca)");
				System.out.println(" var_size: 1~ : default=5");
				System.out.println(" minsup: 0~100(%) : default=0");
				System.out.println(" para_mode: 0=seq, 1=node, 2=subtree, 3=paraFor : default=0");
				System.out.println(" #Threads: 1~ : default=4");
				System.out.println(" dl(krajca): 1~ : default=2");
				return;
			}
			else{
				if(args.length >= 2){
					var_size = Integer.parseInt(args[1]);
					if(args.length >= 3){
						min_sup = Integer.parseInt(args[2]);
						if(args.length >= 4){
							para_mode = Integer.parseInt(args[3]);    						
							if(args.length >= 5){
	    						threads = Integer.parseInt(args[4]);        						
	    						if(args.length >= 6){
	        						dl = Integer.parseInt(args[5]);            						
	        					}
	    					}
						}
					}
				}
			}
		}
		
	}
	
}


