package miningRule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import miningRule.MiningIP.Dictionary;
import miningRule.MiningIP.IntervalPattern;
import miningRule.MiningIP.PatternAtom;
import miningRule.MiningIP.Structure;

public class ComputingGARule {
	Dictionary dict;
	IntervalPattern template;
	Random rnd = new Random();
	static final int BIT_MAX = 2147483647*2+1 ;
	//親
	MiningIP parent;
	
	int varNum = 0;
	int keySize;
	float minSupp;
	float minSuppRate;
	float minConf;
	int int_count_o;
	int patternLength;
	int leftLastQuantIdx;
	ArrayList<float[]> database;
	short[][] databaseInteger;
	short[] maxValueInteger;
	short[] minValueInteger;
	float[] maxValue;
	float[] minValue;
	int[] varTypeArray;
	int[] ruleSide;
	int[] nominalIndex;
	ConcurrentHashMap<Integer, ArrayList<Float>> restoreMap;
	int[] MIC_index;
	ConcurrentHashMap<Integer, ArrayList<ArrayList<Short>>> rank2ID;
	
	int popSize = 250;//個体数
	int generationLimit = 100;//世代数
	float crossRate = 0.5f;
	float mutaRate = 0.4f;
	
	private int quantCnt = 0;//量的属性の数
	private Individual[] individuals;//親になる個体群
	private int[] differentValues;
	private float[] attr_Length;
	private short[] IndicesParentForCross = new short[16384];
	private int crossCount = 0;//親を選択するカウンタ
	IntervalPattern bestPattern = null;

	/**属性ごとの, その値を持つ出現のid集合(ビット列) のArrayList*/
	ArrayList<ArrayList<int[]>> occIntegerArray = new ArrayList<ArrayList<int[]>>();
	
	int[][] rootObjectsR;
	int[][] rootObjectsA;
	int preChildrenSize = 0;
	long suppTime = 0;
	int compSup_mode;  // 0: compSuppModで計算, 1: QM流 database 1 scanでsupp計算.
	private long compSupHash_suppTime=0;
	private long compSupQM_suppTime=0;
	
	public ComputingGARule(IntervalPattern template, MiningIP parent){
		this.parent = parent;
		Structure data = parent.dataStructures;
		this.dict = data.dict;
		this.template = template;
		varNum = template.length();
		this.keySize = data.keySize;
		this.minSupp = data.minSupp;
		this.minSuppRate = data.minSuppRate;
		this.minConf = data.minConf;
		this.int_count_o = data.int_count_o;
		this.leftLastQuantIdx = data.leftLastQuantIdx;
		this.patternLength = data.patternLength;
		this.databaseInteger = data.databaseInteger;
		this.database = data.database;
		this.maxValueInteger = data.maxValueInteger;
		this.minValueInteger = data.minValueInteger;
		this.maxValue = data.maxValue;
		this.minValue = data.minValue;
		this.varTypeArray = data.varTypeArray;
		this.ruleSide = data.ruleSide;
		this.nominalIndex = data.nominalIndex;
		this.restoreMap = data.restoreMap;
		this.MIC_index = data.MIC_index;
		this.rank2ID = data.rank2ID;
		this.suppTime=0;
		this.compSup_mode = parent.getCompSupMode();
		this.generationLimit = parent.getGenerationLimit();
		
		attr_Length = new float[varNum];
		differentValues = new int[varNum];
		for(int i=0;i<varNum;++i){
			attr_Length[i] = maxValue[i] - minValue[i];
			if(varTypeArray[i] == -1){
				++quantCnt;
				differentValues[i] = restoreMap.get(i).size();
			}
		}
		rootObjectsR = new int[varNum][int_count_o];
		rootObjectsA = new int[varNum][int_count_o];
		preChildrenSize = popSize - 1;
	}
	public IntervalPattern process(){
		preparate(template);
		
		//初期個体生成
		individuals = new Individual[popSize];
		for(int i=0;i<popSize;++i){
			individuals[i] = new Individual(quantCnt);
			//individuals[i].init();
		}

		
		Initialize(individuals);
		int[] sup_first = computeSuppMod(template, individuals[0]);
		if(sup_first[0] < minSupp){return null;}
		CalculateTableIndices();
		
		//GAによる最適化
		for(int i=0;i<generationLimit;++i){
			Evolution();
		}
		int strongCount = 0;
		float bestFitness = 0;
		Individual bestChild = null;
		for(int i=0;i<popSize;++i){
			float temp = individuals[i].getFitness();
			if(bestFitness < temp){
				bestFitness = temp;
				bestChild = individuals[i];
			}
			if(individuals[i].getConf() > minConf && individuals[i].getSupp() > minSupp){
				++strongCount;
			}
		}
		if(bestChild != null){
			int qIt = 0;
			for(int i=0;i<varNum;++i){
				if(varTypeArray[i] == -1){
					Gene gene = bestChild.getInterval(qIt);
					template.getAtom(i).set((short)(gene.getL() + 0.5f), (short)(gene.getU()));
					++qIt;
				}
			}
			bestPattern = template;
			System.out.println("#(strong rules): " + strongCount);
			System.out.print("suppComp_time (");
			if(this.compSup_mode==0){
				System.out.println("hash_compSupp) "+ suppTime);
			}
			else if(this.compSup_mode==1){
				System.out.println("a la QM) "+ suppTime);
			}else{
				System.out.println("hash_compSupp) "+ compSupHash_suppTime);
				System.out.println("suppComp_time (a la QM)) "+ compSupQM_suppTime);
			}
			System.out.println("popSize : "+ popSize+ ",  generationLimit: "+generationLimit);
		}
		else{
			//System.out.println("! suppTime " + suppTime);
		}
		
		return bestPattern;
	}
	private void Evolution(){
		ArrayList<Individual> children = new ArrayList<Individual>();
		int crossLimit = (int)(popSize * crossRate);
		//crossLimitまで交叉で新個体を生成
		for(int i=0;i<crossLimit;++i){
			Individual child = new Individual(quantCnt);
			int p1 = IndicesParentForCross[(int)((crossCount++)&0x3FFF)];
			int p2 = IndicesParentForCross[(int)((crossCount++)&0x3FFF)];
			//交叉
			child.cross(individuals[p1], individuals[p2]);
			//System.out.println("c " + child);
			if(java.lang.Math.random() < mutaRate){
				//突然変異
				mutation(child);//System.out.println("  c " + child);
				//child.muta();
			}
			children.add(child);			
		}
		//不足分をmutaRateに従い突然変異で補う
		int mutaTargetID = preChildrenSize;//前回のワースト個体から書き換えていく
		for(int i=crossLimit;i<popSize-1;++i){
			if(java.lang.Math.random() < mutaRate){
				//int index = (int)(Math.random() * popSize) % (popSize - 1);
				mutation(individuals[mutaTargetID]);
				children.add(individuals[mutaTargetID]);
				--mutaTargetID;
				//System.out.println(individuals[mutaTargetID]);
			}
		}
		
		children.add(individuals[0]);//最優個体をコピー
		
		float best = 0;
		long StartTime = System.currentTimeMillis();
		if(compSup_mode==0){
			for(int i=0,n=children.size();i<n;++i){
				//System.out.println(i + " " + children.get(i));
				float fit = fitness(template, children.get(i));//子どもたちの適応度を計算
				children.get(i).setFitness(fit);
				if(best < fit){
					best = fit;
				}
			}			
		}
		else if(compSup_mode==1){
			computeFitness_ala_QM(template,children);
		}else{ // 両方のsupport計算をやる
			long compSupHash_startTime = System.currentTimeMillis();
			for(int i=0,n=children.size();i<n;++i){
				//System.out.println(i + " " + children.get(i));
				float fit = fitness(template, children.get(i));//子どもたちの適応度を計算
				children.get(i).setFitness(fit);
				if(best < fit){
					best = fit;
				}
			}
			long tempTime = (System.currentTimeMillis() - compSupHash_startTime);
			compSupHash_suppTime += tempTime;
			// QM流supp計算
			long compSupQM_startTime = System.currentTimeMillis();
			computeFitness_ala_QM(template,children);
			tempTime = (System.currentTimeMillis() - compSupQM_startTime);
			compSupQM_suppTime += tempTime;
		}
		//if(best != 0)System.out.println(best);
		long tempTime = (System.currentTimeMillis() - StartTime);
		suppTime += tempTime;
		
		Collections.sort(children, new IndividualComparator());
		preChildrenSize = children.size() - 1;
		
		int restSize = popSize - children.size();
		int bestIdx = 0;
		//不足分、上位個体からコピー
		for(int i=0;i<restSize;++i){
			Individual child = new Individual(children.get(bestIdx));
			children.add(child);
			++bestIdx;
		}
		for(int i=0;i<popSize;++i){//世代交代
			individuals[i] = children.get(i);
		}
	}
	private void mutation(Individual indv){
		//System.out.println(indv);
		int qIt = 0;
		//if(indv.getInterval(0).getL() >= 180){System.out.println(" " + indv);}
		for(int i=0;i<varNum;++i){
			if(varTypeArray[i] == -1){
				if(Math.random() > 0.5){
					Gene gene = indv.getInterval(qIt);
					float v1 = (float)Math.random() * (maxValueInteger[i] - 1);
					if(v1 >= maxValueInteger[i]){v1 = maxValueInteger[i] - 1;}
					float v2 = (float)Math.random() * (maxValueInteger[i] - 1);
					if(v2 >= maxValueInteger[i]){v2 = maxValueInteger[i] - 1;}
					gene.set(v1, v2);
					gene.revision();
					++qIt;
				}
			}
		}
		//if(indv.getInterval(0).getL() >= 180){System.out.println("  " + indv);}
		//System.out.println(" " + indv);
	}
	public int[] computeSuppMod(IntervalPattern IP, Individual indv){
		int[] ret = new int[4];
		int[] objects = new int[int_count_o];
		Arrays.fill(objects, BIT_MAX);
		int[] objectA = new int[int_count_o];
		Arrays.fill(objectA, BIT_MAX);
		int qIt = 0;
		for(int i=0;i<varNum;++i){
			if(varTypeArray[i] == -1){
				ArrayList<int[]> array = occIntegerArray.get(qIt);
				Gene gene = indv.getInterval(qIt);
				int l = (int)Math.floor(gene.getL()) + 1;
				float u = gene.getU();
				
				//System.out.println(l + " -> " + u);
				if(ruleSide[i] == 1){
					int[] tempR = new int[int_count_o];
					int[] tempA = new int[int_count_o];
					for(int j=l;j<u;++j){
						if(j >= maxValueInteger[i]){break;}
						int[] occ = array.get(j);
						for(int o=0;o<int_count_o;++o){
							tempR[o] |= occ[o];
							tempA[o] |= occ[o];
						}
					}
					for(int o=0;o<int_count_o;++o){
						objects[o] &= tempR[o];
						objectA[o] &= tempA[o];
					}
				}
				else{
					int[] tempR = new int[int_count_o];
					for(int j=l;j<u;++j){
						if(j >= maxValueInteger[i]){break;}
						int[] occ = array.get(j);
						for(int o=0;o<int_count_o;++o){
							tempR[o] |= occ[o];
						}
					}
					for(int o=0;o<int_count_o;++o){
						objects[o] &= tempR[o];
					}
				}
				++qIt;
			}
			else if(varTypeArray[i] == 1){
				if(ruleSide[i] == 1){
					for(int o=0;o<int_count_o;++o){
						objects[o] &= rootObjectsR[i][o];
						objectA[o] &= rootObjectsA[i][o];
					}
				}
				else{
					for(int o=0;o<int_count_o;++o){
						objects[o] &= rootObjectsR[i][o];
					}
				}
			}
		}
		int[] keyCheck = new int[keySize];
		int[] A_Check = new int[keySize];
		int hit = 0;
		int supA = 0;
		for(int o=0;o<int_count_o;++o){
			for(int i=0;i<32;++i){
				int idx = o*32+i;
				if(idx >= databaseInteger.length){break;}
				int id = databaseInteger[idx][0];
				if((objects[o] & (1<<i)) != 0){
					if(keyCheck[id] == 0){
						++keyCheck[id];
						++hit;
					}
				}
				if((objectA[o] & (1<<i)) != 0){
					if(A_Check[id] == 0){
						++A_Check[id];
						++supA;
					}
				}
			}
		}
		ret[0] = hit;
		ret[1] = supA - ret[0];
		ret[2] = 0;
		ret[3] = keySize - ret[0] - ret[1] - ret[2];
		//System.out.println(" " + supA + ", " + hit);
		//int[] sup = computeSupp(IP, indv);
		//System.out.println("  " + (sup[0]+sup[1]) + ", " + sup[0]);
		return ret;
	}
	public int[] computeSupp(IntervalPattern IP, Individual indv){
		//  +C -C
		//+A a  b
		//-A c  d
		int[] ret = new int[4];//クロス表
		int hit = 0;
		int supA=0,supC=0;
		int[] keyCheck = new int[keySize];
		int[] A_Check = new int[keySize];
		int[] C_Check = new int[keySize];
		for(short[] data : databaseInteger){
			int keyID = data[0];
			if(A_Check[keyID] == 1 && C_Check[keyID] == 1){continue;}
			boolean matchA = true, matchC = true;
			int qIt = 0;
			for(int i=0;i<varNum;++i){
				PatternAtom atom = IP.getAtom(i);
				int v = data[i];
				if(varTypeArray[i] == -1){
					Gene gene = indv.getInterval(qIt);
					float l = gene.getL();
					float u = gene.getU();
					//System.out.println(l + " " + v + " " + u);
					if(v < l || u < v){
						if(ruleSide[i] == 1){matchA = false;}
						else{matchC = false;}
					}
					++qIt;
				}
				else{
					if(atom.getValue() != 0 && v != atom.getValue()){
						if(ruleSide[i] == 1){matchA = false;}
						else{matchC = false;}
					}
				}
			}
			if(matchA){
				if(A_Check[keyID] != 1){
					++supA;
					A_Check[keyID] = 1;
				}
				if(matchC){
					if(C_Check[keyID] != 1){
						++supC;
						C_Check[keyID] = 1;
					}
					if(keyCheck[keyID] != 1){
						++hit;
						keyCheck[keyID] = 1;
					}
				}
			}
			else if(matchC){
				if(C_Check[keyID] != 1){
					++supC;
					C_Check[keyID] = 1;
				}
			}
		}
		ret[0] = hit;
		ret[1] = supA - ret[0];
		ret[2] = supC - ret[0];
		ret[3] = keySize - ret[0] - ret[1] - ret[2];
		return ret;
	}
	public float computeFitness_ala_QM(IntervalPattern IP, ArrayList<Individual> children){
		float best=0;
		//  +C -C
		//+A a  b
		//-A c  d
		// int [] -> int [][] 2次元へ． 第0次元は各ルール(child)用．
		int noOfChildren = children.size();
		int[][] ret = new int[noOfChildren][4];//クロス表
		int [] hit = new int[noOfChildren];
		int[] supA=new int[noOfChildren];
		int[] supC=new int[noOfChildren];
		int[][] keyCheck = new int[noOfChildren][keySize];
		int[][] A_Check = new int[noOfChildren][keySize];
		int[][] C_Check = new int[noOfChildren][keySize];
		for(short[] dataTuple : databaseInteger){
			int keyID = dataTuple[0];
			for(int r=0;r<noOfChildren;++r){ // ｒ番目のchild (rule) 
				if(A_Check[r][keyID] == 1 && C_Check[r][keyID] == 1){ continue;}
				boolean matchA = true, matchC = true;
				int qIt = 0; // 処理対象の量的変数の順番を示すための変数
				for(int i=0;i<varNum;++i){
					PatternAtom atom = IP.getAtom(i);
					int v = dataTuple[i];
					if(varTypeArray[i] == -1){ // 量的変数
						Gene gene = children.get(r).getInterval(qIt);
						float l = gene.getL();
						float u = gene.getU();
						//System.out.println(l + " " + v + " " + u);
						if(v < l || u < v){
							if(ruleSide[i] == 1){matchA = false;}
							else{matchC = false;}
						}
						++qIt;
					}
					else{
						if(atom.getValue() != 0 && v != atom.getValue()){  // atom.getValue() != 0 ???
							if(ruleSide[i] == 1){matchA = false;}
							else{matchC = false;}
						}
					}
				}
				if(matchA){
					if(A_Check[r][keyID] != 1){
						++supA[r];
						A_Check[r][keyID] = 1;
					}
					if(matchC){
						if(C_Check[r][keyID] != 1){
							++supC[r];
							C_Check[r][keyID] = 1;
						}
						if(keyCheck[r][keyID] != 1){
							++hit[r];
							keyCheck[r][keyID] = 1;
						}
					}
				}
				else if(matchC){
					if(C_Check[r][keyID] != 1){
						++supC[r];
						C_Check[r][keyID] = 1;
					}
				}			
				ret[r][0] = hit[r];
				ret[r][1] = supA[r] - ret[r][0];
				ret[r][2] = supC[r] - ret[r][0];
				ret[r][3] = keySize - ret[r][0] - ret[r][1] - ret[r][2];
			}
		}
		//
		for(int r=0,n=children.size();r<n;++r){
			//System.out.println(i + " " + children.get(i));
			float fit = fitness_with_supp(template, children.get(r), ret[r]);//子どもたちの適応度を計算
			children.get(r).setFitness(fit);
			if(best < fit){
				best = fit;
			}
		}
		return best;
	}
	
	public float fitness(IntervalPattern IP, Individual indv){
		int[] sup = computeSuppMod(IP, indv);
		//int[] sup = computeSupp(IP, indv);
		float A = sup[0] + sup[1];
		float AC = sup[0];
		float gain = (AC) - minConf*(A);
		float fitness = gain;
		if(gain > 0){
			int qIt = 0;
			for(int i=0;i<varNum;++i){
				if(varTypeArray[i] == -1){
					Gene gene = indv.getInterval(qIt);
					ArrayList<Float> array = restoreMap.get(i);
					float L = array.get((int)(gene.getL() + 0.5f));
					float U = array.get((int)gene.getU());
					float prop = 1.0f - ((U - L) / (maxValue[i] - minValue[i]));
					fitness *= (prop*prop);
					if(AC < minSupp){fitness -= keySize;}//penalty
					++qIt;
				}
			}
		}
		float conf = AC / A;
		indv.setSupp((int)AC);
		indv.setConf(conf);
		return fitness;
	}
	public float fitness_with_supp(IntervalPattern IP, Individual indv, int[] sup){
		//int[] sup = computeSuppMod(IP, indv);
		//int[] sup = computeSupp(IP, indv);
		float A = sup[0] + sup[1];
		float AC = sup[0];
		float gain = (AC) - minConf*(A);
		float fitness = gain;
		if(gain > 0){
			int qIt = 0;
			for(int i=0;i<varNum;++i){
				if(varTypeArray[i] == -1){
					Gene gene = indv.getInterval(qIt);
					ArrayList<Float> array = restoreMap.get(i);
					float L = array.get((int)(gene.getL() + 0.5f));
					float U = array.get((int)gene.getU());
					float prop = 1.0f - ((U - L) / (maxValue[i] - minValue[i]));
					fitness *= (prop*prop);
					if(AC < minSupp){fitness -= keySize;}//penalty
					++qIt;
				}
			}
		}
		float conf = AC / A;
		indv.setSupp((int)AC);
		indv.setConf(conf);
		return fitness;
	}
	public void CalculateTableIndices(){
		int iPlageRoulette = (popSize * (popSize+1)) / 2;
		for (int iTable=0; iTable<16384; iTable++) {
			int iTirage = (int)(java.lang.Math.random() * (double)iPlageRoulette);  // iTirage= draw
			int iPlageRestante = iPlageRoulette - popSize;
			int iIndiceReglePotentielle = popSize - 1;
			while (iTirage<iPlageRestante) {
				iPlageRestante -= iIndiceReglePotentielle;
                iIndiceReglePotentielle--;
			}
			IndicesParentForCross[iTable] = (short)iIndiceReglePotentielle;
			//System.out.println("IndicesParentForCross:  "+ IndicesParentForCross[iTable]);
		}
		crossCount = 0;
	}
	private void Initialize(Individual[] individuals){
		for(int i=0;i<popSize;++i){
			Individual target = individuals[i];
			int qIt = 0;
			for(int j=0;j<varNum;++j){
				if(varTypeArray[j] == -1){
					Gene gene = target.getInterval(qIt);
					float V = (differentValues[j] - 1);
					float A = V;
					A -= (i * (V - minSuppRate*V)) / (popSize - 1);
					float l = (float)Math.random() * (V - A);
					float u = l + A;
					if(l >= maxValueInteger[j]){l = maxValueInteger[j] - 1;}
					if(u >= maxValueInteger[j]){u = maxValueInteger[j] - 1;}
					gene.set(l, u);
					gene.revision();
					++qIt;
				}
			}
		}
	}
	private void preparate(IntervalPattern IP){
		for(int i=0;i<varNum;++i){
			//Arrays.fill(rootObjectsR[i], BIT_MAX);
			//Arrays.fill(rootObjectsA[i], BIT_MAX);
			
			if(varTypeArray[i] == -1){ // 数値属性
				ArrayList<ArrayList<Short>> array_array = rank2ID.get(i);
				ArrayList<int[]> occArray = new ArrayList<int[]>();
				for(int j=0;j<differentValues[i];++j){
					ArrayList<Short> occ = array_array.get(j);
					int[] objects = new int[int_count_o];
					for(int id : occ){
						objects[id/32] |= (1 << (id%32));
					}
					occArray.add(objects);
				}
				occIntegerArray.add(occArray);
			}
			else if(varTypeArray[i] == 1){
				short val = IP.getAtom(i).getValue();
				for(int j=0,n=databaseInteger.length;j<n;++j){
					if(val == databaseInteger[j][i]){
						rootObjectsR[i][j/32] |= (1 << (j%32));
						if(ruleSide[i] == 1){
							rootObjectsA[i][j/32] |= (1 << (j%32));
						}
					}
				}
				/*
				dict.getInt2Str(i).size();
				for(int j=0,n=array_array.size();j<n;++j){
					if(IP.getAtom(i).getValue() == j){
						ArrayList<Short> occ = array_array.get(j);
						if(ruleSide[i] == 1){
							int[] objects = new int[int_count_o];
							for(int id : occ){
								objects[id / 32] |= (1 << (id%32));
							}
							for(int o=0;o<int_count_o;++o){
								rootObjectsR[i][o] |= objects[o];
								rootObjectsA[i][o] |= objects[o];
							}
							
						}
						else{
							int[] objects = new int[int_count_o];
							for(int id : occ){
								objects[id / 32] |= (1 << (id%32));
							}
							for(int o=0;o<int_count_o;++o){
								rootObjectsR[i][o] |= objects[o];
							}
						}
					}
				}
				*/
			}
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public class Individual{//区間を持つGAの個体
		private int size= 0;//区間の数
		private Gene[] intervals;
		private float fitness = 0;//適応度
		private int support;
		private float confidence;
		public Individual(int qSize){
			size = qSize;
			intervals = new Gene[qSize];//区間生成
			int qIt = 0;
			for(int i=0;i<varNum;++i){
				if(varTypeArray[i] == -1){
					intervals[qIt] = new Gene(i);
					++qIt;
				}
			}
		}
		public Individual(Individual orig){//copy
			size = orig.size();
			intervals = new Gene[size];//区間生成
			for(int i=0;i<size;++i){
				intervals[i] = new Gene(i);
				Gene orgG = orig.getInterval(i);
				intervals[i].set(orgG.getL(), orgG.getU());
			}
			fitness = orig.getFitness();
		}
		void init(){
		}
		public void cross(Individual p1, Individual p2){
			int method = (int)(Math.random() * 100) % 8;//0~7
			if(method == 0){
				for(int i=0;i<size;++i){
					intervals[i].set(p1.getInterval(i).getL(), p1.getInterval(i).getU());
				}
			}
			else if(method == 1){
				for(int i=0;i<size;++i){
					intervals[i].set(p2.getInterval(i).getL(), p2.getInterval(i).getU());
				}
			}
			else if(method < 5){//2,3,4
				for(int i=0;i<size;++i){
					intervals[i].set(p1.getInterval(i).getL(), p2.getInterval(i).getU());
				}
			}
			else{//5,6,7
				for(int i=0;i<size;++i){
					intervals[i].set(p2.getInterval(i).getL(), p1.getInterval(i).getU());
				}
			}
			
		}
		public void muta(){
			for(int i=0;i<size;++i){
				intervals[i].muta();
			}
		}
		Gene getInterval(int n){return intervals[n];}
		float getFitness(){return fitness;}
		void setFitness(float f){fitness = f;}
		float getConf(){return confidence;}
		void setConf(float v){confidence = v;}
		int getSupp(){return support;}
		void setSupp(int v){support = v;}
		int size(){return size;}
		public String toString(){
			String str = "";
			for(int i=0;i<size;++i){
				str += intervals[i].toString();
			}
			return str;
		}
	}
	public class Gene{
		int id;
		float l;//0~1
		float u;
		public Gene(int n){
			id = n;
		}
		public float getL(){return l;}
		public float getU(){return u;}
		void revision(){//区間反転を修正
			if(l > u){
				float temp = u;
				u = l;
				l = temp;
			}
			if(l >= maxValueInteger[id]){l = maxValueInteger[id] - 1;}
			if(u >= maxValueInteger[id]){u = maxValueInteger[id] - 1;}
		}
		void init(){
			/*l = (float)Math.random();//l,uを0.0~1.0で表す
			l = (float)Math.floor((double)l * 100) / 100;
			u = (float)Math.random();
			u = (float)Math.floor((double)u * 100) / 100;*/
			revision();
		}
		public void muta(){
			float v = (float)Math.random();
			if(Math.random() < 0.5){l = v;}
			else{u = v;}
			revision();
		}
		public void set(float l, float u){
			this.l = l;
			this.u = u;
			revision();
		}
		public String toString(){
			String str = ("[" + l + "," + u + "]");
			
			return str;
		}
	}
	public class IndividualComparator implements Comparator<Individual> {

		public int compare(Individual a, Individual b) {
			float no1 = a.getFitness();
			float no2 = b.getFitness();
			if (no1 < no2) {//降順
				return 1;
			}
			else if (no1 == no2){
				return 0;
			}
			else{
				return -1;
			}
				
		}
	}
}
