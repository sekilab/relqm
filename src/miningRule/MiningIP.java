package miningRule;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

import database.DataBase;
import database.obj.Atom;
import database.obj.Conjunction;
import database.obj.Term;
import database.obj.Var;

public class MiningIP {
	private int mode = 0;  // mainでsetされる
	static final int BIT_MAX = 2147483647*2+1 ;
	Conjunction conjunction;
	//ffLCMから受け取る複数の情報を格納
	DataBase miningDatabase;
	//区間パターンマイニング用のデータベース: 使うConjunctionの出現集合を形式文脈のように扱う
	ArrayList<float[]> database = new ArrayList<float[]>();
	//databaseの量的属性を順序値(short)に書き換えたもの
	short[][] databaseInteger = null;
	//keyに属性の番号, valueに量的属性の値を昇順で格納したArrayList(昇順なので順序値でgetできる)
	ConcurrentHashMap<Integer, ArrayList<Float>> restoreMap = new ConcurrentHashMap<Integer, ArrayList<Float>>();
	//データを昇順で格納するために使用
	short[][] sortDatabase;
	int[][] sortIndex;
	//keyに属性の番号, valueにその値を持つ対象のタプルIDのリスト
	ConcurrentHashMap<Integer, ArrayList<ArrayList<Short>>> rank2ID = new ConcurrentHashMap<Integer, ArrayList<ArrayList<Short>>>();
	
	ConcurrentHashMap<Integer, int[]> supportCategory = new ConcurrentHashMap<Integer, int[]>();//get(ある質的属性について)[特定の属性値の] = サポートカウント
	
	//マイニングされたCIPを格納(不使用)
	ArrayList<IntervalPattern> patternList = new ArrayList<IntervalPattern>();
	private int patternCount = 0;//求められたCIPの数
	private int ruleCount = 0;//CIPのうち、ルールとしてminsup, minconfを満たしたもの
	//ルールテンプレートのリスト
	ArrayList<IntervalPattern> templateList = new ArrayList<IntervalPattern>();
	//幅優先探索用のキュー
	//LinkedList<Call> CallQueue = new LinkedList<Call>();

	short[][] convexHull;
	
	private Dictionary dict = new Dictionary();//質的属性をユニークIDに変換するためのハッシュ等
	private float[] minValue;//n番目の属性の最大値
	private float[] maxValue;//n番目の属性の最小値
	private short[] minValueInteger;//n番目の属性の最大値(short)
	private short[] maxValueInteger;//n番目の属性の最小値(short)

	private int int_count_o = 0;//object32個ごとに1加算
	private int objects = 0;

	private IntervalPattern bestPattern;//Fitness最大となるルール(パターン)を記録
	
	/*-----------------入力パラメータ------------------------------------------------------------*/
	private float minSupRate = 0.1f;//0~1
	private float minConf = 0.6f;//0~1
	/*---------------------------------------------------------------------------------------*/
	
	private int count_minsupp = 0;
	private int count_closure = 0;

	private float minSupp = 0;//1~objects objects*minsupRateを後で計算
	private int varNum = 0;//パターン中の変数の数
	private int keySize = 0;//keyに入る代入の数
	private int patternLength = 0;//パターン長
	private int conditionsLength = 0;
	private int leftLastQuantIdx = 0;//ルール条件部の最後の量的変数の位置(なければ0)

	int[] varTypeArray;//0:keyID, 1:カテゴリ, -1:数値
	int[] nominalIndex;//-1:num, else:IDorCategory_uniqueID
	int[] ruleSide;//変数の位置
	int[] MIC_index;//i番目の変数を何番目に処理するか. パターン内の, ルール条件部の要素から処理するため
	int[] atomRuleSide;//ConjunctionのAtomの位置
	Structure dataStructures;//ルールマイニングを行うクラスに必要なデータをまとめたクラス
	int nominalCount;
	private int compSup_mode;
	private int generationLimit;
	public MiningIP(DataBase db){
		miningDatabase = db;
	}
	/** minSupを0~1で入力*/
	public void setMinSup(float val){
		minSupRate = val;
	}
	/** minConfを0~1で入力*/
	public void setMinConf(float val){
		minConf = val;
	}
	public void setMode(int m){
		mode = m;
	}
	public void process(Conjunction conj){
		conjunction = conj;//ルールに用いる関係パターン
		System.out.println("\nconj : " + conj);
		System.out.println("occSize : " + conj.getOccur().size());
		
		long StartTime = System.currentTimeMillis();
		
		//データベース等生成
		readContext(conj);

		//ルールテンプレート生成
		long StartTemplateTime = System.currentTimeMillis();
		short[] voidArray = new short[varNum];
		makeTemplate(voidArray, 0);
		
		System.out.println("template time " + (System.currentTimeMillis() - StartTemplateTime));
		System.out.println("time " + (System.currentTimeMillis() - StartTime));
		System.out.println("templateList.size() : " + templateList.size());
		
		//区間計算クラスに渡すデータを一纏めに
		dataStructures = new Structure();

		if(mode == 0){
			miningIPRule();
		}
		else if(mode == 1){
			miningGARule();
		}
		else if(mode == 2){
			miningCAIMRule();
		}
		else{
			miningIPRule();miningGARule();miningCAIMRule();
		}
		//outputCSV();
		//System.out.println("best CIP :\n" + patternList.get(maxID));
	}
	/** ルール中の量的属性の区間をCAIMを用いて決定*/
	private void miningCAIMRule(){
		long AllTime = System.currentTimeMillis();
		System.out.println("/*----------------------------------------------------------------------------------------------------------------------------*/\n");
		MiningCAIM instance = new MiningCAIM(dataStructures);
		//MiningCAIM instance = new MiningCAIM(conjunction,database,varTypeArray,ruleSide,atomRuleSide,minValue,maxValue,dict,(int)minSupp, minConf);
		instance.process(templateList);
		System.out.println("all time " + (System.currentTimeMillis() - AllTime));
	}
	private void miningGARule(){
		long AllTime = System.currentTimeMillis();
		try {
            // file に出力する PrintStream を生成
            //PrintStream out = new PrintStream("GA_log.txt");           
            //置き換える
            //System.setOut(out);
            System.setOut(new PrintStream(new FileOutputStream("GA_log.txt", true))); 
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
		
		for(int i=0;i<templateList.size();++i){
			IntervalPattern IP = templateList.get(i);
			System.out.println("IP from templateList("+i +"): "+ IP.toString());
			ComputingGARule computeGA = new ComputingGARule(IP, this);
			long StartTime = System.currentTimeMillis();
			bestPattern = computeGA.process();
			if(bestPattern != null){
				System.out.println("time (compGA) " + (System.currentTimeMillis() - StartTime));
				printRule(bestPattern);
				System.out.println("----------------------------------------------------------------------------------------------------------------------------\n");
			}
		}
		FileOutputStream fdOut = new FileOutputStream(FileDescriptor.out);
		System.setOut(new PrintStream(new BufferedOutputStream(fdOut, 128), true));
		System.out.println("all time " + (System.currentTimeMillis() - AllTime));
	}
	/** ルール中の量的属性の区間を飽和区間パターンを用いて決定*/
	private void miningIPRule(){
		long AllTime = System.currentTimeMillis();
		System.out.println("----------------------------------------------------------------------------------------------------------------------------\n");
		for(int i=0;i<templateList.size();++i){
			IntervalPattern IP = templateList.get(i);
			ComputingCIPRule computeCIP = new ComputingCIPRule(IP, this);
			bestPattern = computeCIP.process();
			if(bestPattern != null){
				printRule(bestPattern);
				System.out.println("----------------------------------------------------------------------------------------------------------------------------\n");
			}
		}
		System.out.println("all time " + (System.currentTimeMillis() - AllTime));
	}
	private void printRule(IntervalPattern IP){
		int hit = 0;
		int supA=0,supC=0;
		int[] keyCheck = new int[keySize];
		int[] A_Check = new int[keySize];
		int[] C_Check = new int[keySize];
		for(int j=0,len=databaseInteger.length;j<len;++j){
			int keyID = databaseInteger[j][0];
			//if(A_Check[keyID] == 1 && C_Check[keyID] == 1){continue;}
			boolean matchA = true, matchC = true;
			for(int i=0;i<varNum;++i){
				PatternAtom atom = IP.getAtom(i);
				if(varTypeArray[i] == -1){
					if(databaseInteger[j][i] < atom.getL() || atom.getU() < databaseInteger[j][i]){
						if(ruleSide[i] == 1){matchA = false;}
						else{matchC = false;}
					}
				}
				else if(varTypeArray[i] == 1){
					if(atom.getValue() != 0 && databaseInteger[j][i] != atom.getValue()){
						if(ruleSide[i] == 1){matchA = false;}
						else{matchC = false;}
					}
				}
			}
			if(matchA){
				if(A_Check[keyID] == 0){
					++supA;
					A_Check[keyID] = 1;
				}
				if(matchC){
					if(C_Check[keyID] == 0){
						++supC;
						C_Check[keyID] = 1;
					}
					if(keyCheck[keyID] == 0){
						++hit;
						keyCheck[keyID] = 1;
					}
				}
			}
			else if(matchC){
				if(C_Check[keyID] != 1){
					++supC;
					C_Check[keyID] = 1;
				}
			}
		}
		float a = hit;
		float b = supA - a;
		float c = supC - a;
		float d = keySize - a - b - c;
		float chi2 = (keySize*(a*d - b*c)*(a*d - b*c)) / ((a+c)*(b+d)*(a+b)*(c+d));
		float gain = ((float)hit) - minConf*((float)supA);
		float fitness = gain;
		if(gain > 0){
			for(int i=0;i<varNum;++i){
				if(varTypeArray[i] == -1){
					PatternAtom atom = IP.getAtom(i);
					float L = restoreMap.get(i).get(atom.getL()) - minValue[i];
					float U = restoreMap.get(i).get(atom.getU()) - minValue[i];
					float prop = 1.0f - ((U - L) / (maxValue[i] - minValue[i]));
					fitness *= (prop*prop);
					if(hit < minSupp){fitness -= keySize;}
				}
			}
		}
		float OR = (a/b)/(c/d);
		//System.out.println(a + " " + b + " " + c + " " + d);
		float conf = 100.0f * hit / supA;
		System.out.println("supA = " + supA + "(" + ((float)supA / (keySize)) + ")"
				+ ",supC = " + supC + "(" + ((float)supC / (keySize)) + ")"
				+ ",supAC = " + hit + "(" + ((float)hit / (keySize)) + ")"
				+ ", conf = " + conf
				+ "\nchi2 = " + chi2 + ", oddsRatio = " + OR + ", gain = " + gain + ", fitness = " + fitness);
		//System.out.println(IP);
		StringBuilder sb_l = new StringBuilder();
		StringBuilder sb_r = new StringBuilder();
		Atom[] atoms = conjunction.getAtoms();
		for(int i=0;i<atoms.length;i++) {
			Atom atom = atoms[i];
			Term[] args = atom.getArgs();
			Var[] vars = atom.getVars();
			StringBuilder sb = new StringBuilder(atom.getName());
			sb.append("(");
			sb.append(args[0]);
			for(int j=1;j<args.length;j++) {
				sb.append(",");
				sb.append(args[j]);
				if(varTypeArray[vars[j].getIndex()] == -1){
					PatternAtom I = IP.getAtom(vars[j].getIndex());
					sb.append("[");
					sb.append(restoreMap.get(vars[j].getIndex()).get(I.getL()));
					sb.append(", ");
					sb.append(restoreMap.get(vars[j].getIndex()).get(I.getU()));
					sb.append("]");
				}
				if(varTypeArray[vars[j].getIndex()] == 1){
					PatternAtom I = IP.getAtom(vars[j].getIndex());
					String name = dict.getInt2Str(vars[j].getIndex()).get((int)I.getValue());
					sb.append("[");
					sb.append(name);
					sb.append("]");
				}
			}
			sb.append(")");
			String atomStr = sb.toString();
			
			if(atomRuleSide[i] == 1){
				if(sb_l.length() != 0){sb_l.append(", ");}
				sb_l.append(atomStr);
			}
			else{
				if(sb_r.length() != 0){sb_r.append(", ");}
				sb_r.append(atomStr);
			}
		}
		String out = sb_l.toString() + " → " + sb_r.toString();
		System.out.println(out);
	}
/*
	//breadth first search MinIntChange
	private void BFS_MIC(){
		IntervalPattern IP;
		int n;
		boolean free;
		while(!CallQueue.isEmpty()){
			//Call call = CallQueue.removeFirst();//幅優先
			Call call = CallQueue.removeLast();//深さ優先
			IP = call.IP;
			n = call.n;
			free = call.free;
			if(n >= IP.length()){
				continue;
			}
			int index = MIC_index[n];
			
			if(varTypeArray[index] == -1){//n番目が量的属性
				if(free){
					IntervalPattern IP_nr = getNewIP(IP);
					boolean c_flag = IP_nr.changeRight(index);
					
					if(c_flag){
						boolean clo_flag = new_closure(IP_nr,n);
						if(clo_flag){
							++patternCount;
							float fitness = computeFitness(IP_nr);
							if(bestFitness < fitness){
								bestFitness = fitness;
								bestPattern = IP_nr;
							}
							float sup = IP_nr.getSupport();
							float supA = IP_nr.getSupportA();
							float conf = sup / supA;
							if((conf >= minConf)){
								++ruleCount;
							}
							CallQueue.add(new Call(IP_nr, n, true));
						}
					}
				}
				IntervalPattern IP_nl = getNewIP(IP);
				boolean c_flag = IP_nl.changeLeft(index);
				
				if(c_flag){
					boolean clo_flag = new_closure(IP_nl,n);
					if(clo_flag){
						++patternCount;						
						float fitness = computeFitness(IP_nl);						
						if(bestFitness < fitness){
							bestFitness = fitness;
							bestPattern = IP_nl;
						}
						float sup = IP_nl.getSupport();
						float supA = IP_nl.getSupportA();
						float conf = sup / supA;
						if((conf >= minConf)){
							++ruleCount;
						}
						CallQueue.add(new Call(IP_nl, n, false));
					}
				}
			}
			if(n+1 < patternLength){CallQueue.add(new Call(IP, n+1, true));}
		}
	}
	class Call{
		IntervalPattern IP;
		int n;
		boolean free;
		public Call(IntervalPattern IP, int n, boolean free){
			this.IP = IP;
			this.n = n;
			this.free = free;
		}
	}
	public IntervalPattern getNewIP(IntervalPattern IP){
		return new IntervalPattern(IP);
	}
*/
	private float computeFitness(IntervalPattern IP){
		float supAC = IP.getSupport();
		float supA = IP.getSupportA();
		if(supAC < minSupp || (supAC/supA) < minConf){return -1;}
	
		float gain = (supAC) - minConf*(supA);
		float fitness = gain;
		if(gain > 0){
			for(int i=0;i<patternLength;++i){
				if(varTypeArray[i] == -1){
					PatternAtom atom = IP.getAtom(i);
					ArrayList<Float> array = restoreMap.get(i);
					float L = array.get(atom.getL());
					float U = array.get(atom.getU());
					float prop = 1.0f - ((U - L) / (maxValue[i] - minValue[i]));
					float prop2 = prop*prop;
					fitness *= (prop2);
				}
			}
		}
		return fitness;
	}

	/**
	 * ルールテンプレートの作成からデータの読み込み、効率化用のデータ構造生成まで
	 * @param conj 使用する関係パターン
	 */
	private void readContext(Conjunction conj){
		atomRuleSide = new int[conj.getAtoms().length];
		String ruleSideStr = "";
		if(miningDatabase.getTranslator().getRuleString() != ""){
			ruleSideStr = miningDatabase.getTranslator().getRuleString();
		}
		else{
			InputStreamReader is = new InputStreamReader(System.in);
	        BufferedReader br = new BufferedReader(is);
	        System.out.println("Input RuleNumber (ex. 1 1 1 2)");
	        try {
	        	ruleSideStr = br.readLine();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
        String[] ruleSideStrArr = ruleSideStr.split(" ");
        for(int i=0,n=ruleSideStrArr.length;i<n;++i){
        	atomRuleSide[i] = Integer.parseInt(ruleSideStrArr[i]);
        }
        //ルール型作成完了
        
		ArrayList<String[]> occur = conj.getOccur();
		varNum = occur.get(0).length;
		ruleSide = new int[varNum];
		varTypeArray = new int[varNum];
		Atom[] atoms = conjunction.getAtoms();
		//パターンの要素がルールの条件・結論のどちらに来るか、から変数がどちらに来るかを判別
		for(int i=0;i<atoms.length;i++) {
			Atom atom = atoms[i];
			Term[] args = atom.getArgs();
			Var[] vars = atom.getVars();
			for(int j=0;j<args.length;j++) {
				if(ruleSide[vars[j].getIndex()] != 0){continue;}
				varTypeArray[vars[j].getIndex()] = miningDatabase.name2type.get(atom.getName())[j];
				if(atomRuleSide[i] == 1){ruleSide[vars[j].getIndex()] = 1;}
				else{ruleSide[vars[j].getIndex()] = 2;}
			}
		}
		
		//パターン内の変数に対してMinIntChangeを行う順番を作成
		{
			int index = 0;
			int[] temp = new int[ruleSide.length];
			MIC_index = new int[ruleSide.length];
			for(int i=0,len=ruleSide.length;i<len;++i){
				if(ruleSide[i] == 1){
					if(varTypeArray[i] == -1){leftLastQuantIdx = index;}
					temp[i] = index;
					++index;
				}
			}
			//System.out.println("leftLastQuantIdx " + leftLastQuantIdx);
			for(int i=0,len=ruleSide.length;i<len;++i){
				if(ruleSide[i] == 2){temp[i] = index;++index;}
			}
			for(int i=0,len=ruleSide.length;i<len;++i){
				MIC_index[temp[i]] = i;
			}
			//System.out.println(Arrays.toString(MIC_index));
		}
		
		minValue = new float[varNum];
		maxValue = new float[varNum];
		minValueInteger = new short[varNum];
		maxValueInteger = new short[varNum];
		
		ArrayList<ArrayList<Integer>> nullCheckList = new ArrayList<ArrayList<Integer>>();
		for(int i=0;i<varNum;++i){
			nullCheckList.add(new ArrayList<Integer>());
		}
		float[] sumArray = new float[varNum];
		int[] sumCount = new int[varNum];
		
		int cnt = 0;
		nominalIndex = new int[varNum];
		for(int i=0;i<varNum;++i){
			minValue[i] = 0.0f;
			maxValue[i] = 0.0f;
			if(varTypeArray[i] != -1){nominalIndex[i] = cnt++;}
			else{nominalIndex[i] = -1;}
		}
		nominalCount = cnt+1;
		ConcurrentHashMap<String,Integer>[] hash1 = new ConcurrentHashMap[nominalCount];//String to Integer
		ConcurrentHashMap<Integer,String>[] hash2 = new ConcurrentHashMap[nominalCount];//Integer to String
		hash1[0] = new ConcurrentHashMap<String,Integer>();
		hash2[0] = new ConcurrentHashMap<Integer,String>();
		for(int i=1;i<nominalCount;++i){//key以外に"0：_*" を登録(ワイルドカード) 
			hash1[i] = new ConcurrentHashMap<String,Integer>();
			hash1[i].put("_*", 0);
			hash2[i] = new ConcurrentHashMap<Integer,String>();
			hash2[i].put(0, "_*");
		}
		//データベース読み込み
		for(int i=0,n=occur.size();i<n;++i){
			String[] array = occur.get(i);
			float[] data = new float[varNum];
			for(int j=0;j<varNum;++j){
				if(nominalIndex[j] == -1){//numerical
					Float num = 0.0f;
					try{
						num = Float.parseFloat(array[j]);
						data[j] = num;
						sumArray[j] += num;
						++sumCount[j];
					}catch (Exception e) {
						//数値化エラー: 欠損値と判断
						//System.out.println(j + " parseFloat Error : " + e + " (NULL -> 0)");
						data[j] = 0.0f;
						nullCheckList.get(j).add(i);
					}
					if(i == 0){minValue[j] = num;maxValue[j] = num;}
					else if(minValue[j] > num){minValue[j] = num;}
					else if(maxValue[j] < num){maxValue[j] = num;}
				}
				else{//category
					int v = 0;
					int idx = nominalIndex[j];
					if(hash1[idx].get(array[j]) == null){
						v = hash1[idx].size();//現時点でのハッシュのサイズを新たなユニークIDとする
						hash1[idx].put(array[j], hash1[idx].size());
						hash2[idx].put(hash2[idx].size(), array[j]);
					}
					else{
						v = hash1[idx].get(array[j]);
					}
					data[j] = 1.0f * v;
				}
				
			}
			database.add(data);
		}
		//質的属性のサポートカウントを調べる
		for(int i=0,n=occur.size();i<n;++i){
			float[] array = database.get(i);
			for(int j=0;j<varNum;++j){
				if(nominalIndex[j] == 1){
					int idx = nominalIndex[j];
					int[] supArray = supportCategory.get(j);
					if(supArray == null){
						supArray = new int[hash1[idx].size()];
						supportCategory.put(j, supArray);
					}
					supArray[(int)array[j]]++;
				}
			}
		}
		for(Integer i : supportCategory.keySet()){
			int[] array = supportCategory.get(i);
			//System.out.println(i + " " + Arrays.toString(array));
		}
		
		for(int i=0;i<varNum;++i){
			float mean = sumArray[i] / sumCount[i];
			ArrayList<Integer> array = nullCheckList.get(i);
			for(Integer index : array){
				database.get(index)[i] = mean;//欠損値を平均値で埋め立て
			}
		}
		for(int i=0;i<varNum;++i){
			System.out.println(i + " : " + minValue[i] + " ~ " + maxValue[i]);
		}
		
		dict.set(nominalIndex, hash1, hash2);
		keySize = hash1[0].size();
		minSupp = (float)Math.floor((minSupRate * (keySize)));
		if(minSupp == 0){minSupp = 1;}
		System.out.println("minSupp:" + minSupp);
		
		objects = occur.size();
		int_count_o = (occur.size()+1)/(32) + 1 ;
		
		databaseInteger = new short[occur.size()][varNum];
		sortDatabase = new short[varNum][occur.size()];
		sortIndex = new int[varNum][occur.size()];
		//ここから変数ごとに処理を開始
		long startHashTime = System.currentTimeMillis();
		for(int i=0;i<varNum;++i){
			minValueInteger[i] = 0;//順序値の最小
			maxValueInteger[i] = (short) (occur.size()-1);//順序値の最大

			//量的属性 -> 順序値の処理を開始
			if(nominalIndex[i] == -1){
				for(short j=0;j<occur.size();++j){
					sortDatabase[i][j] = j;//IDリスト作成
				}
				ArrayList<ArrayList<Short>> rank2ID_List = new ArrayList<ArrayList<Short>>();
				ArrayList<Short> rank2ID_Array = new ArrayList<Short>();
				ArrayList<Float> restore_Array = new ArrayList<Float>();
				quickSort(i, sortDatabase[i], 0, occur.size()-1);//属性値の昇順でIDリストをソート

				short index = 0;//順序値
				float last = database.get(sortDatabase[i][0])[i];
				for(int j=0;j<occur.size();++j){
					sortIndex[i][sortDatabase[i][j]] = j;
					if(last != database.get(sortDatabase[i][j])[i]){//属性値が変わったら更新作業
						//順序値 -> 属性値
						restore_Array.add(last);
						//順序値 -> IDリスト
						rank2ID_List.add(rank2ID_Array);
						rank2ID_Array = new ArrayList<Short>();
						++index;
					}
					rank2ID_Array.add(sortDatabase[i][j]);//同じ順序値を持つタプルidを同じリストに格納していく
					last = database.get(sortDatabase[i][j])[i];
					databaseInteger[sortDatabase[i][j]][i] = index;//データベースに順序値を置いていく
				}

				restore_Array.add(last);
				rank2ID_List.add(rank2ID_Array);

				maxValueInteger[i] = index;
				rank2ID.put(i, rank2ID_List);
				restoreMap.put(i, restore_Array);
				System.out.println(i + " restore_Array.size : " + restore_Array.size());
			}
			else{
				//質的属性は値をそのまま float -> short に変換するだけ
				for(int j=0;j<occur.size();++j){
					databaseInteger[j][i] = (short)database.get(j)[i];
				}
			}
		}
		System.out.println("makeHash time = " + (System.currentTimeMillis() - startHashTime));
		for(int j=0;j<occur.size();++j){
			//System.out.println(Arrays.toString(databaseInteger[j]));
		}
		System.out.println("keySize " + keySize);
		System.out.println("varNum " + varNum);
	}
	/**
	 * ルールテンプレートを生成
	 * @param array:short[varNum]  質的属性の値(順序値)を入れる配列
	 * @param index array[index]を操作
	 * 例: templateList(0): A:[ * ], B:[1], C:[-3.768(0), -0.529(176)], D:[-0.02(0), 7.84(106)], E:[yes], 
	 */
	private void makeTemplate(short[] array, int index){
		if(index == array.length){
			ArrayList<PatternAtom> pat = new ArrayList<PatternAtom>();
			for(int i=0;i<varNum;++i){
				PatternAtom atom = null;
				if(varTypeArray[i] == 0){//id
					atom = new PatternAtom(i, (short) 0);
				}
				else if(varTypeArray[i] == 1){//category
					atom = new PatternAtom(i, array[i]);
				}
				else if(varTypeArray[i] == -1){//num
					atom = new PatternAtom(i, minValueInteger[i], maxValueInteger[i]);
				}
				pat.add(atom);
			}
			IntervalPattern IP = new IntervalPattern(pat);
			templateList.add(IP);
			return;
		}
		int length = 0; // カテゴリ属性の順序値
		if(varTypeArray[index] == 1){//category
			length = dict.getInt2Str(index).size(); // length: 第index属性のとる順序値の数
			for(short i=1;i<length;++i){
				array[index] = i;
				makeTemplate(array, index+1);
			}
		}
		else{
			array[index] = 0;
			makeTemplate(array, index+1);
		}
	}
	/**
	 * パターンの出現集合をCSVに出力
	 */
	private void outputCSV(){
		try{
			File file = new File("result.csv");
			if(file.exists() && file.isFile() && file.canWrite()){
				PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(file)));
				
				//pw.println("% file:"+file_name+", minsup:"+minSup);
				StringBuilder sb = new StringBuilder();
				sb.append("0");
				for(int i=1;i<varNum;++i){
					sb.append(",");
					sb.append(i);
				}
				sb.append("\n");
				for(float[] data : database){
					sb.append(data[0]);
					for(int i=1;i<varNum;++i){
						sb.append(",");
						
						if(varTypeArray[i] != -1){
							String name = dict.getInt2Str(i).get((int)data[i]);
							sb.append(name);
						}
						else{
							sb.append(data[i]);
						}
					}
					sb.append("\n");
				}
				pw.print(sb.toString());
				pw.close();
			}
			else{
				System.out.println("ファイルに書き込めません");
			}
		}
		catch(IOException e){
			System.out.println(e);
		}
	}
	/*----------------------------------------------------------------------------------------------------------------*/
	/*----------------------------------------------------------------------------------------------------------------*/
	public class IntervalPattern{
		ArrayList<PatternAtom> pattern = null;
		private int patLength = 0;
		private int numQuant = 0;//数値属性の数
		private int lastChanged = -1;
		private short preValue = 0;//区間変更前の値1つ
		private int[] extent;
		private int[] extentA;//ルールの左辺の出現集合
		private int support = 0;//support count
		private int supportA = 0;
		public short[] keyCount;
		public short[] keyCountA;
		
		//[i番目の属性の][順序値j] = を持つオブジェクトの数
		//数値属性以外の部分はnullであることに注意
		private short[][] rankCount;
		//private ArrayList<Integer> occur;
		//最初のIP
		public IntervalPattern(ArrayList<PatternAtom> pat){
			pattern = pat;
			patLength = pat.size();
			for(PatternAtom atom : pattern){
				float v = atom.getValue();
				if(v == -1){
					++numQuant;
				}
			}
		}
		public IntervalPattern(IntervalPattern IP){//heavy?
			numQuant = IP.getNumQuant();
			pattern = new ArrayList<PatternAtom>();
			for(int i=0,n=IP.length();i<n;++i){
				PatternAtom atom;
				short v = IP.getAtom(i).getValue();
				if(v == -1){
					atom = new PatternAtom(i,IP.getAtom(i).getL(),IP.getAtom(i).getU());
				}
				else{
					atom = new PatternAtom(i,v);
				}
				pattern.add(atom);
			}
			
			patLength = pattern.size();
			lastChanged = IP.getLastChanged();
			extent = new int[int_count_o];
			extentA = new int[int_count_o];
			int[] ex = IP.getExtent();
			int[] exA = IP.getExtentA();
			System.arraycopy(ex, 0, extent, 0, int_count_o);
			System.arraycopy(exA, 0, extentA, 0, int_count_o);
			
			support = IP.getSupport();
			supportA = IP.getSupportA();
			keyCount = IP.keyCount.clone();
			keyCountA = IP.keyCountA.clone();
			rankCount = new short[patLength][];
			short[][] rCount = IP.getRankCount();
			for(int i=0;i<patLength;++i){
				if(varTypeArray[i] == -1){rankCount[i] = rCount[i].clone();}
			}
		}
		public void copy(IntervalPattern IP){
			numQuant = IP.getNumQuant();

			for(int i=0,n=IP.length();i<n;++i){
				PatternAtom atom = this.getAtom(i);
				short v = IP.getAtom(i).getValue();
				if(v == -1){
					atom.set(IP.getAtom(i).getL(), IP.getAtom(i).getU());
				}
				else{
					atom.setValue(v);
				}
			}
			patLength = pattern.size();
			lastChanged = IP.getLastChanged();
			
			int[] ex = IP.getExtent();
			int[] exA = IP.getExtentA();
			System.arraycopy(ex, 0, extent, 0, int_count_o);
			System.arraycopy(exA, 0, extentA, 0, int_count_o);
			support = IP.getSupport();
			supportA = IP.getSupportA();
			keyCount = IP.keyCount.clone();
			keyCountA = IP.keyCountA.clone();
			short[][] rCount = IP.getRankCount();
			for(int i=0;i<patLength;++i){
				if(varTypeArray[i] == -1){rankCount[i] = rCount[i].clone();}
			}
		}
		public ArrayList<PatternAtom> getPattern(){return pattern;}
		public PatternAtom getAtom(int i){return pattern.get(i);}
		public int length(){return patLength;}
		public int[] getExtent(){return extent;}
		public void setExtent(int[] occ){
			extent = occ;
		}
		public int[] getExtentA(){return extentA;}
		public void setExtentA(int[] occ){
			extentA = occ;
		}
		public int getSupport(){return support;}
		public void setSupport(int supp){
			support = supp;
		}
		public int getSupportA(){return supportA;}
		public void setSupportA(int supp){
			supportA = supp;
		}
		public void setRankCount(short[][] array){
			rankCount = array;
		}
		public short[][] getRankCount(){
			return rankCount;
		}
		public int getNumQuant(){return numQuant;}
		public boolean changeLeft(int i){
			PatternAtom atom = pattern.get(i);
			preValue = atom.getL();
			boolean ret = atom.changeLeft();
			if(ret){lastChanged = i;}
			return ret;
		}
		public boolean changeRight(int i){
			PatternAtom atom = pattern.get(i);
			preValue = atom.getU();
			boolean ret = atom.changeRight();
			if(ret){lastChanged = i;}
			return ret;
		}
		public int getLastChanged(){
			return lastChanged;
		}
		public short getPreValue(){
			return preValue;
		}
		public String toString(){
			String str = "";
			for(PatternAtom atom : pattern){
				str += atom;
				str += ", ";
			}
			return str;
		}
	}
	public class PatternAtom{
		private int id = 0;
		private short value = -1;//num:-1, id:0, category:1~*
		private short lower;
		private short upper;
		
		public PatternAtom(int id, short v){
			this.id = id;
			value = v;
		}
		public PatternAtom(int id, short l, short u){
			this.id = id;
			value = -1;
			lower = l;
			upper = u;
		}
		public short getValue(){return value;}
		public short getL(){return lower;}
		public short getU(){return upper;}
		public void set(short l, short u){
			lower = l;
			upper = u;
		}
		public void setValue(short v){
			value = v;
		}
		public void setL(short l){
			lower = l;
		}
		public void setU(short u){
			upper = u;
		}
		public boolean changeLeft(){
			short newL = (short) (lower + 1);
			if(newL > upper){return false;}
			lower = newL;
			return true;
		}
		public boolean changeRight(){
			short newU = (short) (upper - 1);
			if(newU < lower){return false;}
			upper = newU;
			return true;
		}
		public String toString(){
			String str = "";
			char var = (char) ('A' + id);
			if(value == 0){
				str = (var + ":[ * ]");
			}
			else if(value > 0){
				String name = dict.getInt2Str(id).get((int)value);
				str = (var + ":[" + name + "]");
			}
			else{
				//System.out.println(id + " " + lower + " " + upper);
				float L = restoreMap.get(id).get(lower);
				//float L = lower;
				float U = restoreMap.get(id).get(upper);
				//float U = upper;
				str = (var + ":[" + L + "(" + lower + "), " + U + "(" + upper + ")]");
			}
			return str;
		}
	}
	/**
	 * 質的属性と, それに対するユニークIDを格納したハッシュのクラス
	 */
	public class Dictionary{
		private int[] OccurIdx2HashIdx;//引数番号からカテゴリ属性としての番号に変換(Hashを引っ張る用)
		public ConcurrentHashMap<String,Integer>[] Str2Int;//カテゴリ属性の名前 -> ユニークID
		public ConcurrentHashMap<Integer,String>[] Int2Str;//ユニークID -> カテゴリ属性の名前
		
		public void set(int[] ID, ConcurrentHashMap<String,Integer>[] Str, ConcurrentHashMap<Integer,String>[] Int){
			OccurIdx2HashIdx = ID;
			Int2Str = Int;
			Str2Int = Str;
		}
		public ConcurrentHashMap<String,Integer> getStr2Int(int occIdx){
			return Str2Int[OccurIdx2HashIdx[occIdx]];
		}
		public ConcurrentHashMap<Integer,String> getInt2Str(int occIdx){
			return Int2Str[OccurIdx2HashIdx[occIdx]];
		}
	}
	/*----------------------------------------------------------------------------------------------------*/
	//クイックソート用の関数群
	private int pivot(int id, short[] array, int i, int j){
		int k = i+1;
		while(k <= j && database.get(array[i])[id] == database.get(array[k])[id]){++k;}
		if(k > j){return -1;}
		if(database.get(array[i])[id] >= database.get(array[k])[id]){return i;}
		return k;
	}
	private int partition(int id, short[] array, int i, int j, float x){
		int l=i,r=j;
		while(l <= r){
			while(l <= j && database.get(array[l])[id] < x){++l;}
			while(r >= i && database.get(array[r])[id] >= x){--r;}
			if(l > r){break;}
			short temp = array[l];
			array[l] = array[r];
			array[r] = temp;
			++l;
			--r;
		}
		return l;
	}
	private void quickSort(int id, short[] array, int i, int j){
		if(i == j){return;}
		int p = pivot(id, array, i, j);
		if(p != -1){
			int k = partition(id, array, i, j , database.get(array[p])[id]);
			quickSort(id, array, i, k-1);
			quickSort(id, array, k, j);
		}
	}
	/*-------------------------------------------------------------------------------*/
	/** ルールマイニングを行うクラスにわたす構造をまとめたクラス 名前は暫定*/
	public class Structure{
		Conjunction conjunction;
		Dictionary dict;
		int keySize;
		float minSupp;
		float minSuppRate;
		float minConf;
		int int_count_o;
		int patternLength;
		int leftLastQuantIdx;
		DataBase miningDB;
		ArrayList<float[]> database;
		short[][] databaseInteger;
		short[] minValueInteger;
		short[] maxValueInteger;
		float[] minValue;
		float[] maxValue;
		int[] varTypeArray;
		int[] ruleSide;
		int[] atomRuleSide;
		int[] nominalIndex;
		ConcurrentHashMap<Integer, ArrayList<Float>> restoreMap;
		int[] MIC_index;
		ConcurrentHashMap<Integer, ArrayList<ArrayList<Short>>> rank2ID;
		ConcurrentHashMap<Integer, int[]> supportCategory;
		public Structure(){
			this.conjunction = MiningIP.this.conjunction;
			this.dict = MiningIP.this.dict;
			this.keySize = MiningIP.this.keySize;
			this.minSupp = MiningIP.this.minSupp;
			this.minSuppRate = MiningIP.this.minSupRate;
			this.minConf = MiningIP.this.minConf;
			this.int_count_o = MiningIP.this.int_count_o;
			this.patternLength = MiningIP.this.patternLength;
			this.leftLastQuantIdx = MiningIP.this.leftLastQuantIdx;
			this.miningDB = MiningIP.this.miningDatabase;
			this.database = MiningIP.this.database;
			this.databaseInteger = MiningIP.this.databaseInteger;
			this.minValueInteger = MiningIP.this.minValueInteger;
			this.maxValueInteger = MiningIP.this.maxValueInteger;
			this.minValue = MiningIP.this.minValue;
			this.maxValue = MiningIP.this.maxValue;
			this.varTypeArray = MiningIP.this.varTypeArray;
			this.ruleSide = MiningIP.this.ruleSide;
			this.atomRuleSide = MiningIP.this.atomRuleSide;
			this.nominalIndex = MiningIP.this.nominalIndex;
			this.restoreMap = MiningIP.this.restoreMap;
			this.MIC_index = MiningIP.this.MIC_index;
			this.rank2ID = MiningIP.this.rank2ID;
			this.supportCategory = MiningIP.this.supportCategory;
		}
	}
	public void setCompSupMode(int compSup_mode) {
		this.compSup_mode=compSup_mode;
		
	}
	public int getCompSupMode() {
		return this.compSup_mode;
		
	}
	public void setGenerationLimit(int generationLimit) {
		this.generationLimit= generationLimit;
		
	}
	public int getGenerationLimit() {
		return this.generationLimit;
		
	}
}
