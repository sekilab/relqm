package miningRule;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import miningRule.MiningIP.IntervalPattern;
import miningRule.MiningIP.PatternAtom;
import miningRule.MiningIP.Structure;

public class ComputingCIPRule {
	//CIPを求めるルールテンプレート
	IntervalPattern template;
	int varNum = 0;
	int SUPPORT_C = 0;//カイ二乗での枝刈りで使用
	boolean ifCutByChi2 = false;//ルールの結論部が質的属性のみであった場合trueになる
	int pruneMethod = 0;//枝刈りの方式
	float SignificanceLevel = 0;//6.63f;//3.84f;
	float ChiConst1;//n(n-m)/m
	float ChiConst2;//(m*n)/(n-m)
	//親
	MiningIP parent;
	
	int keySize;
	float minSupp;
	float minConf;
	int int_count_o;
	int patternLength;
	int leftLastQuantIdx;
	short[][] databaseInteger;
	short[] maxValueInteger;
	short[] minValueInteger;
	float[] maxValue;
	float[] minValue;
	int[] varTypeArray;
	int[] ruleSide;
	int[] nominalIndex;
	ConcurrentHashMap<Integer, ArrayList<Float>> restoreMap;
	int[] MIC_index;
	ConcurrentHashMap<Integer, ArrayList<ArrayList<Short>>> rank2ID;
	ConcurrentHashMap<Integer, int[]> supportCategory;//get(ある質的属性について)[特定の属性値の] = サポートカウント
	private int patternCount = 0;
	private int ruleCount = 0;
	float bestFitness = 0;
	IntervalPattern bestPatternFitness = null;
	float bestChiSq = 0;
	IntervalPattern bestPatternChiSq = null;
	
	public ComputingCIPRule(IntervalPattern template, MiningIP parent){
		this.parent = parent;
		Structure data = parent.dataStructures;
		this.template = template;
		varNum = template.length();
		this.keySize = data.keySize;
		this.minSupp = data.minSupp;
		this.minConf = data.minConf;
		this.int_count_o = data.int_count_o;
		this.leftLastQuantIdx = data.leftLastQuantIdx;
		this.patternLength = data.patternLength;
		this.databaseInteger = data.databaseInteger;
		this.maxValueInteger = data.maxValueInteger;
		this.minValueInteger = data.minValueInteger;
		this.maxValue = data.maxValue;
		this.minValue = data.minValue;
		this.varTypeArray = data.varTypeArray;
		this.ruleSide = data.ruleSide;
		this.nominalIndex = data.nominalIndex;
		this.restoreMap = data.restoreMap;
		this.MIC_index = data.MIC_index;
		this.rank2ID = data.rank2ID;
	}
	public IntervalPattern process(){
		{
			boolean rightNominalOnly = true;
			int index = 0;
			for(PatternAtom atom : template.pattern){
				if(ruleSide[index] == 2 && atom.getValue() == -1){rightNominalOnly = false;}
				++index;
			}
			//System.out.println(rightNominalOnly);
			if(rightNominalOnly){
				ifCutByChi2 = true;
				countSupport();//supCが固定可能 → 計算
			}
		}

		long StartTime = System.nanoTime();
		boolean clo_flag = first_closure(template);
		if(clo_flag){
			++patternCount;
			float sup = template.getSupport();
			float conf = sup / template.getSupportA();
			if(sup >= minSupp && conf >= minConf){
				++ruleCount;
			}
			System.out.println(" start IP : " + template);
			
			MinIntChange(template, 0, true);
			
			System.out.println(" #CIP = " + patternCount + ", #(strong rules) = " + ruleCount);
			if(patternCount != 0){
				System.out.println("MIC time " + (long)((System.nanoTime() - StartTime)*0.000001));
				
				//return bestPatternChiSq;//カイ二乗最大のルールを返す
				return bestPatternFitness;//Fitness最大のルールを返す
			}
		}
		return null;
	}
	
	/////////////////////////////////////////////////////////////////////////
	/** 再帰関数MinIntChange 
	 * @param IP 元のCIP
	 * @param n ルールの左からn番目を操作
	 * @param free trueのときのみ区間の右側を操作可能
	 * */
	private void MinIntChange(IntervalPattern IP, int n, boolean free){
		if(n >= IP.length()){return;}
		int index = MIC_index[n];//ルールのn番目 → パターンIPのindex番目
		
		if(IP.getAtom(index).getValue() == -1){//n番目が量的属性	
			if(free){
				IntervalPattern IP_nr = parent.new IntervalPattern(IP);
				boolean c_flag = IP_nr.changeRight(index);
				if(c_flag){
					boolean clo_flag = new_closure(IP_nr,n);
					if(clo_flag){
						++patternCount;
						float fitness = computeFitness(IP_nr);							
						if(bestFitness < fitness){
							bestFitness = fitness;
							bestPatternFitness = IP_nr;
						}
						float sup = IP_nr.getSupport();
						float supA = IP_nr.getSupportA();
						float conf = sup / supA;
						float chi = computeChi(IP_nr.getSupportA(), IP_nr.getSupport());
						if((conf >= minConf)){
							if(SignificanceLevel <= chi){
								++ruleCount;
							}
						}
						if(Float.isFinite(chi) && bestChiSq < chi){
							//Infinity以外で最もchiが高かったものを記録
							bestChiSq = chi;
							bestPatternChiSq = IP_nr;
						}
						boolean p_flag = prune(IP_nr,n,pruneMethod);//再帰処理を続ける
						if(!p_flag){
							MinIntChange(IP_nr, n, true);
						}
					}
				}
			}
			IntervalPattern IP_nl = parent.new IntervalPattern(IP);
			boolean c_flag = IP_nl.changeLeft(index);
			if(c_flag){
				boolean clo_flag = new_closure(IP_nl,n);
				if(clo_flag){
					++patternCount;
					float fitness = computeFitness(IP_nl);							
					if(bestFitness < fitness){
						bestFitness = fitness;
						bestPatternFitness = IP_nl;
					}
					float sup = IP_nl.getSupport();
					float supA = IP_nl.getSupportA();
					float conf = sup / supA;
					float chi = computeChi(IP_nl.getSupportA(), IP_nl.getSupport());
					if((conf >= minConf)){
						if(SignificanceLevel <= chi){
							++ruleCount;
						}
					}
					if(Float.isFinite(chi) && bestChiSq < chi){
						bestChiSq = chi;
						bestPatternChiSq = IP_nl;
					}
					boolean p_flag = prune(IP_nl,n,pruneMethod);//再帰処理を続ける
					if(!p_flag){
						MinIntChange(IP_nl, n, false);
					}
				}
			}
		}
		MinIntChange(IP, n+1, true);
	}
	/** 改良したclosure関数*/
	private boolean new_closure(IntervalPattern IP, int changed){
		int changedIndex = MIC_index[changed];

		int length = IP.length();
		ArrayList<ArrayList<Short>> list = rank2ID.get(changedIndex);//変更した区間のrank2IDリスト取得
		short deletedValue = IP.getPreValue();
		int[] extent = IP.getExtent();
		int[] extentA = IP.getExtentA();
		short[][] rankCount = IP.getRankCount();
		
		//区間から弾かれた値を持つオブジェクトのIDリストを取得(出現集合に含まれないものも入るがあとで照合するので問題ない)
		ArrayList<Short> deletedIDs = list.get(deletedValue);
		for(int i=0;i<changed;++i){
			int MICIndex = MIC_index[i];
			if(varTypeArray[MICIndex] == -1){//quant
				PatternAtom atom = IP.getAtom(MICIndex);
				short Li = atom.getL();//左端
				short Ui = atom.getU();//右端
				int del_countL = 0;
				int del_countU = 0;
				for(short index : deletedIDs){
					if((extent[index/32] & (1 << (index&31))) != 0){//indexは出現集合に含まれていたもの
						short value = databaseInteger[index][MICIndex];
						if(Li == value){
							++del_countL;//atomの左端から消えたobjあり
						}
						else if(Ui == value){
							++del_countU;//atomの右端から消えたobjあり
						}
					}
				}
				//atomの端から消えたobjの数が出現集合内の端にあったobjの数と一致
				if(rankCount[MICIndex][Li] == del_countL || rankCount[MICIndex][Ui] == del_countU){
					//++count_closure;
					return false;
				}
			}
		}
		//ここまでppc拡張的な重複計算の確認
		//ここから飽和区間作成
		ArrayList<Short> deletedArrayA = new ArrayList<Short>();//changedより先の区間が圧縮された場合にsupAを修正するべく

		PatternAtom changedAtom = IP.getAtom(changedIndex);
		short margin = (short) (changedAtom.getU() - changedAtom.getL());
		//区間の左を狭めた
		if(deletedValue < changedAtom.getL()){
			boolean end_flag = false;
			int newValue = 0;
			for(int i=1;i<margin;++i){
				newValue =  ((int)deletedValue+i);//区間の左端の新たな値
				if(rankCount[changedIndex][newValue] != 0){//newValueの出現をパターンが持つ
					end_flag = true;
					break;
				}
				ArrayList<Short> newBorders = list.get(newValue);
				deletedArrayA.addAll(newBorders);//newValueの出現をパターンが持っていなかったので条件部からの削除処理用に保存
			}			
			if(end_flag){
				changedAtom.setL((short)newValue);
			}
			else{
				return false;
			}
		}
		else{//区間の右を狭めた
			boolean end_flag = false;
			int newValue = 0;
			for(int i=1;i<margin;++i){
				newValue = ((int)deletedValue-i);
				if(rankCount[changedIndex][newValue] != 0){
					end_flag = true;
					break;
				}
				ArrayList<Short> newBorders = list.get(newValue);
				deletedArrayA.addAll(newBorders);
			}			
			if(end_flag){
				changedAtom.setU((short)newValue);
			}
			else{
				return false;
			}
		}

		//変化させた区間から次の部分を更新
		ArrayList<Short> deletedArray = new ArrayList<Short>();
		//
		int supp = IP.getSupport();
		short[] keyCount = IP.keyCount;
		int suppA = IP.getSupportA();
		short[] keyCountA = IP.keyCountA;
		for(int i=0,n=deletedIDs.size();i<n;++i){
			short index = deletedIDs.get(i);
			int tempBit = (1 << (index&31));
			int int_count = index/32;
			if((extent[int_count] & tempBit) != 0){
				deletedArray.add(index);//indexは出現集合から削除されたオブジェクト
				extent[int_count] ^= tempBit;//出現集合から削除
				for(int j=0;j<length;++j){
					if(varTypeArray[j] == -1){
						--rankCount[j][databaseInteger[index][j]];//順序値の数を減らす
					}
				}
				short key_id = databaseInteger[index][0];
				--keyCount[key_id];
				if(keyCount[key_id] == 0){//同一keyIDが全て消滅
					--supp;
				}
			}
			if(ruleSide[changedIndex] == 1){//操作した量的変数はルールの条件部 -> supAを更新
				if((extentA[int_count] & tempBit) != 0){
					extentA[int_count] ^= tempBit;//左辺の出現集合から削除
					int key_id = databaseInteger[index][0];
					--keyCountA[key_id];
					if(keyCountA[key_id] == 0){
						--suppA;
					}
				}
			}
		}
		if(supp < minSupp){
			//++count_minsupp;
			return false;
		}
		IP.keyCount = keyCount;
		IP.setSupport(supp);
		IP.keyCountA = keyCountA;
		IP.setSupportA(suppA);
		
		
		//区間の両端が、削除されたオブジェクトで構成されてないか確認
		for(int i=changed+1;i<length;++i){
			int MICIndex = MIC_index[i];
			if(varTypeArray[MICIndex] == -1){//quant
				PatternAtom atom_i = IP.getAtom(MICIndex);
				int margin_i = (atom_i.getU() - atom_i.getL());
				ArrayList<ArrayList<Short>> list_i = rank2ID.get(MICIndex);
				short Li = atom_i.getL();
				if(rankCount[MICIndex][Li] == 0){//左端の圧縮が必要
					ArrayList<Short> lower = list_i.get(Li);//System.out.println(Li);
					deletedArrayA.addAll(lower);
					boolean finish = false;
					for(int j=0;j<margin_i;++j){
						boolean c_left = atom_i.changeLeft();
						if(!c_left){return false;}
						if(rankCount[MICIndex][atom_i.getL()] != 0){
							finish = true;
							break;
						}
						ArrayList<Short> new_lower = list_i.get(atom_i.getL());
						deletedArrayA.addAll(new_lower);
					}
					if(!finish){return false;}
				}
				short Ui = atom_i.getU();
				if(rankCount[MICIndex][Ui] == 0){//右端の圧縮が必要
					ArrayList<Short> upper = list_i.get(Ui);//System.out.println(Ui);
					deletedArrayA.addAll(upper);
					boolean finish = false;
					for(int j=0;j<margin_i;++j){
						boolean c_right = atom_i.changeRight();
						if(!c_right){return false;}
						if(rankCount[MICIndex][atom_i.getU()] != 0){
							finish = true;
							break;
						}
						ArrayList<Short> new_upper = list_i.get(atom_i.getU());
						deletedArrayA.addAll(new_upper);
					}
					if(!finish){return false;}
				}
			}
		}
		if(ruleSide[changedIndex] == 1){//条件部のみのサポートを更新
			suppA = IP.getSupportA();
			for(short id : deletedArrayA){
				int int_count = id/32;
				int tempBit = (1 << (id&31));
				if((extentA[int_count] & tempBit) != 0){
					extentA[int_count] ^= tempBit;
					int key_id = databaseInteger[id][0];
					--keyCountA[key_id];
					if(keyCountA[key_id] == 0){
						--suppA;
					}
				}
			}
			IP.keyCountA = keyCountA;
			IP.setSupportA(suppA);
		}
		float support = supp;
		float supportA = suppA;
		float conf = support / supportA;
		if((changed) > leftLastQuantIdx && conf < minConf){
			//操作した変数の次の変数がルールの結論部 & confがしきい値未満
			return false;
		}
		return true;
	}
	
	/** 
	 * ルールのFitnessを計算
	 * @param IP Fitnessを求めるルールとなる区間パターン
	 * @return ルールのFitness値
	 */
	private float computeFitness(IntervalPattern IP){
		float supAC = IP.getSupport();
		float supA = IP.getSupportA();
		if(supAC < minSupp || (supAC/supA) < minConf){return -1;}
		int length = IP.length();
		float gain = (supAC) - minConf*(supA);
		//System.out.println("gain:"+gain);
		float fitness = gain;
		if(gain > 0){
			for(int i=0;i<length;++i){
				if(varTypeArray[i] == -1){
					PatternAtom atom = IP.getAtom(i);
					ArrayList<Float> array = restoreMap.get(i);
					float L = array.get(atom.getL());
					float U = array.get(atom.getU());
					float prop = 1.0f - ((U - L) / (maxValue[i] - minValue[i]));
					float prop2 = prop*prop;
					fitness *= (prop2);
				}
			}
		}
		return fitness;
	}
	
	private boolean first_closure(IntervalPattern IP){
		
		int length = IP.length();
		short[][] convexHull = new short[length][2];
		for(int i=0;i<length;++i){
			convexHull[i][0] = maxValueInteger[i];
			convexHull[i][1] = minValueInteger[i];
		}

		int hit = 0;
		int supA = 0;
		short[] keyCheck = new short[keySize];
		short[] keyCheckA = new short[keySize];
		int[] extent = new int[int_count_o];
		int[] extentA = new int[int_count_o];
		short[][] rankCount = new short[length][];
		for(int i=0;i<length;++i){
			if(varTypeArray[i] == -1){rankCount[i] = new short[restoreMap.get(i).size()];}
		}
		for(int j = 0, len = databaseInteger.length;j<len;++j){
			int keyID = databaseInteger[j][0];

			boolean match = true;
			for(int i=1;i<length;++i){
				PatternAtom atom = IP.getAtom(i);
				if(varTypeArray[i] == -1){
					if(databaseInteger[j][i] < atom.getL() || atom.getU() < databaseInteger[j][i]){
						match = false;
						break;
					}
				}
				else{
					int value = atom.getValue();
					if(value != 0 && databaseInteger[j][i] != value){
						match = false;
						break;
					}
				}
			}
			if(match){
				extent[j / 32] |= 1 << (j % 32);//出現集合生成
				if(keyCheck[keyID] == 0){
					++hit;
				}
				++keyCheck[keyID];
				for(int i=0;i<length;++i){
					//PatternAtom atom = IP.getAtom(i);
					if(varTypeArray[i] == -1){
						++rankCount[i][databaseInteger[j][i]];//順序値の数を加算
						if(convexHull[i][1] < databaseInteger[j][i]){convexHull[i][1] = databaseInteger[j][i];}//max
						if(databaseInteger[j][i] < convexHull[i][0]){convexHull[i][0] = databaseInteger[j][i];}//min
					}
				}
			}
		}
		//System.out.println(" hit " + hit + ", supA " + supA);
		if(hit < minSupp){return false;}
		for(int i=0;i<length;++i){
			
			PatternAtom atom = IP.getAtom(i);
			if(varTypeArray[i] == -1){
				atom.set(convexHull[i][0], convexHull[i][1]);
				//System.out.println(Arrays.toString(rankCount[i]));
			}
		}
		
		for(int j = 0, len = databaseInteger.length;j<len;++j){
			int keyID = databaseInteger[j][0];
			boolean matchA = true;//左辺の一致
			for(int i=1;i<length;++i){
				PatternAtom atom = IP.getAtom(i);
				if(varTypeArray[i] == -1){
					if(databaseInteger[j][i] < atom.getL() || atom.getU() < databaseInteger[j][i]){
						if(ruleSide[i] == 1){matchA = false;}
						//break;
					}
				}
				else{
					int value = atom.getValue();
					if(value != 0 && databaseInteger[j][i] != value){
						if(ruleSide[i] == 1){matchA = false;}
						//break;
					}
				}
			}
			if(matchA){
				extentA[j / 32] |= 1 << (j % 32);//左辺の出現集合生成
				if(keyCheckA[keyID] == 0){
					++supA;
				}
				++keyCheckA[keyID];
			}
		}
		IP.keyCount = keyCheck;
		IP.keyCountA = keyCheckA;
		IP.setExtent(extent);
		IP.setExtentA(extentA);
		IP.setSupport(hit);
		IP.setSupportA(supA);
		IP.setRankCount(rankCount);
		return true;
	}
	/** 枝刈りの種類をflagで指定 枝刈りする場合はtrueを返す*/
	private boolean prune(IntervalPattern IP, int n, int flag){
		boolean ret = false;
		if(flag == 1){
			//bestなfitnessを探索するための枝刈り
			if((n > leftLastQuantIdx && (IP.getSupport()-minConf*IP.getSupportA()) < bestFitness)){
				ret = true;
			}
		}
		else if(flag == 2 && ifCutByChi2){
			//bestなカイ二乗値を探索するための枝刈り
			float chi_bb = computeChi_bb(IP.getSupport());
			float chi_ab0 = computeChi_ab0(IP.getSupportA() - IP.getSupport());
			if((chi_bb >= chi_ab0 && chi_bb < bestChiSq) || (chi_bb < chi_ab0 && chi_ab0 < bestChiSq)){
				ret = true;
			}
		}
		else if(flag == 3 && ifCutByChi2){
			//有意水準以上のカイ二乗値を探索するための枝刈り
			float chi_bb = computeChi_bb(IP.getSupport());
			float chi_ab0 = computeChi_ab0(IP.getSupportA() - IP.getSupport());
			if((chi_bb >= chi_ab0 && chi_bb < SignificanceLevel) || (chi_bb < chi_ab0 && chi_ab0 < SignificanceLevel)){
				ret = true;
			}
		}
		return ret;
	}
	private void countSupport(){
		int supA=0,supC=0;
		int[] keyCheck = new int[keySize];
		int[] A_Check = new int[keySize];
		int[] C_Check = new int[keySize];
		for(int j=0,len=databaseInteger.length;j<len;++j){
			int keyID = databaseInteger[j][0];
			boolean matchA = true, matchC = true;
			for(int i=0;i<template.length();++i){
				PatternAtom atom = template.getAtom(i);
				if(varTypeArray[i] == -1){
					if(databaseInteger[j][i] < atom.getL() || atom.getU() < databaseInteger[j][i]){
						if(ruleSide[i] == 1){matchA = false;}
						else{matchC = false;}
					}
				}
				else if(varTypeArray[i] == 1){
					if(atom.getValue() != 0 && databaseInteger[j][i] != atom.getValue()){
						if(ruleSide[i] == 1){matchA = false;}
						else{matchC = false;}
					}
				}
			}
			if(matchC){
				if(C_Check[keyID] != 1){
					++SUPPORT_C;
					C_Check[keyID] = 1;
				}
			}
		}
		float m = SUPPORT_C;
		float n = keySize;
		ChiConst1 = n * (n-m) / m;
		ChiConst2 = (m * n) / (n - m);
	}
	private float computeChi(int x, int y){
		float chi = 0;
		float m = SUPPORT_C;
		float n = keySize;
		float mdn = m/n;
		float nmdn = (n - m) / n;
		float Rt1 = y;
		float R1 = x;
		float Rf1 = R1 - Rt1;
		float Rt2 = m - Rt1;
		float Rf2 = n - R1 - Rt2;
		float R2 = n - R1;
		float c1 = (Rt1-R1*mdn)*(Rt1-R1*mdn)/(R1*mdn);
		float c2 = (Rf1-R1*nmdn)*(Rf1-R1*nmdn)/(R1*nmdn);
		float c3 = (Rt2-R2*mdn)*(Rt2-R2*mdn)/(R2*mdn);
		float c4 = (Rf2-R2*nmdn)*(Rf2-R2*nmdn)/(R2*nmdn);
		chi = c1 + c2 + c3 + c4;
		return chi;
	}
	/** chi^2(b,b) = (n*(n-m)/m) * (b/(n-b)) */
	private float computeChi_bb(int val){
		float chi = 0;
		float R1 = val;
		chi = (ChiConst1 * R1) / (keySize - R1);
		return chi;
	}
	/** chi^2(a-b,0) */
	private float computeChi_ab0(int val){
		float chi = 0;
		float R1 = val;
		chi = (ChiConst2 * R1) / (keySize - R1);
		return chi;
	}
}
