package miningRule;

import java.util.ArrayList;
import java.util.Arrays;

import database.obj.Atom;
import database.obj.Conjunction;
import database.obj.Term;
import database.obj.Var;
import miningRule.MiningIP.Dictionary;
import miningRule.MiningIP.IntervalPattern;
import miningRule.MiningIP.PatternAtom;
import miningRule.MiningIP.Structure;

public class MiningCAIM {
	MiningIP parent;
	Conjunction conj;
	ArrayList<float[]> database;//使うConjunctionの出現集合を形式文脈のように扱う
	ArrayList<IntervalPattern> patternList = new ArrayList<IntervalPattern>();

	private Dictionary dict;//質的属性をユニークIDに変換するためのハッシュ等
	private int[] varTypeArray;
	private int[] ruleSide;
	private int[] atomRuleSide;//ConjunctionのAtomの位置
	private float[] minValue;//n番目の属性の最大値
	private float[] maxValue;//n番目の属性の最小値
	private int numQuant = 0;
	private float[] attr_Length;
	private int keySize = 0;
	private int labelIndex = 0;
	private String labelName;
	private float[][] columns;
	private ArrayList<Float>[] boundaryList;
	private ArrayList<Float>[] D;
	private ArrayList<Intervals> intervalsList = new ArrayList<Intervals>();
	int minSupp = 0;
	float minConf = 0.6f;
	int numLabel = 0;
	public MiningCAIM(Structure data){
		this.conj = data.conjunction;
		database = data.database;
		this.dict = data.dict;
		keySize = this.dict.getInt2Str(0).size();
		varTypeArray = data.varTypeArray;
		ruleSide = data.ruleSide;
		atomRuleSide = data.atomRuleSide;
		minValue = data.minValue;
		maxValue = data.maxValue;
		numQuant = minValue.length;
		attr_Length = new float[numQuant];
		for(int i=0;i<numQuant;++i){
			attr_Length[i] = maxValue[i] - minValue[i];
		}
		minSupp = (int)data.minSupp;
		minConf = data.minConf;
		labelName = data.miningDB.getTranslator().getTargetName();
	}
	public MiningCAIM(Conjunction conj, ArrayList<float[]> db, int[] vta, int[] rs, int[] ars, float[] min, float[] max, Dictionary dict, int ms, float mc){
		this.conj = conj;
		database = db;
		this.dict = dict;
		keySize = this.dict.getInt2Str(0).size();
		varTypeArray = vta;
		ruleSide = rs;
		atomRuleSide = ars;
		minValue = min;
		maxValue = max;
		numQuant = minValue.length;
		attr_Length = new float[numQuant];
		for(int i=0;i<numQuant;++i){
			attr_Length[i] = maxValue[i] - minValue[i];
		}
		minSupp = ms;
		minConf = mc;
	}
	public void process(ArrayList<IntervalPattern> patterns){
		Atom[] atoms = conj.getAtoms();
		int it = 0;
		for(int i=0;i<atoms.length;i++) {
			Atom atom = atoms[i];
			if(atom.getName().equals(labelName)){
				Term[] args = atom.getArgs();
				Var[] vars = atom.getVars();
				labelIndex = vars[1].getIndex();
			}
		}
		System.out.println("target -> " + labelName);

		numLabel = this.dict.getInt2Str(labelIndex).size() - 1;
		System.out.println("#labels = " + numLabel);
		
		long MiningTime = System.currentTimeMillis();
		setting();
		int quantCount = 0;
		for(int i=0;i<numQuant;++i){
			if(varTypeArray[i] == -1){
				++quantCount;
				mining(i);
				System.out.println(i + " " + D[i].toString());
			}
		}
		System.out.println("quantCount " + quantCount);
		makeIntervals(new float[quantCount*2],0,0);
		System.out.println("mining time " + (System.currentTimeMillis() - MiningTime));
		
		System.out.println("listSize = " + intervalsList.size());
		for(Intervals temp : intervalsList){
			System.out.println(temp);
		}
		
		for(IntervalPattern IP : patterns){
			long StartTime = System.currentTimeMillis();
			boolean print = false;
			for(Intervals temp : intervalsList){
				/*int it = 0;
				for(int i=0;i<numQuant;++i){
					if(varTypeArray[i] == -1){
						IP.getAtom(i).set(temp.getValue(it*2), temp.getValue(it*2+1));
						++it;
					}
				}*/
				
				boolean ret = printRule(IP, temp);
				if(!print && ret){print = true;}
				
			}
			if(print){
				System.out.println("time " + (System.currentTimeMillis() - StartTime));
				System.out.println("--------------------------------------------------------------------------------------------------------------");
			}
			
			//System.out.println(" ");
		}
		
	}
	private boolean printRule(IntervalPattern IP, Intervals vals){
		int patternLength = IP.length();
		int hit = 0;
		int supA=0,supC=0;
		int[] keyCheck = new int[keySize];
		int[] A_Check = new int[keySize];
		int[] C_Check = new int[keySize];
		for(float[] data : database){
			int keyID = (int)data[0];
			//if(A_Check[keyID] == 1 && C_Check[keyID] == 1){continue;}
			boolean matchA = true, matchC = true;
			int it = 0;
			for(int i=0;i<patternLength;++i){
				PatternAtom atom = IP.getAtom(i);
				if(varTypeArray[i] == -1){
					if(data[i] < vals.getValue(it*2) || vals.getValue(it*2+1) < data[i]){
						if(ruleSide[i] == 1){matchA = false;}
						else{matchC = false;}
					}
					++it;
				}
				else{
					if(atom.getValue() != 0 && data[i] != atom.getValue()){
						if(ruleSide[i] == 1){matchA = false;}
						else{matchC = false;}
					}
				}
			}
			if(matchA){
				if(A_Check[keyID] != 1){
					++supA;
					A_Check[keyID] = 1;
				}
				if(matchC){
					if(C_Check[keyID] != 1){
						++supC;
						C_Check[keyID] = 1;
					}
					if(keyCheck[keyID] != 1){
						++hit;
						keyCheck[keyID] = 1;
					}
				}
			}
			else if(matchC){
				if(C_Check[keyID] != 1){
					++supC;
					C_Check[keyID] = 1;
				}
			}
		}
		float conf = 100.0f * hit / supA;
		if(hit < minSupp || conf < minConf){return false;}
		System.out.println("---------------------------------------------------------------------------------------------------------------");

		float a = hit;
		float b = supA - a;
		float c = supC - a;
		float d = keySize - a - b - c;
		float chi2 = (keySize*(a*d - b*c)*(a*d - b*c)) / ((a+c)*(b+d)*(a+b)*(c+d));
		float gain = (a) - minConf*((b+a));
		float fitness = gain;
		//System.out.println("gain:"+gain);
		if(gain > 0){
			int it = 0;
			for(int i=0;i<patternLength;++i){
				if(varTypeArray[i] == -1){
					PatternAtom atom = IP.getAtom(i);
					float prop = 1.0f - (vals.getValue(it*2+1) - vals.getValue(it*2)) / attr_Length[i];
					//System.out.println(" prop:"+prop);
					fitness *= (prop*prop);
					if(hit < minSupp){fitness -= keySize;}
					++it;
				}
			}
		}
		float OR = (a/b)/(c/d);
		//System.out.println(a + " " + b + " " + c + " " + d);
		StringBuilder sb_value = new StringBuilder();
		sb_value.append("supA = ");
		sb_value.append(supA);
		sb_value.append("(");
		sb_value.append(((float)supA/keySize));
		sb_value.append(")");
		sb_value.append(",supC = ");
		sb_value.append(supC);
		sb_value.append("(");
		sb_value.append(((float)supC/keySize) );
		sb_value.append(")");
		sb_value.append(",supAC = " );
		sb_value.append(hit);
		sb_value.append("(");
		sb_value.append(((float)hit/keySize));
		sb_value.append(")");
		sb_value.append(", conf = ");
		sb_value.append(conf);
		sb_value.append("\nchi2 = ");
		sb_value.append(chi2);
		sb_value.append(", oddsRatio = ");
		sb_value.append(OR);
		sb_value.append(", fitness = ");
		sb_value.append(String.format("%.8f", fitness));
		/*System.out.println("supA = " + supA + "(" + ((float)supA/keys) + ")"
				+ ",supC = " + supC + "(" + ((float)supC/keys) + ")"
				+ ",supAC = " + hit + "(" + ((float)hit/keys) + ")"
				+ ", conf = " + conf
				+ ", chi2 = " + chi2 + ", oddsRatio = " + OR + ", fitness = " + String.format("%.8f", fitness));*/
		System.out.println(sb_value.toString());
		StringBuilder sb_l = new StringBuilder();
		StringBuilder sb_r = new StringBuilder();
		Atom[] atoms = conj.getAtoms();
		int it = 0;
		for(int i=0;i<atoms.length;i++) {
			Atom atom = atoms[i];
			Term[] args = atom.getArgs();
			Var[] vars = atom.getVars();
			StringBuilder sb = new StringBuilder(atom.getName());
			sb.append("(");
			sb.append(args[0]);
			
			for(int j=1;j<args.length;j++) {
				sb.append(",");
				sb.append(args[j]);
				if(varTypeArray[vars[j].getIndex()] == -1){
					PatternAtom I = IP.getAtom(vars[j].getIndex());
					sb.append("[");
					float value_l = vals.getValue(it*2);
					sb.append(String.format("%.4f", value_l));
					sb.append(", ");
					float value_r = vals.getValue(it*2+1);
					sb.append(String.format("%.4f", value_r));
					sb.append("]");
					++it;
				}
				if(varTypeArray[vars[j].getIndex()] == 1){
					PatternAtom I = IP.getAtom(vars[j].getIndex());
					String name = dict.getInt2Str(vars[j].getIndex()).get((int)I.getValue());
					sb.append("[");
					sb.append(name);
					sb.append("]");
				}
			}
			sb.append(")");
			String atomStr = sb.toString();
			
			if(atomRuleSide[i] == 1){
				if(sb_l.length() != 0){sb_l.append(", ");}
				sb_l.append(atomStr);
			}
			else{
				if(sb_r.length() != 0){sb_r.append(", ");}
				sb_r.append(atomStr);
			}
		}
		String out = " " + sb_l.toString() + " → " + sb_r.toString();
		System.out.println(out);
		return true;
	}
	public void makeIntervals(float[] array, int index, int pos){
		if(pos >= array.length){
			Intervals intv = new Intervals(array);
			intervalsList.add(intv);
			return;
		}
		if(varTypeArray[index] == -1){
			for(int i=0,n=D[index].size()-1;i<n;++i){
				array[pos] = D[index].get(i);
				array[pos+1] = D[index].get(i+1);
				
				makeIntervals(array, index+1, pos+2);
			}
		}
		else{
			makeIntervals(array, index+1, pos);
		}
	}
	public void mining(int index){
		float GlobalCAIM = 0;
		int k = 1;
		D[index].add(minValue[index]);
		//System.out.println(index + " boundaryList[index] : " + boundaryList[index]);
		int[] checked = new int[boundaryList[index].size()];
		while(true){
			float maxCAIM = 0;
			int maxID = -1;
			for(int i=0,n=boundaryList[index].size()-1;i<n;++i){
				if(checked[i] == 1){continue;}
				ArrayList<Float> tempD = new ArrayList<Float>(D[index]);
				tempD.add(boundaryList[index].get(i));
				tempD.add(maxValue[index]);
				float caim = computeCAIM(tempD, index);
				//System.out.println("caim : " + caim);
				if(maxCAIM < caim){
					maxID = i;
					maxCAIM = caim;
				}
			}
			//System.out.println("maxCAIM : " + maxCAIM);
			if(maxID != -1 && (maxCAIM > GlobalCAIM || k < numLabel)){
				checked[maxID] = 1;
				D[index].add(boundaryList[index].get(maxID));
				GlobalCAIM = maxCAIM;
			}
			else{
				break;
			}
			++k;
			//System.out.println("D[index] : " + D[index]);
		}
		if(!D[index].contains(maxValue[index])){D[index].add(maxValue[index]);}
	}
	public float computeCAIM(ArrayList<Float> tempD, int index){
		float caim = 0;
		float n = tempD.size() - 1;
		float sum = 0;
		int it = 0;
		
		for(int r=1;r<n;++r){
			float L = tempD.get(r-1);
			float U = tempD.get(r);
			float max = 0;
			int Mr = 0;
			int[] labelCount = new int[numLabel];
			for(int i=0;i<database.size();++i){
				float value = database.get(i)[index];
				if(L < value && value < U){
					++Mr;
					labelCount[(int)(database.get(i)[labelIndex])-1]++;
				}
				
			}
			for(int i=0;i<labelCount.length;++i){
				if(max < labelCount[i]){max = labelCount[i];}
			}
			sum += (max * max / Mr);
		}
		caim = sum / n;
		return caim;
	}
	public void setting(){
		boundaryList = new ArrayList[numQuant];
		D = new ArrayList[numQuant];
		for(int i=0;i<numQuant;++i){
			boundaryList[i] = new ArrayList<Float>();
			D[i] = new ArrayList<Float>();
		}
		int size = database.size();
		columns = new float[numQuant][size];
		for(int i=0;i<numQuant;++i){
			for(int j=0;j<size;++j){
				columns[i][j] = database.get(j)[i];
			}
			Arrays.sort(columns[i]);
			//System.out.println(i + " : " + Arrays.toString(columns[i]));
			for(int j=0;j<size-1;++j){
				if(columns[i][j] != columns[i][j+1]){
					boundaryList[i].add(columns[i][j+1]);
				}
			}
			//System.out.println("  : " + boundaryList[i].toString());
		}
	}
	class Intervals{
		float[] intervals = null;
		int length = 0;
		public Intervals(float[] arg){
			length = arg.length;
			intervals = new float[length];
			for(int i=0;i<length;++i){
				intervals[i] = arg[i];
			}
		}
		public float getValue(int i){return intervals[i];}
		public String toString(){
			String str = "";
			for(int i=0;i<length;i+=2){
				str += "[";
				str += intervals[i];
				str += ",";
				str += intervals[i+1];
				str += "] ";
			}
			return str;
		}
	}
}
