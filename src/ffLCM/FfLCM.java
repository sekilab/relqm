package ffLCM;

//import java.text.DecimalFormat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import main.Mining;
import database.DataBase;
import database.DatatypeDef;
import database.ItemTable;
import database.ModeDef;
import database.Type;
import database.obj.Atom;
import database.obj.Conjunction;
import database.obj.Term;
import database.obj.Var;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Collections;

//import net.sourceforge.sizeof.SizeOf;
/**
 *
 * @author yamazaki
 *
 * ffLCM拡張を行います
 *
 */
public class FfLCM{
	//=========================================================================
	// ◆ 変数
	//=========================================================================
	/** データベース */
	private final DataBase	database;

	/**	変数の上限 */
	private int varSize;
	
	/** リテラル集合(アイテムテーブル) */
	private static ItemTable	itemtable;

	/** アトムのモード情報 */
	private static ModeDef modedef;

	/** 述語の引数の型情報 */
	private DatatypeDef datatypedef;

	/** キーのみからなる連言の出現集合 */
	private ArrayList<String[]>	occOnlyKey;

	/** 最小頻度閾値(%ではなく，実値) */
	private int		minFreq	= 0;

	/** 出力解のリスト(解を残しておくだけ) */
	private ArrayList<Conjunction>	outputs;
	
	//0:逐次| 1:新手法| 2:Krajca|
	private int parallelMode = 1;
	public void setMode(int m){parallelMode = m;}
	private int THREAD = 4;
	private int paraForParameter = 2;
	public void setThread(int t){THREAD = t;}
	private int depthLevel = 3;
	public void setDepth(int d){depthLevel = d;}	
		
	//外部から触れるためにメンバ化
	private List<Future<ArrayList<Conjunction>>> futures;
	private ExecutorService executor;
	final List<Worker> workers = new ArrayList<Worker>();
	final List<krajcaWorker> krajcaWorkers = new ArrayList<krajcaWorker>();
	//private final BlockingQueue<Task> tasks = new LinkedBlockingQueue<Task>(Integer.MAX_VALUE);
	private final ConcurrentLinkedQueue<Task> tasks = new ConcurrentLinkedQueue<Task>();
	//private List<parallelMiningNode> taskList;

	/**
	 * ffLCMの準備
	 *
	 * FfLCM ffLCM = new FfLCM(database, itemtable, modedef, datatypedef);
	 * Thread thread = new Thread(FfLCM);
	 * thread.start();
	 *
	 */
	public FfLCM() {
		database = Mining.instance().getDataBase();
	}
	public void set(ItemTable itemtable, ModeDef modedef, DatatypeDef datatypedef,int minFreq,int varSize){

		this.varSize = varSize;
		
		FfLCM.itemtable	= itemtable;//アイテムテーブル準備

		FfLCM.modedef    = modedef;//述語のモード情報

		this.setMinFreq(minFreq);//最小サポートカウント

		this.datatypedef = datatypedef;//述語の各引数の型情報　後で見直し

		outputs	= new ArrayList<Conjunction>();//解のリスト準備

		occOnlyKey	= database.coversKey(); //keyのみの出現集合
		
		
	}
	//=========================================================================
	// ◆ オーバーライドするメソッド
	//=========================================================================

	/**
	 * マイニング 開始
	 * @throws InterruptedException 
	 */
	public long run() throws InterruptedException {

		int[] testarray = new int[200];
		for(int i=0;i<200;++i){
			//testarray[i] = 1;
		}
		//SizeOf.skipStaticField(true);
		//SizeOf.setMinSizeToLog(10);
		//System.out.println("1 " + SizeOf.deepSizeOf(testarray));

		System.out.println("mode = " + parallelMode + ", Thread  = " + THREAD + ", varSize = " + varSize + ", dl = " + depthLevel);
		// キーのみの連言を作る
		Conjunction c = new Conjunction(itemtable.getKey(), occOnlyKey);

		// キーの閉包をとる
		Conjunction c1 = closureRRppc(c,0);
		// メインループ開始
		if(parallelMode == 0){//オリジナル逐次
			this.output(c);
			System.out.println("# non-parallel");
			long StartTime = System.currentTimeMillis();
			System.out.println("init pattern :"+c1.toString());
			mainLoop(c1,0);
			System.out.println("time = " + (System.currentTimeMillis() - StartTime));
		}
		else if(parallelMode == 1 || parallelMode == 3){//提案手法
			executor = Executors.newFixedThreadPool(THREAD);
			this.output(c);
			System.out.println("@parallel");
			try {
				long StartTime = System.currentTimeMillis();
				mainLoopNode(c1,0);
				System.out.println("time = " + (System.currentTimeMillis() - StartTime));
			//} catch (InterruptedException e) {
			//	e.printStackTrace();
			} finally {
				executor.shutdown();
			}
			for (int i = 0; i < THREAD; ++i) {
				int size = workers.get(i).getOutput().size();
				for(int j=0;j<size;++j){
					output(workers.get(i).getOutput().get(j));
				}
			}
			System.out.println(" outputs.size = " + outputs.size());
			//System.out.println(" nodeSize = " + nodeSize);
			//System.out.println(" mainloop end.");
			float avr = 0;
			for (int i = 0; i < THREAD; ++i) {
				avr += workers.get(i).getWorkTime();
			}
			avr /= THREAD;
			float bunsan = 0;
			for (int i = 0; i < THREAD; ++i) {
				bunsan += (workers.get(i).getWorkTime() - avr)*(workers.get(i).getWorkTime() - avr);
			}
			bunsan /= THREAD;
			String sTime = ("avr = " + avr + "; s.d. = " + Math.sqrt(bunsan));
			avr = 0;bunsan = 0;
			for (int i = 0; i < THREAD; ++i) {
				avr += ppcTime[i];
			}
			avr /= THREAD;
			for (int i = 0; i < THREAD; ++i) {
				bunsan += (ppcTime[i] - avr)*(ppcTime[i] - avr);
			}
			bunsan /= THREAD;
			String sPatt = ("avr = " + avr + "; s.d. = " + Math.sqrt(bunsan));
			System.out.println(" time = (" + sTime + ") \n pattern = (" + sPatt + ")");
		}
		else if(parallelMode == 2){//Krajca手法
			executor = Executors.newFixedThreadPool(THREAD);
			this.output(c);
			System.out.println("@krajca parallel");
			long seqTime = 0, paraTime = 0;
			try {
				long StartTime = System.currentTimeMillis();
				mainLoopKrajca(c1, 0 ,0);
				seqTime = (System.currentTimeMillis() - StartTime);
				System.out.println("seq time = " + seqTime);
				for (int i = 0; i < THREAD; ++i) {
					krajcaWorkers.add(new krajcaWorker(i));
				}
				try{
					
					StartTime = System.currentTimeMillis();
					executor.invokeAll(krajcaWorkers);
					paraTime = (System.currentTimeMillis() - StartTime);
					System.out.println(" parallel time = " + paraTime);
					
				}catch(InterruptedException e){}
			//} catch (InterruptedException e) {
			//	e.printStackTrace();
			} finally {
				executor.shutdown();
			}
			System.out.println("time(seq+para) = " + (seqTime + paraTime));
			for(int i=0;i<THREAD;++i){
				//System.out.println(" proc"+i+" time = " + krajcaWorkers.get(i).getWorkTime() + " ; |coversAtom| = " + ppcTime[i]);
			}
			System.out.println(" krajcaWorkers.size() = " + krajcaWorkers.size());
			for (int i = 0; i < krajcaWorkers.size(); ++i) {
				int size = krajcaWorkers.get(i).getOutput().size();

				for(int j=0;j<size;++j){
					output(krajcaWorkers.get(i).getOutput().get(j));
				}
			}
			System.out.println(" outputs.size = " + outputs.size());
			//System.out.println(" nodeSize = " + nodeSize);
			//System.out.println(" mainloop end.");
			float avr = 0;
			for (int i = 0; i < THREAD; ++i) {
				avr += krajcaWorkers.get(i).getWorkTime();
			}
			avr /= THREAD;
			float bunsan = 0;
			for (int i = 0; i < THREAD; ++i) {
				bunsan += (krajcaWorkers.get(i).getWorkTime() - avr)*(krajcaWorkers.get(i).getWorkTime() - avr);
			}
			bunsan /= THREAD;
			String sTime = ("avr = " + avr + "; s.d. = " + Math.sqrt(bunsan));
			avr = 0;bunsan = 0;
			for (int i = 0; i < THREAD; ++i) {
				avr += ppcTime[i];
			}
			avr /= THREAD;
			for (int i = 0; i < THREAD; ++i) {
				bunsan += (ppcTime[i] - avr)*(ppcTime[i] - avr);
			}
			bunsan /= THREAD;
			String sPatt = ("avr = " + avr + "; s.d. = " + Math.sqrt(bunsan));
			System.out.println(" time = (" + sTime + ") \n pattern = (" + sPatt + ")");
		}

		return 0;
	}
	//=========================================================================
	// ◆ メインループ
	//=========================================================================
//node法
	private void mainLoopNode(Conjunction c,int last) {
		paraForParameter = THREAD;
		if(parallelMode == 1){tasks.offer(new Task(c, last, 1));}
		else{
			for (int i = 0; i < paraForParameter; ++i) {
				tasks.offer(new Task(c, last, i));
			}
		}
		for (int i = 0; i < THREAD; ++i) {
			workers.add(new Worker(i));
		}
		try{
			executor.invokeAll(workers);
		}
		catch(InterruptedException e){};
		
		for(int i=0;i<THREAD;++i){
			procTime[i] = (int)workers.get(i).getWorkTime();
		}
	}
		
	
	final private boolean ifLoop(){
		for (int i = 0; i < THREAD; ++i) {
			if(!workers.get(i).ifIdle()){return true;}
		}
		if(!tasks.isEmpty()){return true;}
		return false;
	}
	
	int[] procTime = new int[32];//プロセッサごとの時間を記録
	int[] ppcTime = new int[32];//プロセッサごとの閉包処理時間を記録
	
	public class Worker implements Callable<Integer>{//マイニング実行ワーカー
		int testCount = 0;
		int workerId;
		int leftId;
		int rightId;
		
		long idleTime = 0;
		public long getIdleTime(){return idleTime;}
		long workTime = 0;//実働時間
		long allTime = 0;//全体時間
		long maxTaskTime = 0;
		long ppcCount = 0;
		
		private ArrayList<Conjunction>	outputs = new ArrayList<Conjunction>();
		public ArrayList<Conjunction> getOutput(){return outputs;}

		boolean idleFlag;//=false;
		boolean loopFlag;//=false;
		public int f_count = 0;
		public int s_count = 0;
		final public boolean ifIdle(){return idleFlag;}
		public Worker(int id){
			this.workerId=id;
			this.leftId = (THREAD+workerId-1) % THREAD;
			this.rightId = (workerId+1) % THREAD;
		}
		public long getAllTime(){
			return allTime;
		}
		public long getWorkTime(){
			return workTime;
		}
		public Integer call() {
			if(parallelMode == 1){
				callNode();
			}
			else{
				callParaFor();
			}
			return 0;
		}
		private void callNode(){
			long procStartTime = System.currentTimeMillis();

			while(true){//終了条件 ifLoop(): workerがidleかつtasks.empty
				Task task = tasks.poll();
				if (task == null){
					idleFlag = true;//++f_count;
					if(ifLoop() == false){break;}
					continue;
				}
				//++s_count;
				idleFlag = false;

				long workStartTime = System.currentTimeMillis();
				Conjunction c = task.getConj();
				int		itemSize	= itemtable.getItemSize(); //アイテムのサイズ
				Var[]	vars		= c.getVars(); //cに含まれる変数
				int start = task.getStart()+1;
				for(int i=start;i<itemSize;i++) {
					Atom p = itemtable.getAtom(i); //次に追加するリテラル
					if(c.hasAtom(p)) {  continue; }
					if(! biasCheck(p, vars)) { continue; }
					if(! biasCheck2(p, vars)) { continue; }
					if(! modeCheck(p,vars)){ continue; }
					if(! varTypeCheck(c, p)){ continue; }
					ArrayList<String[]> occur  = database.coversAtomAL(p, c.getOccur());
					int keyOcc = occur.size();
					if(keyOcc >= minFreq){
						Conjunction c1	= new Conjunction(c,p,occur);//ｃ＋ｐの連言生成
						Conjunction c2 = closureRRppc(c1, i);
						if(c2 != null) {
							this.outputs.add(c2);
							Task T = new Task(c2, i, 1);
							tasks.offer(T);//キューに積む
						}
					}
				}
				long temp_workTime = (System.currentTimeMillis() - workStartTime);
				if(maxTaskTime < temp_workTime){maxTaskTime = temp_workTime;}
				workTime += temp_workTime;
			}
			allTime = (int)(System.currentTimeMillis() - procStartTime);
			//String resultTime = ("time = " + (procTime[workerId]-idleTime) + "(" + procTime[workerId] + "/" + idleTime + ")");
			System.out.println(this.workerId + " : (work/all)=(" + workTime + "ms/" + allTime + "ms) : size = " + this.outputs.size() + ", maxTime = " + maxTaskTime);
		}
		public void callParaFor() {
			long procStartTime = System.currentTimeMillis();

			while(true){//終了条件 ifLoop(): workerがidleかつtasks.empty
				Task task = tasks.poll();
				if (task == null){
					idleFlag = true;//++f_count;
					if(ifLoop() == false){break;}
					continue;
				}
				//++s_count;
				idleFlag = false;
				long workStartTime = System.currentTimeMillis();
				Conjunction c = task.getConj();
				int		itemSize	= itemtable.getItemSize(); //アイテムのサイズ
				Var[]	vars		= c.getVars(); //cに含まれる変数
				int start = task.getStart()+1 + task.getEven();
				int pace = paraForParameter;
				for(int i=start;i<itemSize;i+=pace) {
					//if(i%THREAD != task.getEven()){continue;}
					Atom p = itemtable.getAtom(i); //次に追加するリテラル
					if(c.hasAtom(p)) {  continue; }
					if(! biasCheck(p, vars)) { continue; }
					if(! biasCheck2(p, vars)) { continue; }
					if(! modeCheck(p,vars)){ continue; }
					if(! varTypeCheck(c, p)){ continue; }
						
					ArrayList<String[]> occur  = database.coversAtomAL(p, c.getOccur());
					//ppcTime[workerId]++;
					int keyOcc = occur.size();
					if(keyOcc >= minFreq){
						Conjunction c1	= new Conjunction(c,p,occur);//ｃ＋ｐの連言生成
						Conjunction c2 = closureRRppc(c1, i);
						if(c2 != null) {
							this.outputs.add(c2);
							for(int j=0;j<paraForParameter;++j){
								Task T = new Task(c2, i, j);
								while (!tasks.offer(T)) {};//キューに積む
							}
						}
					}
				}
				long temp_workTime = (System.currentTimeMillis() - workStartTime);
				if(maxTaskTime < temp_workTime){maxTaskTime = temp_workTime;}
				workTime += temp_workTime;
			}
			allTime = (int)(System.currentTimeMillis() - procStartTime);
			//String resultTime = ("time = " + (procTime[workerId]-idleTime) + "(" + procTime[workerId] + "/" + idleTime + ")");
			System.out.println(this.workerId + " : (work/all)=(" + workTime + "ms/" + allTime + "ms) : size = " + this.outputs.size() + ", maxTime = " + maxTaskTime);
		}
	}
	
	
/*--------------------------------------------------------------------------------------------------------------*/
	
/*--------------------------------------------------------------------------------------------------------------*/
	private void mainLoopKrajca(Conjunction c,int last, int level) throws InterruptedException{
		if(level == depthLevel){
			//krajcaWorker worker = new krajcaWorker(c, last);
			//krajcaWorkers.add(worker);
			Task T = new Task(c, last, -1);
			while (!tasks.offer(T)) {};
		}
		else{
			int		itemSize	= itemtable.getItemSize(); //アイテムのサイズ
			Var[]	vars		= c.getVars(); //cに含まれる変数
			
			for(int i=last+1;i<itemSize;i++) {
				// インデックス → アトム化
				Atom p = itemtable.getAtom(i); //次に追加するリテラル	
				/* pが追加できるか判定 */
				// 既に連言に含まれているかチェック
				if(c.hasAtom(p)) {  continue; }
				// バイアスチェック
				// 追加するアトム中の変数がキーと関連しているか？
				if(! biasCheck(p, vars)) { continue; }
				// 変数が連続しているか
				if(! biasCheck2(p, vars)) { continue; }
				// モードが一致しているか
				if(! modeCheck(p,vars)){ continue; }
				//型検査
				if(! varTypeCheck(c, p)){ continue; }
				/* ここまで来て追加できることが判明 */
				ArrayList<String[]> occur  = database.coversAtomAL(p, c.getOccur());
				int keyOcc = occur.size();
				if(keyOcc >= this.minFreq){
					Conjunction c1 = new Conjunction(c,p,occur);//ｃ＋ｐの連言生成
					Conjunction c2 = closureRRppc(c1, i);
					//ここまでで飽和　次からキー飽和
					if(c2 != null) {
						output(c2);
						mainLoopKrajca(c2, i,level+1);//再帰
					}
				}
			}
		}
		//if(level != 0){return;}
		
	}
	public class krajcaWorker implements Callable<Integer>{//クライカ版マイニング実行ワーカー

		int workerId = 0;
		int taskCount = 0;
		long workTime = 0;
		long maxTime = 0;
		private ArrayList<Conjunction>	outputs = new ArrayList<Conjunction>();
		public ArrayList<Conjunction> getOutput(){return outputs;}
		
		public krajcaWorker(int i){
			workerId = i;
		}
		public long getWorkTime(){
			return workTime;
		}
		public Integer call() {
			long StartTime = System.nanoTime();
			while(true){
				Task task = tasks.poll();
				if (task == null){break;}
				++taskCount;
				long StartTaskTime = System.nanoTime();
				this.mainloop(task.getConj(),task.getStart());
				long temp = (long)(0.000001 * (System.nanoTime() - StartTaskTime));
				if(maxTime < temp){maxTime = temp;}
			}
			workTime = (int)(0.000001 * (System.nanoTime() - StartTime));
			System.out.println(workerId + " time = " + workTime + ", taskCount : " + taskCount + ", maxTime = " + maxTime);
			return 1;
		}
		public void mainloop(Conjunction c, int start) {
			int		itemSize	= itemtable.getItemSize(); //アイテムのサイズ
			Var[]	vars		= c.getVars(); //cに含まれる変数
			for(int i=start+1;i<itemSize;i++) {
				Atom p = itemtable.getAtom(i); //次に追加するリテラル
				
				if(c.hasAtom(p)) {  continue; }
				if(! biasCheck(p, vars)) { continue; }
				if(! biasCheck2(p, vars)) { continue; }
				if(! modeCheck(p,vars)){ continue; }
				if(! varTypeCheck(c, p)){ continue; }
				ArrayList<String[]> occur  = database.coversAtomAL(p, c.getOccur());
				ppcTime[workerId]++;
				int keyOcc = occur.size();
				if(keyOcc >= minFreq){
					Conjunction c1	= new Conjunction(c,p,occur);//ｃ＋ｐの連言生成
					Conjunction c2 = closureRRppc(c1, i);
					if(c2 != null) {
						this.outputs.add(c2);
						mainloop(c2, i);
					}
				}
			}
		}
	}
/*--------------------------------------------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------------------------------------------*/
	/**
	 * @param c  現在の連言
	 * @param last	最後のリテラルのID
	 * @param node 次のノード
	 */
	private void mainLoop(Conjunction c,int last){
		
		int		itemSize	= itemtable.getItemSize(); //アイテムのサイズ
		Var[]	vars		= c.getVars(); //cに含まれる変数
		
		for(int i=last+1;i<itemSize;i++) {
			// インデックス → アトム化
			Atom p = itemtable.getAtom(i); //次に追加するリテラル
			//System.out.println("now adding "+i+" th atom: "+ p.toString());
			/* pが追加できるか判定 */
			// 既に連言に含まれているかチェック
			if(c.hasAtom(p)) {  continue; }
			// バイアスチェック
			// 追加するアトム中の変数がキーと関連しているか？
			if(! biasCheck(p, vars)) { continue; }
			// 変数が連続しているか
			if(! biasCheck2(p, vars)) { continue; }
			// モードが一致しているか
			if(! modeCheck(p,vars)){ continue; }
			//型検査
			if(! varTypeCheck(c, p)){ continue; }
			/* ここまで来て追加できることが判明 */
			//System.out.println("now checking done for "+c.toString()+" adding "+p.toString());
			ArrayList<String[]> occur  = database.coversAtomAL(p, c.getOccur());
			//System.out.println("size of c.occur  "+c.getOccur().size()+" p added.occur"+occur.size());
			int keyOcc = occur.size();
			if(keyOcc >= this.minFreq){
				Conjunction c1	= new Conjunction(c,p,occur);//ｃ＋ｐの連言生成
				Conjunction c2 = this.closureRRppc(c1, i);
				//ここまでで飽和　次からキー飽和
				if(c2 != null) {
					output(c2);
					mainLoop(c2, i);//再帰
				}
			}
		}
	}

	//=========================================================================
	// ◆ GetterおよびSetterのメソッド
	//=========================================================================
	/**
	 * 出力解を取得します
	 */
	public ArrayList<Conjunction> getOutputs() { return outputs; }

	/**
	 * 最小頻度閾値を指定します
	 *
	 * @param percent	最小頻度閾値の%数値
	 */
	public void setMinFreq(int minFreq) {
		this.minFreq = minFreq;

		//System.out.println("最小頻度閾値を"+this.minFreq+"に設定");
	}
	//=========================================================================
	// ◆ リテラルの判定
	//=========================================================================

	/**
	 * アトムが連言に対してバイアス条件を満たすかを判定します
	 *
	 * ここでは，「キーと関連している」かのチェックを行います．
	 *
	 * @param atom		判定されるアトム
	 * @param vars		連言に出現する変数集合
	 * @return			バイアス条件を満たす場合 true
	 */
	public boolean biasCheck(Atom atom,Var[] vars) {

		Var[] vs = atom.getVars(); //判定するリテラルの変数

		for(int i=0;i<vs.length;i++) {
			for(int j=0;j<vars.length;j++) {

				// 共有している変数が見つかった。１つでもあればOK
				if(vs[i] == vars[j]) { return true; }
			}
		}

		return false;//１つも見つからなかった。
	}

	/**
	 * アトムが連言に対してバイアス条件を満たすかを判定します
	 *
	 * ここでは，変数が昇順で出現しているかをチェックします．
	 *
	 * @param atom		判定されるアトム
	 * @param vars		連言に出現する変数集合
	 * @return			バイアス条件を満たす場合 true
	 */
	public boolean biasCheck2(Atom atom,Var[] vars) {

		int maxInd = vars[vars.length-1].getIndex();//最大の変数のインデックス

		Var[] vs = atom.getnosortVars();//判定するリテラルの変数

		for(int i=0;i<vs.length;i++) {

			int index = vs[i].getIndex();//確認する変数のインデックス

			// 変数が間を開けて出現している
			if(index > maxInd+1) {
				return false;//昇順で連結でないものがあった
			}
			// 変数が1つ拡大
			if(index == maxInd+1) {
				maxInd = index;
			}
		}

		return true;//全て昇順で連結
	}

	/**
	 * アトムpのモードが一致するか判定します.
	 * @param p 判定するリテラル
	 * @param vars 変数の集合
	 * @return モードが一致するかの判定
	 */
	public boolean modeCheck(Atom p, Var[] vars){
		Term[] args = p.getArgs();//ｐの変数

		for(int i = 0; i < args.length; i++){

			if(args[i] instanceof Var){

				// i番目の引数の変数がvarsに初めて現れる場合(モード情報"-")
				if(isNewvar((Var)args[i], vars)){

					// i番目の引数のモード情報と一致しない
					if(modedef.getmoddef(p)[i].equals("+")) return false;

					// i番目の引数のモード情報と一致する
					if(modedef.getmoddef(p)[i].equals("-")) continue;
				}
			}
		}
		return true;
	}

	/**
	 * 変数の集合の中にある変数を要素に持つかどうかを判定します
	 * @param var  判定する変数
	 * @param vars 変数の集合
	 * @return	既にある false なかった true
	 */
	public boolean isNewvar(Var var, Var[] vars){
		for(int i = 0; i < vars.length; i++){
			if(vars[i].getIndex() == var.getIndex()) return false;
		}
		return true;
	}


	/**
	 * アトムpの変数の型がそれぞれの引数の型と一致するか判定します.
	 *
	 * @param atom	判定されるアトム
	 * @param vars  連言に出現する変数集合
	 * @return		引数の型が一致すれば true
	 */
	public boolean varTypeCheck(Conjunction c, Atom atom){
		// atomの引数を取得
		//Term[] args = atom.getArgs();
		// atomの引数の型を定義した配列
		int[] atomType = atom.getArgsType();
		int[] atomPattern = atom.getArgsPattern();
		int cMax = c.getMaxVarInd();
		Type[] typeArray = atom.getArgsTypeArray();
		for(int i = 0,n = atomPattern.length; i < n; i++){
			//System.out.println("アトム"+atom+"の第"+i+"引数"+args[i] + ","+ (Var)args[i]);
			//if(c.getTypeInfo(atomPattern[i]) == (999)){
			if(cMax < atomPattern[i]){
				continue;
			}
			//アトムatomのi番目の引数の変数の型がcのi番目の引数の定義された型と一致しない場合
			if(! (atomType[i] == c.getTypeInfo(atomPattern[i]))){
				//return false;
				
				Type argtype = typeArray[i];
				if(!argtype.hasChild( c.getTypeInfo( atomPattern[i] ) )) return false;//一致しなかった
				else{
					continue;
				}
				
			}

			// atomの第i引数の型がanyであったら何もしない
			if(atomType[i] == (999)){
				continue;
			}
		}
		return true;

	}

	/** キー出現集合のサイズを返す(ただし，キー変数は1個の場合だけ)
	 *
	 * @param occur		全変数の出現集合
	 * @return			キーに出現する変数のみの出現集合のサイズ
	 *
	 */
	public int countKeyOcc(String[][] occur) {
		Set<String> hs = new HashSet<String>();

		// キーに含まれる変数を取得(key変数が複数個の場合も考慮)
		Var[] keyVars	= itemtable.getKey().getVars();

		// 出現集合の各タプルo に対しループ
		// for(Const[] o : occur) {
		for(String[] o : occur) {
			// キー制限出現タプルs　（keyVarsの属性を持つ)を生成
			String[] s = new String[keyVars.length];
			for(Var v : keyVars) {
				int i = v.getIndex();
				s[i] = o[i];
			}

			hs.add(s[0]); // ここではキー変数は1個の場合だけを考えている

		}

		int hssize = hs.size();
		//System.out.println("重複除去したキー出現集合HashSetのサイズ="+hssize);

		// 集合から配列への変換

		return hssize;

	}


	//=========================================================================
	// ◆ 飽和
	//=========================================================================

	/**
	 * 領域制限閉包を計算します
	 *
	 * @param c		元となる連言
	 * @param last	prefix保存の判定基準となるアトムのインデックス
	 * @return		Cに対する領域制限閉包の結果
	 */
	public Conjunction closureRRppc(Conjunction c,int last) {
		//System.out.println("Start[Closure]   : "+c);

		ArrayList<String[]> occur = c.getOccurString();//ｃのサポート
		Term[]		range		= c.getRange();//ｃに含まれる項
		Atom[]		newAtoms	= c.getAtoms();//ｃに含まれるリテラル

		int		itemSize	= itemtable.getItemSize();//リテラルの総数
		
		//System.out.println(c+"の出現集合の数"+occur.length+"個");

		for(int i=0;i<itemSize;i++) {//順に含むことができるか全て確認
			// インデックス → アトム化
			Atom p = itemtable.getAtom(i);

			// 既に連言cにpが含まれているかチェック
			if(c.hasAtom(p)) { continue; }

			// 領域制限を満たさない
			if(! isRR(p,range)) { continue; }

			// 型検査 (0426 コメントアウトしても正常に動作)
			if(! varTypeCheck(c, p)) {continue;}

			// 出現集合のチェック. 新たに変数が出現することはないので単純に比較可能
			boolean inClo= database.isInClo(p, occur); //アトムpは閉包に入るとき, inClo=true


			if(inClo) { // pは出現集合を変えない
				// 先行アトムだった場合
				if(i < last) {
					return null;
				}

				// 後続アトムだった場合
				else {
					newAtoms = Arrays.copyOf(newAtoms, newAtoms.length+1);
					newAtoms[newAtoms.length-1] = p;
				}
			}
		}

		Conjunction c1 = new Conjunction(newAtoms,occur);		
		c1.setTypeInfo(c.getTypeInfo());
		c1.setMaxVar(c.getMaxVarInd());
		//System.out.println("\nc is  " + c + ",  " + c.getTypeInfo().length);
		//System.out.println("c1 is " + c1 + ", " + c1.getTypeInfo().length);
		return c1;
	}


	/**
	 * アトムが連言に対して領域制限を満たすかを判定します．
	 *
	 * @param atom		判定されるアトム
	 * @param terms		連言に出現する変数・定数の集合 = 領域
	 * @return			領域制限を満たす場合 true
	 */
	public boolean isRR(Atom atom,Term[] terms) {
		boolean result = true;

		Term[] ts = atom.getArgs();

		for(int i=0,m = ts.length;i<m;i++) {
			boolean bool = false;

			for(int j=0,n=terms.length;j<n;j++) {
				// 含まれていたら true
				if(ts[i]==terms[j]) { bool = true; break; }
			}
			// 一つでも領域制限を満たさない項があれば false
			if(! bool) { result = false; break; }
		}

		return result;
	}

	//=========================================================================
	// ◆ キー飽和
	//=========================================================================
	/**
	 * 連言がキー制限飽和連言かを判定します
	 *
	 * @param c		判定を行う連言
	 * @return		Cがキー制限飽和連言であれば true
	 */
	public boolean isClosureRRkey(Conjunction c) {
		// System.out.println("Start[isClosure] : "+c);

		ArrayList<String[]>	occur		= c.getOccurString();
		// Const[][]	keyOcc0		= occToKeyOcc(occur);
		Term[]		range		= c.getRange();

		int		itemSize	= itemtable.getItemSize();
		for(int i=0;i<itemSize;i++) {
			// インデックス → アトム化
			Atom p = itemtable.getAtom(i);

			// 既に連言に含まれているかチェック
			if(c.hasAtom(p)) { continue; }
			// 領域制限閉包を満たさない
			if(! isRR(p,range)) { continue; }
			// 型検査
			if(! varTypeCheck(c,p)){continue;}

			// long timecount2 = System.currentTimeMillis();
			// dataoccはアトムpを連言cに追加したときの出現集合

			if(database.isInKeyClo(p, occur)){return false; }// pはキー閉包に入る

			//	timecount2 = System.currentTimeMillis() - timecount2;
		}

		return true;
	}

	//=========================================================================
	// ◆ 出力
	//=========================================================================
	/**
	 * 解の出力を行います．
	 * GUI利用
	 * @param c 連言
	 * @param parentNode 親ノード
	 * @return
	 */
	private void output(Conjunction c) {
		//System.out.println("pattern found : "+c.toString());
		outputs.add(c); //解にcを追加

	}
	//ブロッキングキューに積むタスククラス
	static class Task {
		private Conjunction C;
		private int start;
		private int even;//1の時は奇数、0の時は偶数のみ処理

		Task(Conjunction c, int s, int e) {
			this.C = c;
			this.start = s;
			this.even = e;
		}
		public void set(Conjunction c, int s) {
			this.C = c;
			this.start = s;
		}
		public Conjunction getConj(){return C;}
		public int getStart(){return start;}
		public int getEven(){return even;}
	}
	
	//ICKM2015
	private boolean varTradeCheck(Conjunction c, Atom p){
		int varNum = c.getMaxVarInd() + 1;
		occDataArray[] occArray = new occDataArray[varNum];
		for(int i=0;i<varNum;++i){occArray[i] = new occDataArray();}
		if(varNum < 3){return true;}
		Atom[] atoms = c.getAtoms();
		int size = atoms.length;
		for(int i=0;i<size;i++) {
			Atom a = atoms[i];
			int[] pattern = a.getArgsPattern();
			int id = a.getNameID();
			int pat_len = pattern.length;
			if(pat_len != 2){continue;}//2引数の述語のみ判定
			for(int j=0;j<pat_len;j++) {
				if(pattern[j] == 0){continue;}//'A'ならスルー
				occData data = new occData(id,j,pattern[1-j]);
				occArray[pattern[j]].add(data);
				/*
				if(varArray[pattern[j]] == 0){//変数が出現する述語のnameIDを格納
					varArray[pattern[j]] = id;//'pattern[j]'が出現する述語のIDは id
					positionArray[pattern[j]] = j;//引数の位置
					otherArray[pattern[j]] = pattern[1-j];//もう片方の引数(jは0か1である)
					break;
				}
				*/
			}
		}
		
		ArrayList<Integer> tradeList = new ArrayList<Integer>();
		int[] checkList = new int[varNum];//0:未チェック 1:チェック済み
		for(int i=1;i<varNum;i++) {
			if(checkList[i] == 1){continue;}//チェック済みなのでとばす
			tradeList.clear();
			checkList[i] = 1;//チェック
			tradeList.add(i);
			for(int j=i+1;j<varNum;j++) {
				if(occArray[i].equal(occArray[j])){//occArrayの中身が一致
					tradeList.add(j);
					checkList[j] = 1;//チェック
				}
			}
			if(tradeList.size() < 2){continue;}
			else{//とりあえず2変数に対応
				int listSize = tradeList.size();
				for(int m=0;m<listSize-1;++m){
					int a = tradeList.get(m);
					for(int n=m+1;n<listSize;++n){
						int b = tradeList.get(n);
						StringBuilder sb = new StringBuilder();
						int argsP[] = p.getArgsPattern();
						sb.append("(");
						sb.append(new Var(argsP[0]));
						for(int j=1,pLen=argsP.length;j<pLen;j++) {
							sb.append(",");
							int v = argsP[j];
							if(v == a){v = b;}
							else if(v == b){v = a;}
							sb.append(new Var(v));
						}
						sb.append(")");
						int temp = itemtable.getAtomIDfromHM(p.getNameID(), sb.toString());
						if(temp != -1 && p.getID() > temp){
							if(!c.hasAtom(temp)){
								System.out.println("! c is " + c);
								System.out.println("  p is " + p+p.getID() + ". a="+a+",b="+b + " -> " + p.getName() + sb.toString() + ":"+temp);
								return false;						
							}
							else{
								//System.out.println("!! c is " + c);
								//System.out.println("!! p is " + p + ". a="+a+",b="+b);
							}
						}
					}
				}
			}
		}
		return true;
	}
	
	class occDataArray{//occDataを格納するクラス　変数の数だけ作る
		private ArrayList<occData> array = new ArrayList<occData>();
		public ArrayList<occData> getArray(){return array;}
		occDataArray(){
			
			
		}
		public void add(occData data){array.add(data);}
		public boolean equal(occDataArray dataArray){
			int size = array.size();
			ArrayList<occData> array2 = dataArray.getArray();
			if(size != array2.size()){return false;}
			for(int i=0;i<size;++i){
				occData data1 = array.get(i);
				for(int j=0;j<size;++j){
					occData data2 = array2.get(j);
					if(data1.equals(data2)){continue;}//hitしたので次へ
					if(j+1 == size){return false;}//hitしなかったdataが一つある -> 不一致
				}
			}
			return true;
		}
	}
	class occData{
		private int nameID;//出現した述語のnameID
		private int pos;//出現した述語での引数位置
		private int other;//出現した述語でのもう片方の引数
		occData(int id, int p, int o){
			nameID = id;
			pos = p;
			other = o;
		}
		public int getID(){return nameID;}
		public int getPos(){return pos;}
		public int getOther(){return other;}
		public boolean equals(occData data){
			if(data.getID() == nameID && data.getPos() == pos && data.getOther() == other){return true;}
			return false;
		}
	}
}

