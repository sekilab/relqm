package merge;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 * @author hadoop
 *	データ書き込み用
 */
public class DataWrite {

	private String fileName;
	
	private String pass = "";
	
	/**
	 * 呼び出したとき白紙のファイルを生成します。
	 * @param fileName ファイル名
	 */
	public DataWrite(String fileName){
		this.fileName = fileName;
	
		String[] passData = this.fileName.split("/");
		
		for(int i = 0;i<passData.length-1;i++){
			this.pass +=passData[i]+"/"; 
		}
		
		File dir = new File(this.pass);
		dir.mkdirs();
		
		try {		
			
			FileWriter fw = new FileWriter(this.fileName);
			fw.write("");
			fw.close();
	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
/**
 * ファイルに書き込みをします。
 * @param data 書き込むデータ
 */
	public void write(String data){
		
		try {		
			
			FileWriter fw = new FileWriter(this.fileName,true);
			fw.write(data);
			fw.close();
	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
