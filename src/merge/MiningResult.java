package merge;

import java.util.ArrayList;



public class MiningResult {
	
	private  ArrayList<Conjunction> conjunctions = new ArrayList<Conjunction>();
	private String database;
	private int varSize = -1;	
	private int index; //サポート数の比較の時に使用
	/**
	 * 最初に読み込むとき
	 * @param data データベース名 (conjunction=連言 ,keyOcc=キーの出現集合の数 ,occ=出現集合の数)...
	 */
	MiningResult(String data){
		
		String[] miningData = data.split(":");
        
        String[] databaseData = miningData[0].split("/");
        
        this.database = databaseData[0];//データベース名(絶対パス)
        this.varSize = Integer.parseInt(databaseData[1]);//変数の数
        
        //System.out.println(this.database);
        
        String tmp = miningData[1].replace("	", "");//tabを取り除く
        //System.out.println(tmp);
        
        String[] conjunctionData = tmp.split(";");//連言ごとに切り分け
        //System.out.println(conjunctionData.length);
        
        for(int i = 0;i<conjunctionData.length;i++){
        	
        	//System.out.println(conjunctionData[i]);
        	this.conjunctions.add(new Conjunction(conjunctionData[i]));//連言の登録
        	
        }
                
	}
	/**
	 * 途中で作るとき
	 */
	MiningResult(){}
	
	/**
	 * 連言cが含まれているか
	 * 含まれていたらtrue
	 * @param c 連言
	 * @return true:既に登録済みの連言	false:未登録の連言
	 */
	public boolean containConjunction(Conjunction c){
		
		int firstIndex = 0;//最初
		int lastIndex = this.conjunctions.size();//最後
		int midleIndex;//真ん中
		
		
		while((lastIndex - firstIndex) > 5){//連言の数が5以下になるまで
			
			midleIndex = firstIndex +((lastIndex-firstIndex)/2);
			
			int tmp = this.compTo(this.conjunctions.get(midleIndex).getAtomsID(), c.getAtomsID());//大小比較
			
			//System.out.println(tmp);
			
			switch(tmp){
			case 0: this.index = midleIndex;return true;//真ん中に一致
			case 1:lastIndex = midleIndex;break;//真ん中より小さい
			case -1:firstIndex = midleIndex;break;//真ん中より大きい
			default: return false;
			}
			
		}
		
		for(int i = firstIndex;i<lastIndex;i++){
			if(this.compTo(this.conjunctions.get(i).getAtomsID(), c.getAtomsID()) == 0){
				this.index = i;
				return true;//一致するものがあった
			}
		}
		
		//含まれいない
		return false;
		
	}
	
	/**
	 * 連言の大小比較を行います
	 * @param atoms1　アトムのIDのリスト1
	 * @param atoms2  アトムのIDのリスト２
	 * @return atom1>atom2:1  atom1=atom2:0  atom1<atom2:-1
	 */
	private int compTo(ArrayList<Integer> atoms1,ArrayList<Integer> atoms2){
	
		int size = 0;
		
		//どちらが長いか
		if(atoms1.size()>atoms2.size()){
			size = atoms2.size();
		}
		else{
			size = atoms1.size();
		}
		
		for(int i = 0;i < size;i++){
			int id1 = atoms1.get(i);
			int id2 = atoms2.get(i);
			
			if(id1>id2){
				return 1;
			}
			if(id1 < id2){
				return -1;
			}
		}
		
		if(atoms1.size() == atoms2.size()){
		return 0;
		}
		else if(atoms1.size() > atoms2.size()){
			return 1;
		}
		return -1;
		
	}
	
	public void addConjunction(Conjunction c){
    	
		for(int i = 0;i<this.conjunctions.size();i++){//インサートソートをしながら追加
		
			if(this.compTo(this.conjunctions.get(i).getAtomsID(), c.getAtomsID())>0){
				this.conjunctions.add(i, c);//連言の登録
				return;
			}
		}
		
		this.conjunctions.add(c);//連言の登録	
    	
	}
	
	public ArrayList<Conjunction> getConjunction() {
		return conjunctions;
	}
	
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}

	public int getVarSize() {
		return this.varSize;
	}
	public void setVarsize(int varSize) {
		this.varSize = varSize;
	}
	public int getIndex(){
		return this.index;
	}
}
