﻿package merge;

/*
 * CからFCを発見する
 */

import java.util.ArrayList;

public class FindFC {
	
	private MiningResult fc = new MiningResult(); //頻出飽和集合
	private MiningResult ic = new MiningResult(); //頻出でない飽和集合
	private int minsup; //最小サポート数
	
	//FC,ICの振り分け
	public void find(MiningResult c){		
		for(int i=0; i<c.getConjunction().size();i++){
			if(c.getConjunction().get(i).getKeyOccur() >= minsup){
				this.fc.addConjunction(c.getConjunction().get(i));
			}else{
				this.ic.addConjunction(c.getConjunction().get(i));
			}
		}
	}
	
	//minsupの設定
	public void setminsup(int ms){
		this.minsup = ms;
	}
	
	public int getminsup(){
		return this.minsup;
	}
	
	//結果獲得
	public ArrayList<Conjunction> getFC(){
		return this.fc.getConjunction();
	}
	
	public ArrayList<Conjunction> getIC(){
		return this.ic.getConjunction();
	}
	
	//MiningResult型でContainConjunctionを利用したいので.
	public MiningResult getFCMR(){
		return  this.fc;
	}
	 
}
