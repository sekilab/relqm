package merge;

import java.util.ArrayList;

public class Conjunction {

	//private ArrayList<String> atoms = new ArrayList<String>();
	private ArrayList<Integer> atomsID = new ArrayList<Integer>(); //アトムのIDで連言を表現
	//private String conjunction;
	private int occur;//出現の数
	private int keyOccur;//キーの出現の数
	
	
	/**
	 * 
	 * @param data conjunction=連言,keyOcc=キーの出現集合の数,occ=出現集合の数
	 */
	Conjunction(String data){

		//System.out.println(data);
		
		String[] conData = data.split(" ,");
		
		for(int i = 0 ;i<conData.length;i++){
			//System.out.println(conData[i]);
			
/*			if(conData[i].startsWith("conjunction=")){
				String c = conData[i].replace("conjunction=[","");
				c = c.replace("]", "");
				
				//System.out.println(c);
				this.setConjunction(c);
				this.setAtoms();
			}
*/
			
			if(conData[i].startsWith("conID=")){
				String c = conData[i].replace("conID=", "");
				
				String[] stringAtomsID = c.split(",");
				
				for(String stringAtomID:stringAtomsID){
					this.atomsID.add(Integer.parseInt(stringAtomID));//IDをセット
				}
				
			}
			
			if(conData[i].startsWith("keyOcc=")){
				int key = Integer.parseInt(conData[i].replaceAll("keyOcc=", ""));
				
				//System.out.println(key);
				this.setKeyOccur(key);//キーの出現の数をセット
			
			}if(conData[i].startsWith("Occ=")){
				int occ = Integer.parseInt(conData[i].replaceAll("Occ=", ""));
				
				//System.out.println(occ);
				this.setOccur(occ);//出現の数をセット
			}
		}
		
	}

	/*	
	private void setAtoms(){
		String[] atom = this.conjunction.split(", ");
		
		for(int i = 0;i<atom.length;i++){
			this.atoms.add(atom[i]);
			//System.out.println(atom[i]);
		}
	}
	*/
	
	public ArrayList<Integer> getAtomsID() {
		return atomsID;
	}
	
	/*
	public ArrayList<String> getAtoms() {
		return atoms;
	}
	*/
/*
	public String getConjunction() {
		return conjunction;
	}
	public void setConjunction(String conjunction) {
		this.conjunction = conjunction;
	}
*/	
	
	public int getOccur() {
		return occur;
	}
	public void setOccur(int occur) {
		this.occur = occur;
	}
	public int getKeyOccur() {
		return keyOccur;
	}
	public void setKeyOccur(int keyOccur) {
		this.keyOccur = keyOccur;
	}
	
}
