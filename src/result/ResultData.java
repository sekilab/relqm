package result;

//import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;

import database.obj.Atom;
import database.obj.Conjunction;

public class ResultData {

	private String conjunction ="";
	private String conjunctionID =""; //連言をアトムのIDで表示
	
	private ArrayList<String[]> occur; //出現集合
	private ArrayList<String> keyOcc = new ArrayList<String>();
	
	private int range;
	private int occSize;
	private int keyOccSize;
	
	public ResultData(Conjunction c){
		/*
		//メモリ使用量
		final DecimalFormat FORMAT_MEMORY	= new DecimalFormat("#,###KB");
		final DecimalFormat FORMAT_RATIO	= new DecimalFormat("##.#");

		long free;
		long total;
		long max;
		long used;
		double ratio;
		
		free		= Runtime.getRuntime().freeMemory()	>> 10;
		total		= Runtime.getRuntime().totalMemory()>> 10;
		max		= Runtime.getRuntime().maxMemory()	>> 10;
		used		= total - free;
		ratio	= (used * 100 / (double)total);
		
		System.out.println("\n開始");
		System.out.println("  Memory: ALL ="+FORMAT_MEMORY.format(total));
		System.out.println("        : USED="+FORMAT_MEMORY.format(used)+"("+FORMAT_RATIO.format(ratio)+"%)");
		System.out.println("        : MAX ="+FORMAT_MEMORY.format(max));
		
		//===========================================================================================//
		//                                                                                           //
		//===========================================================================================//
		 */
		
		//this.setConjunction(c.toString()); //飽和連言
		this.occSize = c.getOccurString().size(); //出現集合の数
		this.range = c.getRange().length; //変数、定数の長さ
		this.occur = c.getOccurString();
		
		LinkedList<Atom> atoms = this.sort(c.getAtoms());
		StringBuilder buf = new StringBuilder();
		buf.setLength(0);
		
		for(Atom atom:atoms){
			if(this.conjunctionID == ""){
				this.conjunctionID += atom.getID();
				buf.append("[");
				buf.append(atom.getName());
				//this.conjunction +="["+atom.getName();
			}
			else{
				this.conjunctionID += ","+atom.getID();
				buf.append(", ");
				buf.append(atom.getName());
				//this.conjunction +=", "+atom.getName();
			}
		}		
		buf.append("]");
		this.conjunction = buf.toString();
		
		ArrayList<String[]> occurStr =c.getOccurString();
		String preKey=  occurStr.get(0)[0];
		this.keyOcc.add(preKey);
		String currentKey;	
		for(int i = 0, s=this.occSize; i<s;i++){
					
			currentKey = occurStr.get(i)[0];

			if(!currentKey.equals(preKey)){// すでに出現していない
				this.keyOcc.add(currentKey);
			}
			
			
		}
		
		/*
		//===========================================================================================//
		//                                                                                           //
		//===========================================================================================//
		free		= Runtime.getRuntime().freeMemory()	>> 10;
		total		= Runtime.getRuntime().totalMemory()>> 10;
		used		= total - free;
		ratio	= (used * 100 / (double)total);
		
		System.out.println("\n　終了");
		System.out.println("        : USED="+FORMAT_MEMORY.format(used)+"("+FORMAT_RATIO.format(ratio)+"%)");

		//===========================================================================================//
		//                                                                                           //
		//===========================================================================================//
		*/
		
		this.setKeyOccSize(this.keyOcc.size());
		
		//System.out.println("conjunctionID="+conjunctionID+" : keyOccSize="+keyOccSize+" : occSize="+occSize);
	}

	/**
	 * アトムをIDでソート(インサートソート)します
	 * @param atoms
	 * @return ソート結果
	 */
	private LinkedList<Atom> sort(Atom[] atoms){
		
		LinkedList<Atom> atomList = new LinkedList<Atom>();
		
		atomList.add(atoms[0]);
		
		for(int i =1;i<atoms.length;i++){
			
			for(int j = 0;j<atomList.size();j++){
				if(atomList.get(j).getID()>atoms[i].getID()){
					atomList.add(j, atoms[i]);
					break;
				}
				if(j+1 == atomList.size()){
					atomList.add(atoms[i]);
					break;
				}
			}
		}
		
		return atomList;
		
	}
	
	//=========================================================================
	// ◆ GetterおよびSetterのメソッド
	//=========================================================================	
	
	/**
	 * 外部では使わない
	 */
	public void setConjunction(String conjunction) {
		this.conjunction = conjunction;
	}
	
	
	/**
	 * 連言を返します
	 * @return
	 */
	public String getConjunction() {
		return conjunction;
	}
	 
	
	/**
	 * 連言をIDで返します
	 * @return
	 */
	public String getConjunctionID() {
		return conjunctionID;
	}

	
	/**
	 * 出現集合を返します
	 * @return
	 */
	public String getOcc(int i){		
		
		String occ = this.occur.get(i)[0];
		
		for(int j = 1;j<this.range;j++){
				occ += " "+this.occur.get(i)[j];
			}
		return occ;
	}
	
	/**
	 * 変数、定数の数を返します
	 * @return
	 */
	public int getRange(){
		return this.range;
	}

	/**
	 * 出現集合の数を返します
	 * @return
	 */
	public int getOccSize(){
		return this.occSize;
	}

	/**
	 * 外部では使わない
	 * @param keyOccSize
	 */
	public void setKeyOccSize(int keyOccSize) {
		this.keyOccSize = keyOccSize;
	}
	
	/**
	 * キーの出現集合の数を返します。
	 * @return
	 */
	public int getKeyOccSize() {
		return keyOccSize;
	}

	/**
	 * キーの出現集合を返します。
	 * @return
	 */
	public ArrayList<String> getKeyOcc() {
		return keyOcc;
	}

	
}
