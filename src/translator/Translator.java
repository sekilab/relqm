package translator;

//DataBaseのコンストラクタで用いる

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ConcurrentHashMap;


public class Translator {
	String file_name = "";
	/** 述語とその引数の情報を格納したリスト*/
	ArrayList<Predicate> predList = new ArrayList<Predicate>();
	
	/** マイニングするパターンを指定する際に使用*/
	public PatternData patternData = new PatternData();
	private String ruleString = "";
	
	String keyPred = "";
	String targetPred = "";
	
	public Translator(String name){
		file_name = name;
	}
	
	public void process(){
		//System.out.println(file_name);
		String patternStr = "";
		
		try{
			File file = new File(file_name);
			FileReader fl = new FileReader(file);
			BufferedReader br = new BufferedReader(fl);
			String str = "";
			while((str = br.readLine())!=null){
				if(str.contains("file")){ //file読み込みでブロックを開始
					String[] attr_types = null;
					String[] line = str.split("=");
					System.out.println(line[1]);
					CSVtable csv = new CSVtable(line[1]);
					str = br.readLine();//'{'
					str = br.readLine().replaceAll("\t", "");//first
					while(!str.contains("}")){
						String[] Data = str.split(",");
						//System.out.println(Data[0]);
						if(Data[0].equals("attribute")){
							attr_types = Data[1].split(":");
						}
						else if(Data[0].equals("/table")){
							String predName = Data[1];
							System.out.println(" predName:" + predName + " : not used");
						}
						else{
							//table:述語名:使用する属性番号:型(DatatypeDef.java参照)
							String predName = Data[1];
							if(Data[0].equals("key")){
								keyPred = predName;
								System.out.println(" key:" + predName);
							}
							else if(Data[0].equals("target")){
								targetPred = predName;
								System.out.println(" target:" + predName);
							}
							else{System.out.println(" name:" + predName);}
							String[] args = Data[2].split(":");
							Integer[] predArgs = new Integer[args.length];
							for(int i=0;i<args.length;++i){
								predArgs[i] = Integer.parseInt(args[i]);
							}
							Predicate pred = new Predicate(predName, predArgs, attr_types);
							pred.read(csv);
							if(Data.length > 3){
								//型情報あり
								pred.setTypeDef(Data[3]);
							}
							predList.add(pred);
						}

						str = br.readLine().replaceAll("\t", "");//次の一行
					} // while(!str.contains("}"))終了
				} //file読み込みブロック終了
				else if(str.contains("pattern")){//パターン形式の読み込み
					str = br.readLine();// 最初の '{' 読み飛ばし 
					str = br.readLine().replaceAll("\t", "");//str に patternを入れる
					while(!str.contains("}")){
						char first = str.charAt(0);
						//System.out.println("char = "+first+" in str "+str);
						if(first=='/'){ // コメント行
							//System.out.println("if true, char is /"+" in str "+str);
							//str = br.readLine().replaceAll("\t", "");//次の一行
							//System.out.println("next line after /-line:"+str);
						}
						else{
							//System.out.println("char not /");
							patternStr = str;
						}
						str = br.readLine().replaceAll("\t", "");//次の一行
					}				
				} // patternブロック終了
				else if(str.contains("rule")){//要素の条件部・結論部指定
					str = br.readLine();//'{'
					str = br.readLine().replaceAll("\t", "");//str <- rule
					while(!str.contains("}")){
						char first = str.charAt(0);
						//System.out.println("char = "+first+" in str "+str);
						if(first=='/'){ // コメント行
							//System.out.println("if true, char is /"+" in str "+str);
							//str = br.readLine().replaceAll("\t", "");//次の一行
							//System.out.println("next line after /-line:"+str);
						}
						else{
							//System.out.println("char not /");
							ruleString = str;
						}
						str = br.readLine().replaceAll("\t", "");//次の一行
					}			
				}
			}
		}catch(Exception e){
            System.out.println("error : " + e);
        }
		//writeResult();
		for(Predicate pred : predList){
			//System.out.println(pred.toString());
		}

		if(!patternStr.equals("")){
			ConcurrentHashMap<String, Integer> constHash = new ConcurrentHashMap<String, Integer>();
			//System.out.println(patternStr);
			patternStr = patternStr.substring(0, patternStr.length()-1);
			String[] atom = patternStr.split("\\),");
			//System.out.println("test " + Arrays.toString(atom));
			for(String str : atom){//"述語名 + ( + 引数"
				String[] strArray = str.split("\\(");
				String name = strArray[0];
				String argStr = strArray[1];
				String[] argsStr = argStr.split(",");//"A,B,C"
				int[] args = new int[argsStr.length];
				for(int i=0;i<args.length;++i){
					String arg = argsStr[i];
					Integer idx = constHash.get(arg);
					if(idx == null){//新規変数
						int size = constHash.size();
						constHash.put(arg, size);
						args[i] = size;
					}
					else{//既存の変数
						args[i] = constHash.get(arg);
					}
				}
				patternData.add(new AtomData(name, args));
			}
			patternData.setMaxIndex(constHash.size());
			System.out.println(patternData);
		}
	}
	public void writeResult(){
		try{
			File file = new File("src/result_trans.txt");
			if(file.exists() && file.isFile() && file.canWrite()){
				PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(file)));
				
				pw.println("key("+keyPred+").\n");
				for(Predicate pred : getPredList()){
					pw.println(pred.toString());
				}
				pw.close();
			}
			else{
				System.out.println("ファイルに書き込めません");
			}
		}
		catch(IOException e){
			System.out.println(e);
		}
	}
	public String getKeyName(){
		return keyPred;
	}
	public String getTargetName(){
		return targetPred;
	}
	public String getRuleString(){
		return ruleString;
	}
	/*-------------------------------------------------------------------------------------*/
	public class PatternData{
		/** AtomDataを格納するArrayList*/
		public ArrayList<AtomData> atomList = new ArrayList<AtomData>();
		
		int maxIndex;//変数の最大インデックス(maxIndex+1だけ変数が存在する)
		
		public void add(AtomData atom){
			atomList.add(atom);
		}
		public void setMaxIndex(int idx){
			maxIndex = idx;
		}
		public int getMaxIndex(){
			return maxIndex;
		}
		public String toString(){
			return atomList.toString();
		}
	}
	public class AtomData{
		String name;//述語名
		int[] arg;//(B,C) -> {1, 2}, Aを0とする
		int argLength = 0;
		public AtomData(String name, int[] arg){
			this.name = name;
			this.arg = arg;
			argLength = arg.length;
		}
		public int[] getArg(){
			return arg;
		}
		public int getLength(){
			return argLength;
		}
		/** 述語名と変数インデックスをカンマ区切りで出力 (ex. atom,0,2)*/
		public String toString(){
			String str = name;
			//str = name + Arrays.toString(arg);
			for(int i=0;i<argLength;++i){
				str += ",";
				str += arg[i];
			}
			return str;
		}
	}
	public ArrayList<Predicate> getPredList(){
		return predList;
	}
	public class Predicate{
		String name = "";
		Integer[] arg;//テーブル中の番号
		Integer[] type;//0:keyID, 1:カテゴリ, -1:数値
		String typeDef = null;//ex. pred(key,int,int) -> a:b:b
		int length;
		public ArrayList<String[]> occur = new ArrayList<String[]>();
		public String[][] array;
		public Predicate(String n, Integer[] a, String[] typeStr){
			name = n;
			arg = a;
			length = arg.length;
			type = new Integer[length];
			for(int i=0;i<length;++i){
				if(typeStr[arg[i]].equals("q")){type[i] = -1;}
				else if(typeStr[arg[i]].equals("n")){type[i] = 1;}
				else{type[i] = 0;}
			}
		}
		public void setTypeDef(String type){
			this.typeDef = type;
		}
		public void read(CSVtable csv){
			ArrayList<String[]> table = csv.table;
			for(String[] tuple : table){
				String[] occ = new String[length];
				for(int i=0;i<length;++i){
					occ[i] = tuple[arg[i]];
				}
				occur.add(occ);
			}
			//occurのソート
			Collections.sort(occur, new StringArrayComparator());
			array = new String[occur.size()][length];
			for(int i=0;i<occur.size();++i){
				String[] tuple = occur.get(i);
				for(int j=0;j<length;++j){
					array[i][j] = tuple[j];
				}
			}
		}
		public String toString(){
			String head = (name + "(");
			String tail = ").\n";
			StringBuilder ret = new StringBuilder();
			for(String[] tuple : occur){
				ret.append(head);
				ret.append(tuple[0]);
				for(int i=1;i<length;++i){
					ret.append(",");
					ret.append(tuple[i]);
				}
				ret.append(tail);
			}
			return ret.toString();
		}
		public String getName(){
			return name;
		}
		public int getLength(){
			return length;
		}
		public String[][] getArray(){
			return array;
		}
		public Integer[] getTypes(){
			return type;
		}
		public String getTypeDef(){
			return typeDef;
		}
	}
	public class StringArrayComparator implements Comparator<String[]> { 
		@Override
		public int compare(String[] p1, String[] p2) 
		{ 
			for(int i=0;i<p1.length;i++) {
				if(!p1[i].equals(p2[i])){return p1[i].compareTo(p2[i]);}
			}
			return 0;
		} 
	}
	public class CSVtable{
		String name = "";
		String[] label = null;
		ArrayList<String[]> table = new ArrayList();
		public CSVtable(String n){
			name = n;
			//System.out.println("CSV file name:  "+name+" now reading ");
			read();
			
		}
		public void read(){
			try{
				File file = new File(name);
				FileReader fl = new FileReader(file);
				BufferedReader br = new BufferedReader(fl);
				String str = "";
				str = br.readLine();
				String[] labels = str.split(",");
				label = labels;
				//int ln=0;
				while((str = br.readLine())!=null){
					//System.out.println("line  "+ln+": "+str.toString());
					int dquote = str.indexOf("\"");
					String[] line = null;
					if(dquote != -1){//contains ' " '
						line = splitCSV(str);
					}
					else{
						line = str.split(",");
					}
					table.add(line);
				}
			}catch(Exception e){
	            System.out.println("error");
	        }
		}
	}
	/**
	* ダブルクォーテーション内のカンマを考慮してsplitする
	*/
	 public String[] splitCSV(String str){
		 String[] line = str.split(",");
		 int count = 0;
		 boolean mode = false;
		 StringBuilder buff = null;
		 for(int i=0;i<line.length;++i){
			 if(mode){
				 buff.append(",");
				 buff.append(line[i]);
			 }
			 if(line[i].startsWith("\"")){
				 mode = true;
				 count = i;
				 buff = new StringBuilder();
				 buff.append(line[i]);
				 
			 }
			 else if(line[i].endsWith("\"")){
				 mode = false;
				 line[count] = buff.toString();
				 for(int j=i+1;j<line.length;++j){
					 line[j-(i-count)] = line[j];
				 }
			 } 
		 }
		return line;
	 }
}
