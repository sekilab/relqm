package io;

import java.io.IOException;
import java.io.Reader;

/**
 * データファイル(疑似SWI-Prologファイル)を読み込むクラスです
 *
 * @author sin8
 */
public class SwiFileReader {

	//=========================================================================
	// ◆ 変数
	//=========================================================================
	/** リーダ */
	private Reader in;
	/** 1文字バックフラグ */
	private boolean unread;
	/** 直前に読み込んだ文字 */
	private int ch;


	//=========================================================================
	// ◆ コンストラクタ
	//=========================================================================
	/**
	 * データファイルの読み込みを行うインスタンスを生成します
	 */
	public SwiFileReader(Reader in) {
		this.in		= in;
	}


	//=========================================================================
	// ◆ GetterおよびSetterのメソッド
	//=========================================================================
	/**
	 * 1文字バックフラグを立てます
	 */
	public void unread() { unread = true; }


	//=========================================================================
	// ◆ その他のメソッド
	//=========================================================================
	/**
	 * 1文字読み込みます
	 */
	public int read() throws IOException {
		if(unread) { unread = false; }
		else { ch = in.read(); }
		return ch;
	}


	/**
	 * 空白文字を読み飛ばします
	 *
	 * @throws Exception
	 */
	public void skipWhiteCharacter() throws Exception {
		if(! unread) { ch = in.read(); }
		while(ch != -1 && Character.isWhitespace((char)ch)) { ch = in.read(); }
		unread();
	}

}
