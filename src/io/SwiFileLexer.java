package io;

import java.io.Reader;
import java.util.ArrayList;
import java.util.Hashtable;

import database.obj.Const;

/**
 * データファイル(疑似SWI-Prologファイル)を構文解析するクラスです
 *
 * @author sin8
 */
public class SwiFileLexer {

	//=========================================================================
	// ◆ 変数
	//=========================================================================
	/** 入力ストリームリーダ */
	private SwiFileReader reader;
	/** 定数値のキャッシュ */
	private Hashtable<String,Const> constTable;

	/** 解析結果の述語名 */
	private String		name;
	/** 解析結果の引数 */
	private Const[]	args;


	//=========================================================================
	// ◆ コンストラクタ
	//=========================================================================
	/**
	 * 構文解析を行うインスタンスを生成します
	 *
	 * リーダを設定すると自動でラッパークラスのSwiFileReaderを生成します．
	 */
	public SwiFileLexer(Reader in) {
		reader 		= new SwiFileReader(in);
		constTable	= new Hashtable<String,Const>();
	}


	//=========================================================================
	// ◆ GetterおよびSetterのメソッド
	//=========================================================================
	/**
	 * 解析結果の述語名を取得します
	 *
	 * @return 述語名
	 */
	public String name() { return name+"/"+args.length; }


	/**
	 * 解析結果の引数を取得します
	 *
	 * @return	引数
	 */
	public Const[] args() { return args; }


	//=========================================================================
	// ◆ その他のメソッド
	//=========================================================================
	/**
	 * アトム1つ分の解析を行います
	 *
	 * @return EOFに到達した場合 false
	 */
	public boolean advance() {
		try {
			// 空白文字をスキップ
			reader.skipWhiteCharacter();

			if(reader.read() < 0) { return false; }
			reader.unread();

			// 述語名を取得
			name = getString();

			int c;
			if((c=reader.read()) != '(') { throw new Exception("構文エラー"+(char)c); }

			// 引数を取得
			ArrayList<Const> list = new ArrayList<Const>();

			// 第一引数のみ別処理
			reader.skipWhiteCharacter();
			list.add(getConst());

			// 第二引数以降
			while(true) {
				reader.skipWhiteCharacter();

				int ch = reader.read();
				if(ch == ')') { break; }
				if(ch != ',') { throw new Exception("構文エラー"); }

				reader.skipWhiteCharacter();
				list.add(getConst());
			}
			if(reader.read() != '.') { throw new Exception("構文エラー"); }

			args = list.toArray(new Const[0]);
		}
		catch (Exception e) { e.printStackTrace(); }

		return true;
	}


	/**
	 * 入力ストリームから連続する文字列を取得します
	 *
	 * @return	文字列
	 * @throws Exception
	 */
	private String getString() throws Exception {
		StringBuilder sb = new StringBuilder();

		while(true) {
			int ch = reader.read();
			// ファイルの終端に到達した場合
			if(ch < 0) { throw new Exception("構文エラー"); }

			// '''だった場合
			if(ch == '\'') {
				while(true) {
					ch = reader.read();

					if(ch == '\'') { break; }
					sb.append((char)ch);
				}
				break;
			}
			//
			if(! Character.isJavaIdentifierPart((char)ch) &&
					(ch != '-') && (ch != '.')) {
				reader.unread();
				break;
			}
			sb.append((char)ch);
		}

		return sb.toString();
	}


	/**
	 * 定数項を取得します
	 *
	 * getString()で文字列を読み込み，キャッシュと照合した結果を返します．
	 *
	 * @return	定数項
	 * @throws Exception
	 */
	private Const getConst() throws Exception {
		String str = getString();

		Const c = constTable.get(str);
		if(c == null) {
			c = new Const(str);
			constTable.put(str, c);
		}
		return c;
	}
}

